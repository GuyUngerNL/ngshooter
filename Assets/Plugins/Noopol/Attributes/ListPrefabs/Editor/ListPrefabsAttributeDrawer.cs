﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;
using System.Collections;

namespace Noopol {

	[CustomPropertyDrawer(typeof(ListPrefabsAttribute), true)]
	public sealed class ListPrefabsAttributeDrawer : PropertyDrawer {

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {

			if(property.propertyType != SerializedPropertyType.ObjectReference)
			{
				EditorGUI.LabelField(position, property.displayName);
				return;
			}
			
			int selectedIndex = -1;
			int newIndex = 0;
			var listPrefabsAttribute = attribute as ListPrefabsAttribute;

			string typeSelector = "t:Prefab";
			var guids = AssetDatabase.FindAssets(typeSelector);

			List<UnityEngine.Object> valueList = new List<UnityEngine.Object> ();
			List<GUIContent> nameList = new List<GUIContent> ();

			valueList.Add(null);
			nameList.Add(new GUIContent("None"));

			if (guids != null)
			{
				for(int i = 0, l = guids.Length; i < l; i++)
				{
					var guid = guids [i];
					var path = AssetDatabase.GUIDToAssetPath (guid);
					var value = AssetDatabase.LoadAssetAtPath <GameObject>(path);

					bool hasAllRequiredComponents = true;

					for (int j = 0; j < listPrefabsAttribute.requiredComponents.Length; j++)
					{
						if (value.GetComponentInChildren(listPrefabsAttribute.requiredComponents[j]) == null)
						{
							hasAllRequiredComponents = false;
						}
					}

					if(!hasAllRequiredComponents)
					{
						continue;
					}

					if (value == property.objectReferenceValue)
					{
						selectedIndex = i + 1;
					}

					valueList.Add (value);
					nameList.Add (new GUIContent(value.name));
				}
			}

			position.width -= 20f;

			if (selectedIndex == -1)
			{
				selectedIndex = 0;

				newIndex = EditorGUI.Popup(position, label, selectedIndex, nameList.ToArray());

				GUI.changed = true;

				property.objectReferenceValue = valueList[newIndex];

				if (!EditorApplication.isPlaying)
				{
					EditorSceneManager.MarkAllScenesDirty();
				}
			}
			else
			{
				EditorGUI.BeginChangeCheck();
				newIndex = EditorGUI.Popup(position, label, selectedIndex, nameList.ToArray());
				if (EditorGUI.EndChangeCheck())
				{
					property.objectReferenceValue = valueList[newIndex];

					if (!EditorApplication.isPlaying)
					{
						EditorSceneManager.MarkAllScenesDirty();
					}
				}
			}

			position.x += position.width + 2f;
			position.width = 18f;
			position.height -= 1f;

			EditorGUI.BeginDisabledGroup(property.objectReferenceValue == null);

			if(GUI.Button(position, "•"))
			{
				EditorGUIUtility.PingObject(property.objectReferenceValue);
			}

			EditorGUI.EndDisabledGroup();
		}

	}

}
