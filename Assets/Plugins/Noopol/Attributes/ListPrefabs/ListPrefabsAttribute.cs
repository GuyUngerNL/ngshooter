﻿using System;
using UnityEngine;

[AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
public class ListPrefabsAttribute : PropertyAttribute
{
	public Type[] requiredComponents { get; set; }

	public ListPrefabsAttribute(params Type[] components) 
	{
		this.requiredComponents = components;
	}
}