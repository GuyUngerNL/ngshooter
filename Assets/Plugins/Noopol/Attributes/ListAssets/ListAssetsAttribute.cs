﻿using System;
using UnityEngine;

[AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
public class ListAssetsAttribute : PropertyAttribute {

	public string Folder { get; set; }
	public bool AllowNone { get; set; }

	/// <summary>
	/// Initializes a new instance of the <see cref="Noopol.ListAssetsAttribute"/> class.
	/// </summary>
	/// <param name="searchInFolder">Search in folder.</param>
	public ListAssetsAttribute(string searchInFolder = "", bool allowNone = true) 
	{
		Folder = searchInFolder;
		AllowNone = allowNone;
	}
			
}
