﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;
using System.Collections;

namespace Noopol {

	[CustomPropertyDrawer(typeof(ListAssetsAttribute), true)]
	public sealed class ListAssetsAttributeDrawer : PropertyDrawer {

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {

			if(property.propertyType != SerializedPropertyType.ObjectReference)
			{
				EditorGUI.LabelField(position, property.displayName);
				return;
			}
			
			int selectedIndex = -1;
			int newIndex = 0;
			var listAssetsAttribute = attribute as ListAssetsAttribute;

			var fieldType = fieldInfo.FieldType;

			if (fieldType.IsGenericType)
			{
				foreach(var genericType in fieldType.GetGenericArguments())
				{
					fieldType = genericType;
					break;
				}
			}

			string typeSelector = String.Format ("t:{0}", fieldType.Name);
			string[] guids;

			if(String.IsNullOrEmpty (listAssetsAttribute.Folder))
			{
				guids = AssetDatabase.FindAssets (typeSelector);
			}
			else
			{
				string[] searchInFolder = new string[] {String.Format ("Assets/{0}", listAssetsAttribute.Folder)};
				guids = AssetDatabase.FindAssets (typeSelector, searchInFolder);
			}

			List<UnityEngine.Object> valueList = new List<UnityEngine.Object> ();
			List<GUIContent> nameList = new List<GUIContent> ();

			if(listAssetsAttribute.AllowNone)
			{
				valueList.Add(null);
				nameList.Add(new GUIContent("None"));
			}

			if (guids != null)
			{
				for(int i = 0, l = guids.Length; i < l; i++)
				{
					var guid = guids [i];
					var path = AssetDatabase.GUIDToAssetPath (guid);
					var value = AssetDatabase.LoadAssetAtPath (path, fieldType);

					if (value == property.objectReferenceValue)
					{
						selectedIndex = i;

						if(listAssetsAttribute.AllowNone)
						{
							selectedIndex++;
						}
					}

					valueList.Add (value);
					nameList.Add (new GUIContent(value.name));
				}
			}

			position.width -= 20f;

			if(!listAssetsAttribute.AllowNone && selectedIndex == -1 && guids.Length == 0)
			{
				valueList.Add(null);
				nameList.Add(new GUIContent("None"));

				EditorGUI.Popup(position, label, 0, nameList.ToArray());
			}
			else
			{
				if(selectedIndex == -1)
				{
					selectedIndex = 0;

					newIndex = EditorGUI.Popup(position, label, selectedIndex, nameList.ToArray());

					GUI.changed = true;

					property.objectReferenceValue = valueList[newIndex];

					if (!EditorApplication.isPlaying)
					{
						EditorSceneManager.MarkAllScenesDirty();
					}
				}
				else
				{
					EditorGUI.BeginChangeCheck();
					newIndex = EditorGUI.Popup(position, label, selectedIndex, nameList.ToArray());
					if (EditorGUI.EndChangeCheck())
					{
						property.objectReferenceValue = valueList[newIndex];

						if (!EditorApplication.isPlaying)
						{
							EditorSceneManager.MarkAllScenesDirty();
						}
					}
				}
			}

			

			position.x += position.width + 2f;
			position.width = 18f;
			position.height -= 1f;

			EditorGUI.BeginDisabledGroup(property.objectReferenceValue == null);

			if(GUI.Button(position, "•"))
			{
				EditorGUIUtility.PingObject(property.objectReferenceValue);
			}

			EditorGUI.EndDisabledGroup();
		}

	}

}
