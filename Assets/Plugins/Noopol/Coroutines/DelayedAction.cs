using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class DelayedAction : CustomYieldInstruction 
{
	float m_time;
	Action m_action;

	public override bool keepWaiting {
		get { 
			if(Time.time < m_time) {
				return true;
			} else {
				if(m_action != null) {
					m_action.Invoke ();
				}

				return false;
			}
		}
	}

	public DelayedAction(float seconds, Action action) 
	{ 
		m_time = Time.time + seconds; 
		m_action = action;
	}
}
