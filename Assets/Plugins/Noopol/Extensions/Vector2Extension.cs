﻿using UnityEngine;


namespace Noopol
{
	public static class Vector2Extension 
	{
		
		public static float AngleTo(this Vector2 from, Vector2 to) 
		{
			float ang = Vector2.Angle(from, to);
			Vector3 cross = Vector3.Cross(from, to);
			
			if (cross.z > 0)
			{
				ang = -ang;
			}
			
			return ang;
		}

		public static Vector3 ToVector3(this Vector2 value, float depth = 0f)
		{
			return new Vector3 (value.x, value.y, depth);
		}

		//public static Vector2 LengthDir(float length, float direction)
		//{
		//	return new Vector2(Mathf.Cos(direction * Mathf.Deg2Rad) * length, Mathf.Sin(direction * Mathf.Deg2Rad) * length);
		//}
	}
}

