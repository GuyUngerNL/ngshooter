﻿using UnityEngine;

namespace Noopol
{
	static public class BoundsExtensions
	{
		
		static public bool Encapsulates (this Bounds parent, Bounds bounds)
		{
			return (
				parent.min.x < bounds.min.y && 
				parent.min.y < bounds.min.y && 
				parent.max.x > bounds.max.x &&
				parent.max.y > bounds.max.y
			);
		}

		/// <summary>
		/// Converts the point from world space to bounds space 
		/// </summary>
		/// <param name="bounds"></param>
		/// <param name="worldPoint"></param>
		/// <returns></returns>
		static public Vector3 GetLocalPosition(this Bounds bounds, Vector3 worldPoint)
		{
			var min = bounds.min;
			var size = bounds.size;

			return new Vector3(
				(worldPoint.x - min.x) / size.x,
				(worldPoint.y - min.y) / size.y,
				(worldPoint.z - min.z) / size.z
			);
		}

		/// <summary>
		/// Converts the point from bounds space to world space 
		/// </summary>
		/// <param name="bounds"></param>
		/// <param name="localPoint"></param>
		/// <returns></returns>
		static public Vector3 GetWorldPosition(this Bounds bounds, Vector3 localPoint)
		{
			var min = bounds.min;
			var size = bounds.size;

			return new Vector3(
				min.x + size.x * localPoint.x,
				min.y + size.y * localPoint.y,
				min.z + size.z * localPoint.z
			);
		}

		static public void Expand(this Bounds bounds, Vector3 min, Vector3 max)
		{
			bounds.SetMinMax(bounds.min + min, bounds.max + max);
		}

		static public void Scale(this Bounds bounds, Vector3 pivot, Vector3 scale)
		{
			bounds.SetMinMax(pivot + Vector3.Scale((bounds.min - pivot), scale), pivot + Vector3.Scale((bounds.max - pivot), scale));
		}


	}
}

