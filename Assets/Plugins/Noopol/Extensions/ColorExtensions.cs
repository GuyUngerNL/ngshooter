using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Noopol
{
    public static class ColorExtensions
    {
		static public Color WithAlpha(this Color color, float alpha)
		{
			color.a = alpha;
			return color;
		}
    }
}