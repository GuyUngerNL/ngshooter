﻿using UnityEngine;


namespace Noopol
{
	public static class RectExtension 
	{

		public static Rect Normalized(this Rect value) 
		{
			Rect rect = value;

			rect.Set (
				value.width < 0 ? value.x + value.width : value.x,
				value.height < 0 ? value.y + value.height : value.y,
				value.width < 0 ? -value.width : value.width,
				value.height < 0 ? -value.height : value.height
			);

			return rect;
		}

        public static Rect Scale(this Rect value, Vector2 scale)
        {
            return new Rect(value.x * scale.x, value.y * scale.y, value.width * scale.x, value.height * scale.y);
        }
	}
}

