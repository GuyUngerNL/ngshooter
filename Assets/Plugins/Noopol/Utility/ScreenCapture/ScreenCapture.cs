﻿using UnityEngine;
using System.Collections;
using System;

public class ScreenCapture : MonoBehaviour 
{
	public int framerate = 60;
	public int scale = 1;
	public string outputFilename;
	public bool isRecording = false;
	public bool recordOnStart = false;

	private int _lastFrameRecorded;

	public int lastFrameRecorded 
	{
		get {
			return _lastFrameRecorded;
		}
	}

	void Awake()
	{
		_lastFrameRecorded = 0;
	}

	void Start()
	{
		if(recordOnStart) {
			isRecording = true;
		}
	}

	public void Update()
	{

		if(isRecording && !string.IsNullOrEmpty (outputFilename) && outputFilename.Length > 4) {
			Time.captureFramerate = framerate;
			var filename = outputFilename.Substring (0, outputFilename.Length - 4) + "{0:D04}.png";
			UnityEngine.ScreenCapture.CaptureScreenshot (string.Format (filename, _lastFrameRecorded), scale);
			_lastFrameRecorded++;
		}
	}
}