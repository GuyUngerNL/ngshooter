﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;

public class ScreenCaptureWindow : EditorWindow
{
	private ScreenCapture _component { get; set; }
	private ScreenCapture component 
	{ 
		get {
			if(!EditorApplication.isPlaying) {
				return null;
			}

			if(_component == null) {
				var existingComponents = Resources.FindObjectsOfTypeAll (typeof(ScreenCapture)) as ScreenCapture[];

				if(existingComponents != null && existingComponents.Length > 0) {
					_component = existingComponents [0];
					_component.gameObject.hideFlags = HideFlags.HideInHierarchy;
				} else {
					var o = EditorUtility.CreateGameObjectWithHideFlags ("screen_capture_component", HideFlags.HideInHierarchy, typeof(ScreenCapture));
					DontDestroyOnLoad (o);
					_component = o.GetComponent <ScreenCapture> ();
				}
			}

			_component.recordOnStart = recordOnPlay && hasAcceptableFileSize;
			_component.framerate = ScreenCaptureSettings.frameRate;
			_component.scale = ScreenCaptureSettings.superSize;
			_component.outputFilename = ScreenCaptureSettings.fileName;

			return _component;
		} 
	}

	private static Vector2 fileResolution 
	{
		get {
			return Handles.GetMainGameViewSize () * ScreenCaptureSettings.superSize;
		}
	}

	private static int estimatedFileSize 
	{
		get {
			var res = fileResolution;
			return Mathf.CeilToInt( (fileResolution.x * fileResolution.y * 4f) / 1024f );
		}
	}

	private static string estimatedFileSizeFormated
	{
		get {
			var size = estimatedFileSize;
			if(size < 1024) {
				return string.Format ("{0:0}kb", size);
			} else {
				return string.Format ("{0:0.00}MB", size / 1024f);
			}
		}
	}

	private static bool hasAcceptableFileSize
	{
		get {
			return estimatedFileSize < 1024 * 1024;
		}
	}

	[MenuItem ("Window/Screen Capture")]
	public static void  ShowWindow () 
	{
		ScreenCaptureWindow window = (ScreenCaptureWindow)EditorWindow.GetWindow (typeof (ScreenCaptureWindow));
		window.titleContent.text = "Screen Capture";
		window.Show();
	}

	public bool recordOnPlay = false;

	void OnEnable()
	{
		EditorApplication.playmodeStateChanged += OnPlaymodeStateChange;
	}

	void OnDisable()
	{
		EditorApplication.playmodeStateChanged -= OnPlaymodeStateChange;
	}

	void OnPlaymodeStateChange()
	{
		if(_component == null && component != null) {
			component.recordOnStart = recordOnPlay;
		}
	}

	void Update()
	{
		Repaint ();
	}

	void OnGUI ()
	{
		DestinationFolder ();

		EditorGUI.BeginDisabledGroup (string.IsNullOrEmpty (ScreenCaptureSettings.fileName));

		if(string.IsNullOrEmpty (ScreenCaptureSettings.fileName)) {
			EditorGUILayout.HelpBox ("Please select a filename.", MessageType.Error);
		}

		EditorGUI.BeginDisabledGroup (component && component.isRecording);

		ScreenCaptureSettings.frameRate = EditorGUILayout.IntField ("Framerate", ScreenCaptureSettings.frameRate);
		ScreenCaptureSettings.superSize = EditorGUILayout.IntField ("Scale", ScreenCaptureSettings.superSize);

		ScreenCaptureSettings.frameRate = Mathf.Max (ScreenCaptureSettings.frameRate, 1);
		ScreenCaptureSettings.superSize = Mathf.Max (ScreenCaptureSettings.superSize, 1);

		EditorGUI.EndDisabledGroup ();

		if(!component) {
			EditorGUI.BeginDisabledGroup (!hasAcceptableFileSize);

			recordOnPlay = EditorGUILayout.Toggle ("Record on play", recordOnPlay && hasAcceptableFileSize && !string.IsNullOrEmpty (ScreenCaptureSettings.fileName));

			if(hasAcceptableFileSize) {
				EditorGUILayout.HelpBox (string.Format ("Output Resolution: {0:0}x{1:0}\nEstimated File Size: {2}", fileResolution.x, fileResolution.y, estimatedFileSizeFormated), MessageType.Info);
			} else {
				EditorGUILayout.HelpBox (string.Format ("Output Resolution: {0:0}x{1:0}\nEstimated File Size: {2} (Reduce scale to prevent large file sizes)", fileResolution.x, fileResolution.y, estimatedFileSizeFormated), MessageType.Error);
			}
			
			EditorGUI.EndDisabledGroup ();
		} else {
			if(hasAcceptableFileSize) {
				EditorGUILayout.HelpBox (string.Format ("Output Resolution: {0:0}x{1:0}\nEstimated File Size: {2}\nScreenshots saved this session: {3}", fileResolution.x, fileResolution.y, estimatedFileSizeFormated, component.lastFrameRecorded), MessageType.Info);
			} else {
				EditorGUILayout.HelpBox (string.Format ("Output Resolution: {0:0}x{1:0}\nEstimated File Size: {2} (Reduce scale to prevent large file sizes)", fileResolution.x, fileResolution.y, estimatedFileSizeFormated), MessageType.Error);
			}

			EditorGUI.BeginDisabledGroup (!hasAcceptableFileSize);

			component.isRecording = GUILayout.Toggle (component.isRecording, "Recording", GUI.skin.button);

			EditorGUI.EndDisabledGroup ();
		}


		EditorGUI.BeginDisabledGroup (!hasAcceptableFileSize);

		if(GUILayout.Button ("Capture Screenshot")) {
			var filename = ScreenCaptureSettings.fileName.Substring (0, ScreenCaptureSettings.fileName.Length - 4) + "_{0}.png";
			filename = string.Format (filename, System.DateTime.Now.ToString ("yyyy-MM-dd-HH-mm-ss"));
			UnityEngine.ScreenCapture.CaptureScreenshot(filename, ScreenCaptureSettings.superSize);
			ShowNotification (new GUIContent("Screenshot captured!"));
		}

		EditorGUI.EndDisabledGroup ();

		EditorGUI.EndDisabledGroup ();


		if(component) {
			component.framerate = ScreenCaptureSettings.frameRate;
			component.scale = ScreenCaptureSettings.superSize;
			component.outputFilename = ScreenCaptureSettings.fileName;
		}
	}


	public void DestinationFolder()
	{
		string path = ScreenCaptureSettings.fileName;
		string directory = string.IsNullOrEmpty (path) ? Directory.GetParent (Application.dataPath).FullName : System.IO.Path.GetDirectoryName (path);
		string name = "Output filename";

		EditorGUILayout.BeginHorizontal();
		EditorGUI.BeginDisabledGroup (true);
		EditorGUILayout.TextField (name, path);
		EditorGUI.EndDisabledGroup ();
		if (GUILayout.Button("Browse", GUILayout.MaxWidth(60f)))
		{
			EditorGUI.BeginChangeCheck ();
			ScreenCaptureSettings.fileName = EditorUtility.SaveFilePanel(name, directory, "", "png" );
			if(EditorGUI.EndChangeCheck ()) {
				Repaint ();
			}
		}
		if (GUILayout.Button("Clear", GUILayout.MaxWidth(60f)))
		{
			ScreenCaptureSettings.fileName = string.Empty;
		}
		EditorGUILayout.EndHorizontal();
	}

}
