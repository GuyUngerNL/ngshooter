﻿using UnityEngine;
using UnityEditor;

public static class ScreenCaptureSettings
{
	readonly static string _frameRateKey = "_screen_capture_settings_framerate";
	readonly static string _superSizeKey = "_screen_capture_settings_supersize";
	readonly static string _fileNameKey = "_screen_capture_settings_filename";

	public static int frameRate 
	{ 
		get {
			return EditorPrefs.GetInt (_frameRateKey, 60);
		} 
		set {
			EditorPrefs.SetInt (_frameRateKey, Mathf.Max (1, value));
		} 
	}

	public static int superSize 
	{ 
		get {
			return EditorPrefs.GetInt (_superSizeKey, 1);
		} 
		set {
			EditorPrefs.SetInt (_superSizeKey, Mathf.Max (1, value));
		} 
	}

	public static string fileName 
	{ 
		get {
			return EditorPrefs.GetString (_fileNameKey, string.Empty);
		} 
		set {
			EditorPrefs.SetString (_fileNameKey, value);
		} 
	}
}
