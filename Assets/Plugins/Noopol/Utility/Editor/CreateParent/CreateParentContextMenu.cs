﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace Noopol 
{
	public class CreateParentContextMenu 
	{
		private static GameObject lastCreatedParent = null;
		
	/// <summary>
	/// Creates Parent for selected objects
	/// You can use this from context menu in hierarchy window
	/// </summary>
		[MenuItem("GameObject/Create Parent %g", false, 0)]
		public static void AssetsCreateParent()
		{			
			Undo.IncrementCurrentGroup();
			
			GameObject activeObject = null;
			
			Vector3 averagePosition = new Vector3();
			int numberOfGameObjectsInSelection = 0;
			
			foreach(Object obj in Selection.objects)
			{
				if(obj is GameObject)
				{
					if(lastCreatedParent == obj) 
					{
						return;
					}
					numberOfGameObjectsInSelection++;
					activeObject = obj as GameObject;
					averagePosition += activeObject.transform.position;
				}
			}
						
			if(activeObject == null) 
			{
				return;
			}
			
			averagePosition /= numberOfGameObjectsInSelection;
			
			
			string expectedParentObjectName = "Group";
			
			GameObject parentObject = null;
						
			foreach(Object obj in Selection.objects)
			{				
				if(obj is GameObject)
				{
					GameObject gameObject = obj as GameObject;
										
					if(parentObject == null)
					{
						parentObject = new GameObject();
						Undo.RegisterCreatedObjectUndo (parentObject, "Created Parent Object");
						parentObject.name = expectedParentObjectName;
						parentObject.transform.position = averagePosition;
						Undo.SetTransformParent(parentObject.transform, Selection.activeGameObject.transform.parent, "Set Initial Parent");
						lastCreatedParent = parentObject;
					}
					Undo.SetTransformParent(gameObject.transform, parentObject.transform, "Move Child To New Parent");

				}
			}
			
			if(parentObject != null)
			{
				Selection.activeGameObject = parentObject;
			}
		}
		
		[MenuItem("GameObject/Create Parent", true, 0)]
		public static bool AssetsCreateParent_Validator()
		{			
			return Selection.objects.Length != 0;
		}
	}
}
