﻿using System;

[Serializable]
public struct RangedFloat
{
	public float minValue;
	public float maxValue;

    bool _initialized;
    float _currentValue;

	public RangedFloat(float minValue, float maxValue) : this()
	{
		this.minValue = minValue;
		this.maxValue = maxValue;
	}

	public float value
    {
        get
        {
            if(!_initialized)
            {
                return Randomize();
            }

            return _currentValue;
        }
    }

    public float Randomize()
    {
        _initialized = true;
        _currentValue = UnityEngine.Random.Range(minValue, maxValue);
        return _currentValue;
    }

    public static implicit operator float (RangedFloat rf)
    {
        return rf.value;
    }
}