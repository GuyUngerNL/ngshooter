﻿using UnityEngine;
using System.Collections;
using System;

public class FastList<T> where T : class
{
	int m_capacity;

	int m_listLength;
	int m_stackLength;

	T[] m_list;
	int[] m_stack;

	int m_removeIterator;

	public FastList(int capacity = 0)
	{
		if(capacity < 0)
		{
			capacity = 0;
		}
		
		m_capacity = capacity;

		m_list = new T[capacity];
		m_stack = new int[capacity];
	}

	public int Add(T item)
	{
		int insertIndex;

		if(m_stackLength > 0)
		{
			m_stackLength--;
			insertIndex = m_stack[m_stackLength];
		}
		else
		{
			if(m_capacity == m_listLength)
			{
				Expand ();
			}

			insertIndex = m_listLength;
			m_listLength++;
		}

		m_list [insertIndex] = item;

		return insertIndex;
	}

	public void Remove(T item)
	{
		Remove (Array.IndexOf <T> (m_list, item));
	}

	public void Remove(int id)
	{
		if(id < 0 || id >= m_listLength)
		{
			return;
		}

		m_stack [m_stackLength] = id;
		m_stackLength++;

		m_list [id] = null;
		m_listLength--;
	}

	void Expand()
	{
		m_capacity = m_capacity == 0 ? 1 : m_capacity * 2;

		Array.Resize<T> (ref m_list, m_capacity);
		Array.Resize<int> (ref m_stack, m_capacity);
	}

	public T this[int i]
	{
		get
		{
			return m_list[i];
		}
	}

	public int Length
	{
		get
		{
			return m_listLength;
		}
	}

	public int Capacity
	{
		get
		{
			return m_capacity;
		}
	}
}
