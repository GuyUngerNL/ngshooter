﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SpitfireEditorPreferences
{
	[Serializable]
    public class Prefs
	{
		public Color resourceItemBackgroundColor = new Color(0f, 0.9f, 0.1f, 0.07f);
		public bool playClipsAutomatically = true;
	}

	private const string prefsKey = "spitfire_editor_preferences";
	private static Prefs _preferences = null;

	public static Prefs preferences
	{
		get
		{
			if(_preferences == null)
			{
				_preferences = new Prefs();
				if (EditorPrefs.HasKey(prefsKey))
				{
					EditorJsonUtility.FromJsonOverwrite(EditorPrefs.GetString(prefsKey), _preferences);
				}
			}

			return _preferences;
		}
	}

	public delegate void OnPrefsChangedHandler();
	public static event OnPrefsChangedHandler OnPrefsChanged;

	[PreferenceItem("Spitfire")]
	private static void CustomPreferencesGUI()
	{
		EditorGUILayout.LabelField("Project View", EditorStyles.boldLabel);

		preferences.resourceItemBackgroundColor = EditorGUILayout.ColorField("Resource Item Color: ", preferences.resourceItemBackgroundColor);
		preferences.playClipsAutomatically = EditorGUILayout.Toggle("Play Audio Clips Automatically", preferences.playClipsAutomatically);

		EditorGUILayout.Space();

		if (GUI.changed)
		{
			EditorPrefs.GetString(prefsKey, EditorJsonUtility.ToJson(preferences));

			if(OnPrefsChanged != null)
			{
				OnPrefsChanged.Invoke();
			}
		}
	}

}
