﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;

public class ModelImporting : AssetPostprocessor
{
	public void OnPreprocessModel()
	{
		ModelImporter modelImporter = (ModelImporter)assetImporter;
		modelImporter.importMaterials = false;
	}
}