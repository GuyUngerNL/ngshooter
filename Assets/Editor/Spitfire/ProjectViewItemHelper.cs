﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using UnityEditor;
using Noopol;


[InitializeOnLoad]
public static class ProjectViewItemHelper
{
	static Texture2D texResource;

	static ProjectViewItemHelper()
	{
		InitAssets();
		EditorApplication.projectWindowItemOnGUI += OnProjectItemGUI;
		SpitfireEditorPreferences.OnPrefsChanged += SpitfireEditorPreferences_OnPrefsChanged; ;
	}

	private static void SpitfireEditorPreferences_OnPrefsChanged()
	{
		InitAssets();
	}

	static void InitAssets()
	{
		texResource = CreateTextureFromColor(SpitfireEditorPreferences.preferences.resourceItemBackgroundColor);
	}

	static Texture2D CreateTextureFromColor(Color color)
	{
		var tex = new Texture2D(1, 1);
		tex.SetPixel(0, 0, color);
		tex.Apply();
		return tex;
	}

	static void OnProjectItemGUI(string guid, Rect selectionRect)
	{
		if (texResource == null) return;

		string path = AssetDatabase.GUIDToAssetPath(guid);

		if(path.Contains("/Resources/"))
		{
			if (selectionRect.height == 16f)
			{
				selectionRect.width = selectionRect.x + selectionRect.width;
				selectionRect.x = 0f;
			}

			selectionRect.y += 1f;
			selectionRect.x += 1f;
			selectionRect.width -= 2f;
			selectionRect.height -= 2f;

			GUI.DrawTexture(selectionRect, texResource);
		}
	}
}

