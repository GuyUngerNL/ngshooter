﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Point {
	public float x, y;
	public Point(float px=0, float py=0)
	{
		x = px;
		y = py;
	}

	public Point set(float xa=0, float ya=0){
		x = xa;
		y = ya;
		return this;
	}

	public Point setTo(float xa=0, float ya=0){
		x = xa;
		y = ya;
		return this;
	}

	public Point clone(){
		return new Point (x, y);
	}

	public void copyFrom(Point other){
		x = other.x;
		y = other.y;
	}

    public float angle()
    {
        return Mathf.Atan2(y, x) * (180f / Mathf.PI);
    }

    public float length()
    {
        return distanceTo(0, 0);
    }
     
    public virtual float distanceTo(float x, float y)
    {
        return Math.Distance(x, y, this.x, this.y);
    }

    public virtual float angleTowards(float x, float y)
    {
        return Math.AngleTo(this.x, this.y, x, y);
    }

    static private int _pointI = 0;
    static private List<Point> _weakPoints = new List<Point>();
    static public Point recycle(float x = 0, float y = 00)
    {
        if (_pointI >= _weakPoints.Count) _weakPoints.Add(new Point());
        _weakPoints[_pointI].x = x;
        _weakPoints[_pointI].y = y;
        _pointI++;
        return _weakPoints[_pointI - 1];
    }

    static public void resetPoolIndex()
    {
        _pointI = 0;
    }
}