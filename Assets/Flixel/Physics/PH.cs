﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;

 
public static class PH  {

	public static float towardValue( float currentValue, float targetValue, float speed )
	{ 
		if ( targetValue > currentValue )
		{
			currentValue += speed;
			if ( currentValue > targetValue ) currentValue = targetValue;
		}
		else if ( targetValue < currentValue )
		{
			currentValue -= speed;
			if ( currentValue < targetValue ) currentValue = targetValue;
		}
		return currentValue;
	}

	public static void log (params object[] values)
	{
		string result = "";
		foreach (object value in values) {
			result += value.ToString () + ",";
		}
		if(result.Length > 0) result.Substring (0,result.Length-2);
		Debug.Log (result);
	}

	public static float roomSpeed = 1;
	public static float DEG_TO_RAD = MathX.PI / 180;
	public static float RAD_TO_DEG = 180 / MathX.PI;

	/**
		 * Helper , } y, {x:, } y,...] that form a closed polygon to determine whether there is an intersection between the two polygons described
		 * by the lists of vertices. Uses the Separating Axis Theorem
		 *
		 * @param a an array of connected points [{x
		 * @param b an array of connected points [{x:, } y, {x:, } y,...] that form a closed polygon
		 * @return true if there is any intersection between the 2 polygons, false otherwise
		 */
//	private static PointDec normal = new PointDec();
	/*public static bool checkOverlapBetweenRotatedPolygons( List<PointDec> a, List<PointDec> b )
	{
		List<List<PointDec>> polygons = new List<List<PointDec>>();
		polygons.Add (a);
		polygons.Add (b);
		decimal minA;
		decimal maxA;
		decimal projected;
		int i;
		int i1;
		int j;
		decimal minB;
		decimal maxB;

		for (i = 0; i < polygons.Count; i++) 
		{
			// for each polygon, look at each edge of the polygon, and determine if it separates
			// the two shapes
			List<PointDec> polygon = polygons[i];
			Debug.Log (polygon.Count);
			Debug.Log (polygons.Count);
			for ( i1 = 0; i < polygon.Count; i++ ) 
			{
				// grab 2 vertices to create an edge
				int i2 = (i1 + 1) % polygon.Count;
				PointDec p1 = polygon[i1];
				PointDec p2 = polygon[i2];

				// find the line perpendicular to this edge
				normal.setTo( p2.y - p1.y,  p1.x - p2.x );

				minA = maxA = -1;
				// for each vertex in the first shape, project it onto the line perpendicular to the edge
				// and keep track of the min and max of these values
				for ( j = 0; j < a.Count; j++ ) 
				{
					Debug.Log ("j " + j);
					projected = normal.x * a[j].x + normal.y * a[j].y;
					//log (minA,maxA);
					if ( minA == -1  || projected < minA) 
					{
						minA = projected;
					}
					if ( (maxA == -1) || projected > maxA) 
					{
						maxA = projected;
					}
				}

				// for each vertex in the second shape, project it onto the line perpendicular to the edge
				// and keep track of the min and max of these values
				minB = maxB = -1;
				for ( j = 0; j < b.Count; j++) 
				{
					Debug.Log ("j2 " + j);
					//log(minB,maxB);
					projected = normal.x * b[j].x + normal.y * b[j].y;
					if ( minB == -1 || projected < minB) {
						minB = projected;
					}
					if (  maxB == -1 || projected > maxB) 
					{
						maxB = projected;
					}
				}

				// if there is no overlap between the projects, the edge we are looking at separates the two
				// polygons, and we know there is no overlap
				if (maxA < minB || maxB < minA) 
				{
					return false;
				}
			}
		}
		return true;
	}*/


	public static bool checkOverlapBetweenRotatedPolygons( List<Point> a, List<Point> b ){
	 	float x1 = 0;
	 	float y1 = 0;
	 	float x2 = 0;
	 	float y2 = 0;
	 	//Debug.Log (a.Count);
	 	//Debug.Log (b.Count);
		for(int i = 1; i < a.Count; i++){
			x1 = a[i-1].x;
			y1 = a[i-1].y;
			x2 = a[i].x;
			y2 = a[i].y;
			//log("first ", i, x1,y1,x2,y2);
			for(int j = 1; j < b.Count; j++){
				float x3 = b[j-1].x;
				float y3 = b[j-1].y;
				float x4 = b[j].x;
				float y4 = b[j].y;
				//log("second ", j, x3,y3,x4,y4);
				if(MathX.isIntersection (x1,y1,x2,y2,x3,y3,x4,y4)) return true;
			}
		}

		bool containsWholePolygon = true;
		for(int i = 0; i < a.Count; i++){
			Point c = a[i];
			if(PH.PolygonContainsPoint (b, c) == false){
				containsWholePolygon = false;
				break;
			}
		}
		if(containsWholePolygon) return true;


		containsWholePolygon = true;
		for(int i = 0; i < b.Count; i++){
			Point c = b[i];
			if (PH.PolygonContainsPoint (a, c) == false) {
				containsWholePolygon = false;
				break;
			}
		}
		if(containsWholePolygon) return true;

		return false;
	}

	public static bool PolygonContainsPoint (List<Point> polygon, Point testPoint){ 
		bool result = false;
        int j = polygon.Count - 1;
        for (int i = 0; i < polygon.Count; i++)
        {
			if (polygon[i].y < testPoint.y && (polygon[j].y >= testPoint.y || MathX.floatsEqual (polygon[j].y, testPoint.y) ) || polygon[j].y < testPoint.y && (polygon[i].y >= testPoint.y || MathX.floatsEqual (polygon[i].y, testPoint.y)  ))
            {
                if (polygon[i].x + (testPoint.y - polygon[i].y) / (polygon[j].y - polygon[i].y) * (polygon[j].x - polygon[i].x) < testPoint.x)
                {
                    result = !result;
                }
            }
            j = i;
        }
        return result;

//		Point p1, p2;
//
//
//        bool inside = false;
//
//
//        if (poly.Count < 3)
//        {
//            return inside;
//        }
//
//
//        var oldPoint = new Point(
//			poly[poly.Count - 1].x, poly[poly.Count - 1].y);
//
//
//		for (int i = 0; i < poly.Count; i++)
//        {
//            var newPoint = new Point(poly[i].x, poly[i].y);
//
//
//            if (newPoint.x > oldPoint.x)
//            {
//                p1 = oldPoint;
//
//                p2 = newPoint;
//            }
//
//            else
//            {
//                p1 = newPoint;
//
//                p2 = oldPoint;
//            }
//
//
//            if ((newPoint.x < p.x) == (p.x <= oldPoint.x)
//                && (p.y - (long) p1.y)*(p2.x - p1.x)
//                < (p2.y - (long) p1.y)*(p.x - p1.x))
//            {
//                inside = !inside;
//            }
//
//
//            oldPoint = newPoint;
//        }
//
//
//        return inside;
	}


	private static Point qBezierPoint = new Point();
	public static Point quadraticBezierPoint(float u, Point anchor1, Point anchor2, Point control)
	{
		float uc = 1 - u;
		float posx = MathX.pow(uc, 2) * anchor1.x + 2 * uc * u * control.x + MathX.pow(u, 2) * anchor2.x;
		float posy = MathX.pow(uc, 2) * anchor1.y + 2 * uc * u * control.y + MathX.pow(u, 2) * anchor2.y;
		qBezierPoint.setTo( posx, posy );
		return qBezierPoint;
	}


	public static float quadraticBezierAngle(float u, Point anchor1, Point anchor2, Point control) 
	{
		float uc = 1 - u;
		float dx = (uc * control.x + u * anchor2.x) - (uc * anchor1.x + u * control.x);
		float dy = (uc * control.y + u * anchor2.y) - (uc * anchor1.y + u * control.y);
		return PH.RAD_TO_DEG * MathX.atan2(dy, dx);
	}

	public static float logx(float val, float b = 10)
	{
		return MathX.log(val) / MathX.log(b);
	}

	static public  float roundDec(float numIn, float decimalPlaces) 
	{
		int nExp =  (int)(MathX.pow(10,decimalPlaces)) ; 
		float nRetVal = MathX.round(numIn * nExp) / nExp;
		return nRetVal;
	}

	public static float normalizeAngle( float angle )
	{
		if ( angle > 360 )
		{
			angle %= 360;
		}
		else if ( angle < 0 )
		{
			if ( angle < -359 ) 
			{
				angle =  (-1) * ( MathX.abs( angle ) % 360 ) ;
			}
			angle += 360;
		} 
		return angle;
	}


	static public bool isAngleInZone( float angle, float angleMiddle, float angleRange )
	{

		angle = normalizeAngle( angle );
		angleMiddle = normalizeAngle( angleMiddle ); 

		/*			angle = 360 - angle;
			angleMiddle = 360 - angle;*/

		float limitA = angleMiddle - angleRange;
		float limitB = angleMiddle + angleRange;

		if ( limitA < 0 )
		{
			limitA += 360;
			limitB += 360;
			angle += 360;
		}




		if ( limitA < limitB )
		{
			if ( angle >= limitA && angle <= limitB )
			{
				return true;
			}
			else 
			{
				return false;
			}
		}
		else
		{
			if ( angle >= limitB && angle <= limitA )
			{
				return true;
			}
			else 
			{
				return false;
			}
		} 
	}

	static public float getSmallerAngleDifference( float angle1, float angle2 )
	{
		angle1 = normalizeAngle( angle1 );
		angle2 = normalizeAngle( angle2 );

		float diff1 =  MathX.abs(angle1 - angle2) ;
		float diff2 = 0;
		if ( angle1 <= 180 )
		{
			diff2 += angle1;
		}
		else if ( angle1 > 180 )
		{
			diff2 += ( 360 - angle1 );
		}
		if ( angle2 <= 180 )
		{
			diff2 += angle2;
		}
		else if ( angle2 > 180 )
		{
			diff2 += ( 360 - angle2 );
		}

		if ( diff1 <= diff2 )
		{
			return diff1;
		}
		else
		{
			return diff2;
		}

	}


	static public float  distance(float X1, float Y1, float X2, float Y2)
	{
		return MathX.sqrt((X2 - X1) * (X2 - X1) + (Y2 - Y1) * (Y2 - Y1));
	}
	static public float angleTowards(float targetX, float targetY, float sourceX, float sourceY)
	{
		return normalizeAngle (MathX.atan2((targetY - (sourceY)), (targetX- (sourceX))) * RAD_TO_DEG) ;
	}


	private static Point pointX = new Point();
	static public Point setVelocity(float speed, float direction  )
	{
		pointX.x = roomSpeed * speed * MathX.cos(direction * DEG_TO_RAD );
		pointX.y = roomSpeed * speed * MathX.sin(direction * DEG_TO_RAD );	
		return pointX;
	}


	public static Point limitVelocity( Point velocity, float limitVel )
	{
		float totVel = totalVelocity( velocity );
		if ( totVel > limitVel )
		{
			float a = totVel / limitVel;
			a = a * a;
			velocity.x /= a;
			velocity.y /= a;
		}
		return velocity;
	}


	public static float totalVelocity( Point velocity )
	{
		return MathX.sqrt( MathX.pow( velocity.x, 2 ) + MathX.pow( velocity.y, 2) );
	}

	static public float getDirection( Point velocity ) 
	{
		return  PH.normalizeAngle(MathX.atan2(velocity.y, velocity.x) * RAD_TO_DEG);		
	}
	static public Point setDirection(Point velocity, float direction)
	{
		return setVelocity( totalVelocity( velocity ), direction);
	}

	private static Point addedVelocity = new Point();
	private static Point resultVel = new Point();
	public static Point motion_add( Point curVelocity, float addSpeed, float addDir )
	{

		addedVelocity.x = addSpeed * MathX.cos( DEG_TO_RAD * addDir );
		addedVelocity.y = addSpeed * MathX.sin( DEG_TO_RAD * addDir );


		resultVel.x = curVelocity.x + addedVelocity.x;
		resultVel.y = curVelocity.y + addedVelocity.y ;

		return resultVel;


	}



	static public float scaleclamp(float Arg0, float Arg1, float Arg2, float Arg3, float Arg4)
	{

		if (Arg4 > Arg3)
		{

			return MathX.max(  MathX.min( Arg3 + ((Arg0 - Arg1) / (Arg2 - Arg1)) * (Arg4 - Arg3), Arg4 ), Arg3 );
		}
		else
		{
			return MathX.max( MathX.min( Arg3 + ((Arg0 - Arg1) / (Arg2 - Arg1)) * (Arg4 - Arg3), Arg3 ), Arg4 );
		}

	}
 

	public static Point rotatePoint(float X, float Y, float PivotX, float PivotY, float Angle,Point P=null)
	{
		if(P == null) P = new Point();
		float radians = -Angle / 180 * MathX.PI;
		float dx = X-PivotX;
		float dy = PivotY-Y;
		P.x = PivotX + MathX.cos(radians)*dx - MathX.sin(radians)*dy;
		P.y = PivotY - (MathX.sin(radians)*dx + MathX.cos(radians)*dy);
		return P;
	}
	public static PointDec rotatePointDec(float X, float Y, float PivotX, float PivotY, float Angle,PointDec P=null)
	{
		if(P == null) P = new PointDec();
		float radians = -Angle / 180 * MathX.PI;
		float dx = X-PivotX;
		float dy = PivotY-Y;
		P.x = (decimal)(PivotX + MathX.cos(radians)*dx - MathX.sin(radians)*dy);
		P.y = (decimal)(PivotY - (MathX.sin(radians)*dx + MathX.cos(radians)*dy));
		return P;
	}
	 


	static public float lengthdir_x(float H, float A)
	{
		return MathX.cos(A * DEG_TO_RAD ) * H;
	}

	static public float lengthdir_y(float H, float A)
	{
		return MathX.sin(A * DEG_TO_RAD ) * H;
	}

	static public float random(float Range)
	{
		return MathX.random() * Range;
	}

	 
	static public float sign(float Num)
	{
		if (Num >= 0)
		{

			return 1;
		}
		else
		{

			return -1;
		}
	}

	public static bool isInArray<T>(T element, List<T> array )
	{
		for (int i = 0; i < array.Count; i++  )
		{
			if ( element.Equals (array[i])  ) return true;  //#check Equals does it work
		}
		return false;
	}
 

	static public T choose<T>(params T[] Options)
	{
		int rnd = MathX.floor( MathX.random() * Options.Length );
		return Options[rnd];
	}

	static public T choose<T>(List<T> Options)
	{
		int rnd = MathX.floor( MathX.random() * Options.Count );
		return Options[rnd];
	}


	static public T chooseAndRemove<T>(List<T> Options)
	{
		int rnd = MathX.floor( MathX.random() * Options.Count );
		T res = Options[rnd];
		Options.RemoveAt(rnd);// splice
		return res;
	}


	public static float sumArray( List<float> array )
	{
		float totalSum = 0;
		for (int i=0; i < array.Count; i++)
		{
			totalSum += array[i];
		}
		return totalSum;
	}

}

  