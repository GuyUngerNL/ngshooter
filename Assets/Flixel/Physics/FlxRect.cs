﻿using UnityEngine;
using System.Collections;

/**
 * Stores a rectangle.
 */
public class FlxRect
{
	/**
	 * @default 0
	 */
	public float x;
	/**
	 * @default 0
	 */
	public float y;
	/**
	 * @default 0
	 */
	public float width;
	/**
	 * @default 0
	 */
	public float height;
	
	/**
	 * Instantiate a new rectangle.
	 * 
	 * @param	X		The X-coordinate of the point in space.
	 * @param	Y		The Y-coordinate of the point in space.
	 * @param	Width	Desired width of the rectangle.
	 * @param	Height	Desired height of the rectangle.
	 */
	public FlxRect (float X = 0, float Y = 0, float Width = 0, float Height = 0)
	{
		x = X; 
		y = Y;
		width = Width;
		height = Height;
	}

	public float left{
		get{
			return x;
		}
	}
	public float right{
		get{
			return x  + width;
		}
	}
	public float top{
		get{
			return y;
		}
	}
	public float bottom{
		get{
			return y + height;
		}
	}

	/**
	 * Fill this rectangle with the data provided.
	 * @param	X		The X-coordinate of the point in space.
	 * @param	Y		The Y-coordinate of the point in space.
	 * @param	Width	Desired width of the rectangle.
	 * @param	Height	Desired height of the rectangle.
	 * @return	A reference to itself.
	 */
	public FlxRect set(float X = 0, float Y = 0, float Width = 0, float Height = 0)
	{
		x = X;
		y = Y;
		width = Width;
		height = Height;
		return this;
	}

	/**
	 * Helper FlxRect, just copies the values from the specified rectangle.
	 * @param	Rect	Any <code>FlxRect</code>.
	 * @return	A reference to itself.
	 */
	public FlxRect copyFrom(FlxRect Rect)
	{
		x = Rect.x;
		y = Rect.y;
		width = Rect.width;
		height = Rect.height;
		return this;
	}
	
	/**
	 * Helper FlxRect, just copies the values from this rectangle to the specified rectangle.
	 * @param	Point	Any <code>FlxRect</code>.
	 * @return	A reference to the altered rectangle parameter.
	 */
	public FlxRect copyTo(FlxRect Rect)
	{
		Rect.x = x;
		Rect.y = y;
		Rect.width = width;
		Rect.height = height;
		return Rect;
	}
	
	/**
	 * Helper FlxRect, just copies the values from the specified Flash rectangle.
	 * @param	FlashRect	Any <code>Rectangle</code>.
	 * @return	A reference to itself.
	 */
	public FlxRect copyFromFlash(Rect FlashRect)
	{
		x = FlashRect.x;
		y = FlashRect.y;
		width = FlashRect.width;
		height = FlashRect.height;
		return this;
	}
	
	/**
	 * Helper Rectangle, just copies the values from this rectangle to the specified Flash rectangle.
	 * @param	Point	Any <code>Rectangle</code>.
	 * @return	A reference to the altered rectangle parameter.
	 */
	public Rect copyToFlash(Rect FlashRect)
	{
		FlashRect.x = x;
		FlashRect.y = y;
		FlashRect.width = width;
		FlashRect.height = height;
		return FlashRect;
	}
	
	/**
	 * Checks to see if some <code>FlxRect</code> object overlaps this <code>FlxRect</code> object.
	 * @param	Rect	The rectangle being tested.
	 * @return	Whether or not the two rectangles overlap.
	 */
	public bool overlaps(FlxRect Rect)
	{
		return (Rect.x + Rect.width > x) && (Rect.x < x + width) && (Rect.y + Rect.height > y) && (Rect.y < y + height);
	}
	
 
	
	public bool containsFlxPoint(Point Point)
	{
		return MathX.pointInFlxRect(Point.x, Point.y, this);
	}
	 
	/**
	 * Add another rectangle to this one by filling in the 
	 * horizontal and vertical space between the two rectangles.
	 * 
	 * @param	Rect	The second FlxRect to add to this one
	 * @return	The changed FlxRect

	public FlxRect union(FlxRect Rect)
	{
		float minX = Math.min(x, Rect.x);
		float minY = Math.min(y, Rect.y);
		float maxX = Math.max(right, Rect.right);
		float maxY = Math.max(bottom, Rect.bottom);
		
		return set(minX, minY, maxX - minX, maxY - minY);
	}*/
}