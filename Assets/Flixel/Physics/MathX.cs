﻿using UnityEngine;
using System.Collections;

public class MathX {
	public static float PI = Mathf.PI;
	public static float log(float f)
	{
		return Mathf.Log(f);
	}
	public static float max(float a,float b){
		return Mathf.Max(a,b);
	}
	public static float min(float a,float b){
		return Mathf.Min(a,b);
	}
	public static float cos(float f){
		return Mathf.Cos(f);
	}
	public static float sin(float f){
		return Mathf.Sin(f);
	}
	public static float atan2(float y,float x){
		return Mathf.Atan2(y,x);
	}
	public static int floor(float f){
		return Mathf.FloorToInt(f);
	}
	public static int ceil(float f){
		return Mathf.CeilToInt(f);
	}
	public static float random(){
		return Random.value;
	}
	public static int toInt(float f){
		return (int)f;
	}
	public static float sqrt(float f){
		return Mathf.Sqrt(f);
	}
	public static float pow(float f,float p){
		return Mathf.Pow (f, p);
	}
	public static int round(float f){
		return Mathf.RoundToInt (f);
	}
	public static float abs(float f){
		return Mathf.Abs (f);
	}

	static public float computeVelocity(float Velocity, float Acceleration, float Drag, float Max, float elapsed)
	{
		if (Acceleration != 0)
		{
			Velocity += Acceleration * elapsed;
		}
		else if(Drag != 0)
		{
			float drag = Drag * elapsed;
			if (Velocity - drag > 0)
			{
				Velocity = Velocity - drag;
			}
			else if (Velocity + drag < 0)
			{
				Velocity += drag;
			}
			else
			{
				Velocity = 0;
			}
		}
		if((Velocity != 0) && (Max != 0))
		{
			if (Velocity > Max)
			{
				Velocity = Max;
			}
			else if (Velocity < -Max)
			{
				Velocity = -Max;
			}
		}
		return Velocity;
	}
 
	static public bool pointInFlxRect(float pointX,float pointY, FlxRect rect)
	{
		if (pointX >= rect.x && pointX <= rect.right && pointY >= rect.y && pointY <= rect.bottom)
		{
			return true;
		}
		return false;
	}

	/**
	 * Returns true if the given x/(float)y coordinate is within the given rectangular block
	 * 
	 * @param	pointX		The X value to test
	 * @param	pointY		The Y value to test
	 * @param	rectX		The X value of the region to test within
	 * @param	rectY		The Y value of the region to test within
	 * @param	rectWidth	The width of the region to test within
	 * @param	rectHeight	The height of the region to test within
	 * 
	 * @return	true if pointX/(float)pointY is within the region, otherwise false
	 */
	static public bool pointInCoordinates(float pointX,float pointY,float rectX,float rectY,float rectWidth,float rectHeight)
	{
		if (pointX >= rectX && pointX <= (rectX + rectWidth))
		{
			if (pointY >= rectY && pointY <= (rectY + rectHeight))
			{
				return true;
			}
		}
		return false;
	}


	public static Point intersectionPoint(float x1,float y1,float x2,float y2,float x3, float y3, float x4,float y4){
		float d = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1);
		float n_a = (x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3);
		float n_b = (x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3);
		
        if (Mathf.Approximately (d, 0)) return null;
 
		float  ua = n_a / d;
		float  ub = n_b / d;
        if (ua >= 0 && ua <= 1 && ub >= 0 && ub <= 1)
        {
            return new Point( x1 + (ua * (x2 - x1)), y1 + (ua * (y2 - y1)));
        }
        return null; 
	}

	public static bool isIntersection(float x1,float y1,float x2,float y2,float x3, float y3, float x4,float y4){
		float d = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1);
		float n_a = (x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3);
		float n_b = (x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3);
		
        if (Mathf.Approximately (d, 0)) return false;
 
		float  ua = n_a / d;
		float  ub = n_b / d;
        if (ua >= 0 && ua <= 1 && ub >= 0 && ub <= 1)
        {
            return true;
        }

        return false; 
	}

	public static bool floatsEqual(float a, float b, float precision = 0.001f){
		return Mathf.Abs (a-b) < precision;
	}

}
