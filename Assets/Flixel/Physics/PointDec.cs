﻿using UnityEngine;
using System.Collections;

public class PointDec {
	public decimal x, y;
	public PointDec(decimal px=0, decimal py=0)
	{
		x = px;
		y = py;
	}

	public PointDec set(decimal xa=0, decimal ya=0){
		x = xa;
		y = ya;
		return this;
	}

	public PointDec setTo(decimal xa=0, decimal ya=0){
		x = xa;
		y = ya;
		return this;
	}

	public PointDec clone(){
		return new PointDec (x, y);
	}

	public void copyFrom(PointDec other){
		x = other.x;
		y = other.y;
	}
}
