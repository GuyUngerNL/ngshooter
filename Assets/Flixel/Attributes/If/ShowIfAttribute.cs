using UnityEngine;
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
public class ShowIfAttribute : PropertyAttribute
{
	public string[] Variables;

	public ShowIfAttribute(params string[] Variables)
	{
		this.Variables = Variables;
	}

	public static bool Validate(object o, params string[] Variables)
	{
		Type type = o.GetType();
		
		foreach(var variable in Variables)
		{
			var field = type.GetField(variable);

			if (field != null && field.FieldType == typeof(bool))
			{
				if((bool)field.GetValue(o))
				{
					continue;
				}
				else
				{
					return false;
				}
			}

			var property = type.GetProperty(variable);

			if (property != null && property.PropertyType == typeof(bool))
			{
				if ((bool)property.GetValue(o, null))
				{
					continue;
				}
				else
				{
					return false;
				}
			}
		}

		return true;
	}
}