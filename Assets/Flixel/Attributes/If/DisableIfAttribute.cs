using UnityEngine;
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
public class DisableIfAttribute : PropertyAttribute
{
	public string[] Variables;

	public DisableIfAttribute(params string[] Variables)
	{
		this.Variables = Variables;
	}
}