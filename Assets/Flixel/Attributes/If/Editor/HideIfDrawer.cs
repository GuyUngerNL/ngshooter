using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

[CustomPropertyDrawer(typeof(HideIfAttribute))]
public class HideIfDrawer : PropertyDrawer
{
	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		return Validate(property.serializedObject.targetObject) ? EditorGUI.GetPropertyHeight(property, label, true) : 0f;
	}

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		if (!Validate(property.serializedObject.targetObject)) return;
		EditorGUI.PropertyField(position, property, label, true);
	}

	bool Validate(object o)
	{
		var attr = attribute as HideIfAttribute;
		return HideIfAttribute.Validate(o, attr.Variables);
	}
}
