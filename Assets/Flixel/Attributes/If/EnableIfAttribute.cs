using UnityEngine;
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
public class EnableIfAttribute : PropertyAttribute
{
	public string[] Variables;

	public EnableIfAttribute(params string[] Variables)
	{
		this.Variables = Variables;
	}
}