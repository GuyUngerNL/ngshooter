using UnityEngine;
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
public class HideIfAttribute : PropertyAttribute
{
	public string[] Variables;

	public HideIfAttribute(params string[] Variables)
	{
		this.Variables = Variables;
	}

	public static bool Validate(object o, params string[] Variables)
	{
		return !ShowIfAttribute.Validate(o, Variables);
	}
}