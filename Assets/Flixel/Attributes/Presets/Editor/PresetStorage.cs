﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PresetStorage : ScriptableObject
{
#if UNITY_EDITOR
	static PresetStorage m_main;

	public static PresetStorage Main
	{
		get
		{
			if (!m_main)
			{
				m_main = UnityEditor.AssetDatabase.LoadAssetAtPath<PresetStorage>("Assets/Flixel/Attributes/Presets/Presets.asset");
			}
			return m_main;
		}
	}

	[System.Serializable]
	public class Item
	{
		public string type;
		public List<string> presetNames;
		public List<string> presetValues;

		public Item(string type)
		{
			this.type = type;

			this.presetNames = new List<string>();
			this.presetValues = new List<string>();
		}
	}

	public List<Item> data;

	public static Dictionary<string, string> GetPresets(string type)
	{
		var data = new Dictionary<string, string>();

		if (Main.data == null) return data;

		foreach(var item in Main.data)
		{
			if(item.type == type)
			{
				for (int i = 0; i < item.presetNames.Count; i++)
				{
					data.Add(item.presetNames[i], item.presetValues[i]);
				}

				return data;
			}
		}

		return data;
	}

	public static void Save(string type, string name, string value)
	{
		if(Main.data == null)
		{
			Main.data = new List<Item>();
		}

		foreach (var item in Main.data)
		{
			if(item.type == type)
			{
				if(item.presetNames == null)
				{
					item.presetNames = new List<string>();
					item.presetValues = new List<string>();
				}

				var index = item.presetNames.IndexOf(name);

				if(index == -1)
				{
					item.presetNames.Add(name);
					item.presetValues.Add(value);
				}
				else
				{
					item.presetNames[index] = name;
					item.presetValues[index] = value;
				}


				return;
			}
		}

		var newItem = new Item(type);

		newItem.presetNames.Add(name);
		newItem.presetValues.Add(value);

		Main.data.Add(newItem);
	}

	public static void Delete(string type, string name)
	{
		if (Main.data == null)
		{
			return;
		}

		foreach (var item in Main.data)
		{
			if(item.type == type)
			{
				var index = item.presetNames.IndexOf(name);

				if(index != -1)
				{
					item.presetNames.RemoveAt(index);
					item.presetValues.RemoveAt(index);
					return;
				}
			}
		}

	}
#endif

	}
