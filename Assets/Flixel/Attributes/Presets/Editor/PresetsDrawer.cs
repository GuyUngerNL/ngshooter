﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(Presets))]
public class PresetsAttributeDrawer : PropertyDrawer
{
	string[] options;
	Dictionary<string, string> presets;

	Rect popupRect;

	Type objectType;

	int selectedIndex = 0;

	public UnityEngine.Object target;

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		position.y += 4f;
		position.height -= 8f;

		GUI.BeginGroup(position, EditorStyles.helpBox);
		GUI.EndGroup();

		target = property.serializedObject.targetObject;
		objectType = target.GetType();

		if (options == null)
		{
			UpdateOptions();
		}

		var currentState = EditorJsonUtility.ToJson(target);

		position.y += 3f;
		position.height -= 3f;
		position.height /= 2f;
		position.width -= 3f;

		var buttonRect = position;
		var toolbarRect = position;

		EditorGUI.BeginChangeCheck();
		selectedIndex = EditorGUI.Popup(buttonRect, "Preset", selectedIndex, options);

		if (EditorGUI.EndChangeCheck())
		{
			if (selectedIndex > 0)
			{
				string data;

				if (presets.TryGetValue(options[selectedIndex], out data))
				{
					Undo.RecordObject(target, "Applied Preset");

					EditorJsonUtility.FromJsonOverwrite(data, target);

					EditorUtility.SetDirty(target);

					property.serializedObject.Update();

					EditorGUIUtility.ExitGUI();
				}
			}
		}

		toolbarRect.y += toolbarRect.height + 0f;
		toolbarRect.x += (int)EditorGUIUtility.labelWidth;
		toolbarRect.width = ((toolbarRect.width - EditorGUIUtility.labelWidth) / 4f);
		toolbarRect.height -= 3;

		EditorGUI.BeginDisabledGroup(selectedIndex == 0);

		if(GUI.Button(toolbarRect, "Apply", EditorStyles.miniButtonLeft))
		{
			PresetStorage.Save(objectType.Name, options[selectedIndex], currentState);
			EditorUtility.SetDirty(PresetStorage.Main);
			UpdateOptions();
			EditorGUIUtility.ExitGUI();
		}

		toolbarRect.x += toolbarRect.width;
		
		if(GUI.Button(toolbarRect, "Revert", EditorStyles.miniButtonMid))
		{
			string data;

			if (presets.TryGetValue(options[selectedIndex], out data))
			{
				Undo.RecordObject(target, "Applied Preset");
				EditorJsonUtility.FromJsonOverwrite(data, target);
				EditorUtility.SetDirty(target);
				property.serializedObject.Update();
				EditorGUIUtility.ExitGUI();
			}
		}

		toolbarRect.x += toolbarRect.width;

		if(GUI.Button(toolbarRect, "Delete", EditorStyles.miniButtonMid))
		{
			if (EditorUtility.DisplayDialog("Delete Preset", "Are you sure you want to delete this preset?", "Delete", "Cancel"))
			{
				PresetStorage.Delete(objectType.Name, options[selectedIndex]);
				EditorUtility.SetDirty(PresetStorage.Main);
				UpdateOptions();
				selectedIndex = 0;
			}
		}

		toolbarRect.x += toolbarRect.width;
		//toolbarRect.width += 3f;

		EditorGUI.EndDisabledGroup();

		if(GUI.Button(toolbarRect, "New", EditorStyles.miniButtonRight))
		{
			var w = new PresetSaveWindow(this, objectType.Name, currentState);
			w.ShowAsDropDown(position, new Vector2(250f, 60f));
		}


	}

	public void UpdateOptions()
	{
		var l = new List<string>();
		l.Add("None");

		presets = PresetStorage.GetPresets(objectType.Name);

		foreach (var p in presets)
		{
			l.Add(p.Key);
		}

		options = l.ToArray();

		EditorUtility.SetDirty(target);
	}

	public void UpdateSelected(string name)
	{
		selectedIndex = Array.IndexOf(options, name);

		if(selectedIndex == -1)
		{
			selectedIndex = 0;
		}

		EditorUtility.SetDirty(target);
	}

	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		return base.GetPropertyHeight(property, label) * 2f + 11f;
	}


	public class PresetSaveWindow : EditorWindow
	{
		string presetName;

		string type;
		string value;
		PresetsAttributeDrawer drawer;

		public PresetSaveWindow(PresetsAttributeDrawer drawer, string type, string value)
		{
			this.drawer = drawer;
			this.type = type;
			this.value = value;
		}

		void OnGUI()
		{
			GUILayout.Label("Save New Preset", EditorStyles.boldLabel);

			presetName = EditorGUILayout.TextField("Name", presetName);

			EditorGUI.BeginDisabledGroup(string.IsNullOrEmpty(presetName) || presetName == "None");

			if(GUILayout.Button("Save"))
			{
				PresetStorage.Save(type, presetName, value);
				EditorUtility.SetDirty(PresetStorage.Main);
				drawer.UpdateOptions();
				drawer.UpdateSelected(presetName);
				this.Close();
			}

			EditorGUI.EndDisabledGroup();
		}
	}



}
