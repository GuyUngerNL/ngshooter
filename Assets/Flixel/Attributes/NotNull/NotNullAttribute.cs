using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
public class NotNullAttribute : PropertyAttribute
{
	public bool throwError;

	public NotNullAttribute(bool throwError = true)
	{
		this.throwError = throwError;
	}
}
