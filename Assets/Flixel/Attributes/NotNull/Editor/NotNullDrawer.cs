using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

[CustomPropertyDrawer(typeof(NotNullAttribute))]
public class NotNullDrawer : PropertyDrawer
{
	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		return EditorGUI.GetPropertyHeight(property, label, true) + (property.objectReferenceValue == null ? 50f : 0f);
	}

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		var attr = attribute as NotNullAttribute;
		var valid = property.objectReferenceValue != null;

		position.height = EditorGUI.GetPropertyHeight(property, label, true);

		EditorGUI.PropertyField(position, property, label, true);

		if(!valid)
		{
			position.y += position.height + 2f;
			position.height = 30f;

			EditorGUI.HelpBox(position, string.Format("{0} should not be null.", label.text), MessageType.Error);

			if(attr.throwError)
			{
				var target = property.serializedObject.targetObject;
				Debug.LogError(string.Format("Null value found on object '{2}' > '{0}.{1}'. Please assign the valid value.", target.GetType().Name, label.text, target.name));
			}
		}
	}
}
