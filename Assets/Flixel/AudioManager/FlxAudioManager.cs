﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;

public enum SoundChannel
{
	SoundEffect	= 0,
	LoudSoundEffect	= 1,
	Music = 2,
	MusicChill = 5,
	MusicCombat = 6,
	PlayerFootsteps = 3,
	PlayerShooting = 4,
}

public enum SoundMasterChannel
{
	SoundEffects,
	Music
}

public class FlxAudioManager : FlxManager
{
	const int SOUND_CHANNELS = 7;
	const string VAR_MUSIC_VOLUME = "MusicVolume";
	const string VAR_SOUND_VOLUME = "SoundVolume";

	[NonSerialized] UUList<FlxAudioAsset> activeAssets = new UUList<FlxAudioAsset>();

	[Header("Sound Channels")]
	public AudioMixer settings;

	public AudioMixerGroup mixerMusic;
	public AudioMixerGroup mixerMusicChill;
	public AudioMixerGroup mixerMusicCombat;
	public AudioMixerGroup mixerSFX;
	public AudioMixerGroup mixerSFXLoud;
	public AudioMixerGroup mixerPlayerFootsteps;
	public AudioMixerGroup mixerPlayerShooting;

	public Dictionary<SoundChannel, SoundChannel[]> mixerParents = new Dictionary<SoundChannel, SoundChannel[]>
	{
		{SoundChannel.Music, new SoundChannel[] { SoundChannel.MusicChill, SoundChannel.MusicCombat } },
		{SoundChannel.SoundEffect, new SoundChannel[] { SoundChannel.PlayerFootsteps, SoundChannel.PlayerShooting } },
	};

	AudioMixerSnapshot musicChillSnapshot;
	AudioMixerSnapshot musicCombatSnapshot;

	[Header("Properties")]
	public float skipIfAlreadyPlaying;

	public void SetMusicVolume(float value)
	{
		baseMusicVolume = value;
	}

	public void SetSoundVolume(float value)
	{
		baseSoundVolume = value;
	}

	float GetVolumeValueInDecibels(float value)
	{
		return Mathf.Pow(1 - value, 3f) * -80f;
	}

	float baseSoundVolume = 0f;
	float baseMusicVolume = 0f;

	float duckSoundVolume = 1f;
	float duckMusicVolume = 1f;

	/// <summary>
	/// </summary>
	/// <param name="volume"></param>
	/// <param name="duration">Total duration of the effect</param>
	/// <param name="fadeIn">Part of duration that will fade in the sounds</param>
	/// <param name="fadeOut">Part of duration that will fade out the sounds</param>
	public void DuckSoundVolume(float volume = 0f, float duration = 0f, float fadeIn = 0f, float fadeOut = 0f)
	{
		StartCoroutine(DuckVolumeCoroutine(true, volume, duration, fadeIn, fadeOut));
	}

	/// <summary>
	/// </summary>
	/// <param name="volume"></param>
	/// <param name="duration">Total duration of the effect</param>
	/// <param name="fadeIn">Part of duration that will fade in the sounds</param>
	/// <param name="fadeOut">Part of duration that will fade out the sounds</param>
	public void DuckMusicVolume(float volume = 0f, float duration = 0f, float fadeIn = 0f, float fadeOut = 0f)
	{
		StartCoroutine(DuckVolumeCoroutine(false, volume, duration, fadeIn, fadeOut));
	}

	IEnumerator DuckVolumeCoroutine(bool soundChannel, float volume = 0f, float duration = 0f, float fadeIn = 0f, float fadeOut = 0f)
	{
		float started = Time.time;

		while (started + duration < Time.time)
		{
			var time = Time.time - started;
			var totalVolume = volume;

			if(time < fadeOut)
			{
				totalVolume *= 1f - Mathf.Clamp01(time / fadeOut);
			}
			else if(time > (duration - fadeIn))
			{
				totalVolume *= Mathf.Clamp01((time - (duration - fadeIn)) / fadeIn);
			}

			if (soundChannel) duckSoundVolume = totalVolume; else duckMusicVolume = totalVolume;

			yield return null;
		}

		if (soundChannel) duckSoundVolume = 1f; else duckMusicVolume = 1f;
	}

	public AudioMixerGroup GetMixer(SoundChannel channel)
	{
		switch(channel)
		{
			case SoundChannel.LoudSoundEffect:
				return mixerSFXLoud;
			case SoundChannel.Music:
				return mixerMusic;
			case SoundChannel.PlayerFootsteps:
				return mixerPlayerFootsteps;
			case SoundChannel.PlayerShooting:
				return mixerPlayerShooting;
			case SoundChannel.MusicChill:
				return mixerMusicChill;
			case SoundChannel.MusicCombat:
				return mixerMusicCombat;
			default:
				return mixerSFX;
		}
	}

	public List<FlxAudioAsset> GetActiveClipsFromChannel(SoundChannel channel)
	{
		List<FlxAudioAsset> filteredActiveClips;

		if (mixerParents.ContainsKey(channel))
		{
			var parentChannels = mixerParents[channel];
			filteredActiveClips = activeAssets
				.Where(x => x.channel == channel || Array.IndexOf(parentChannels, x.channel) != -1)
				.ToList();
		}
		else
		{
			filteredActiveClips = activeAssets.Where(x => x.channel == channel).ToList();
		}

		return filteredActiveClips;
	}

	protected override void Awake()
	{
		base.Awake();

		musicChillSnapshot = settings.FindSnapshot("Snapshot");
		musicCombatSnapshot = settings.FindSnapshot("Snapshot - Combat");
	}

	private void Update()
	{
		for (int i = 0; i < activeAssets.Count; i++)
		{
			var asset = activeAssets[i];
			asset.Update();
		}

		settings.SetFloat(VAR_SOUND_VOLUME, GetVolumeValueInDecibels(baseSoundVolume * duckSoundVolume));
		settings.SetFloat(VAR_MUSIC_VOLUME, GetVolumeValueInDecibels(baseMusicVolume * duckMusicVolume));
	}

	public void SetAssetActive(FlxAudioAsset asset, bool active)
	{
		if(active)
		{
			activeAssets.Add(asset);
		}
		else
		{
			activeAssets.Remove(asset);
		}
	}

	public void FadeInCombatMusic(float duration)
	{
		musicCombatSnapshot.TransitionTo(duration);
	}

	public void FadeInChillMusic(float duration)
	{
		musicChillSnapshot.TransitionTo(duration);
	}

}




