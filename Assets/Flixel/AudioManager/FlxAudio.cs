﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[RequireComponent(typeof(AudioSource))]
public class FlxAudio : FlxComponent
{
	public AudioSource audioSource;

	public float pitch;
	public float volume;

	public float pitchModifier;
	public float volumeModifier;

	public float timeInitialized;
	public float timeStarted;
	public float timeStopped;

	public float fadeIn;
	public float fadeOut;
	public float delay;

	public float clipLength;
	public float currentClipTime;

	public Transform targetTransform;

	[ReadOnly]
	public FlxAudioAsset source;

	private void Awake()
	{
		DontDestroyOnLoad(gameObject);
	}

	public void Init(AudioClip clip, AudioMixerGroup mixer, FlxAudioAsset source)
	{
		if (audioSource.isPlaying)
		{
			audioSource.Stop();
		}

		this.source = source;

		this.timeInitialized = 0f;
		this.timeStarted = 0f;
		this.timeStopped = 0f;

		this.pitch = 1f;
		this.volume = 1f;

		this.pitchModifier = 1f;
		this.volumeModifier = 1f;

		this.fadeIn = 0f;
		this.fadeOut = 0f;
		this.delay = 0f;

		this.targetTransform = null;
		transform.position = Vector3.zero;
		audioSource.panStereo = 0f;

		audioSource.outputAudioMixerGroup = mixer;
		audioSource.loop = false;
		audioSource.clip = clip;

		if(clip && clip.length > 0f)
		{
			clipLength = Mathf.Floor(clip.length * 10f) / 10f;
		}
		else
		{
			StopAndDisable();
		}
	}

	public void Replay()
	{
		flx.exists = true;
		Init(audioSource.clip, audioSource.outputAudioMixerGroup, source);
		Play();
	}

	private void OnDisable()
	{
		audioSource.Stop();
	}

	private void Update()
	{
		if (timeInitialized == 0f) return;

		var duration = Time.time - timeInitialized;

		if(timeStarted == 0f)
		{
			if(duration >= delay)
			{
				audioSource.Play();
				timeStarted = Time.time;
			}
		}

		float volume = this.volume;

		if(fadeIn > 0f)
		{
			volume *= Mathf.Clamp01((duration - delay) / fadeIn);
		}

		if(timeStopped > 0f)
		{
			var fadeOutModifier = 0f;

			if (fadeOut > 0f)
			{
				fadeOutModifier = Mathf.Clamp01(1f - (Time.time - timeStopped) / fadeOut);
			}

			if (fadeOutModifier == 0f)
			{
				StopAndDisable();
				return;
			}
			else
			{
				volume *= fadeOutModifier;
			}
		}
		
		audioSource.volume = volume * volumeModifier;
		audioSource.pitch = pitch * pitchModifier;

		if(targetTransform)
		{
			UpdateStereoPan();
		}

		float currentClipTime = audioSource.time;

		if (timeStarted > 0f && !audioSource.isPlaying && ((currentClipTime >= clipLength) || (currentClipTime == 0)))
		{
			StopAndDisable();
		}
	}

	public void Play()
	{
		timeInitialized = Time.time;
	}

	public void Stop(float fadeOut = 0f)
	{
		timeStopped = Time.time;
		this.fadeOut = fadeOut;

		if (timeInitialized == 0)
		{
			flx.exists = false;
		}
	}
	 
	public void StopAndDisable()
	{
		timeStopped = Time.time;
		audioSource.Stop();
		flx.exists = false;
	}

	public void UpdateStereoPan()
	{
		var position = targetTransform.position - Managers.Camera.transform.position;
		audioSource.panStereo = Mathf.Clamp(position.x / 200f, -1f, 1f);
	}
}