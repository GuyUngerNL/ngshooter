﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public static class FlxEditorAudioUtility
{
	public static bool isPlaying
	{
		get
		{
			foreach (var p in Resources.FindObjectsOfTypeAll<FlxEditorSoundPlayer>())
			{
				if (p.GetComponent<AudioSource>().isPlaying) return true;
			}

			return false;
		}
	}

	public static AudioSource Play(AudioClip clip, float volume = 1f, float pitch = 1f)
	{
		if (clip == null) return null;

		var player = GetPlayer();

		player.volume = volume;
		player.pitch = pitch;
		player.clip = clip;
		player.Play();

		return player;
	}

	public static void StopAll()
	{
		foreach (var p in Resources.FindObjectsOfTypeAll<FlxEditorSoundPlayer>())
		{
			p.GetComponent<AudioSource>().Stop();
		}

		foreach (var p in continuousPlayers)
		{
			p.End();
		}
	}

	static AudioSource GetPlayer()
	{
		AudioSource player = null;

		foreach (var p in Resources.FindObjectsOfTypeAll<FlxEditorSoundPlayer>())
		{
			var a = p.GetComponent<AudioSource>();
			if (!a.isPlaying)
			{
				player = a;
				break;
			}
		}

		if (player == null)
		{
			var go = new GameObject("FlxEditorSoundPlayer", typeof(FlxEditorSoundPlayer), typeof(AudioSource));
			go.hideFlags = HideFlags.HideAndDontSave;
			player = go.GetComponent<AudioSource>();
		}

		return player;
	}



	static HashSet<FlxEditorContinuousGroupPlayer> continuousPlayers = new HashSet<FlxEditorContinuousGroupPlayer>();

	public static void PlayContinuous(FlxAudioGroup flxAudio)
	{
		if (continuousPlayers.Count == 0) EditorApplication.update += UpdateContinuousPlayers;
		continuousPlayers.Add(new FlxEditorContinuousGroupPlayer(flxAudio));
	}

	static void UpdateContinuousPlayers()
	{
		foreach (var p in continuousPlayers)
		{
			p.Update();
		}

		continuousPlayers.RemoveWhere(x => x.ended);

		if (continuousPlayers.Count == 0) EditorApplication.update -= UpdateContinuousPlayers;
	}
}
