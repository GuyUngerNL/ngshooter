using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(FlxAudioClip))]
public class FlxAudioClipEditor : Editor
{

	SerializedProperty spReference;
	SerializedProperty spVolume;
	SerializedProperty spPitch;
	SerializedProperty spLoop;

	void OnEnable()
	{
		spReference = serializedObject.FindProperty("reference");
		spVolume = serializedObject.FindProperty("volume");
		spPitch = serializedObject.FindProperty("pitch");
	}

	public override void OnInspectorGUI()
	{
		serializedObject.Update();
		EditorGUILayout.Space();

		EditorGUILayout.PropertyField (spReference, true);

		EditorGUILayout.PropertyField (spVolume, true);

		EditorGUILayout.PropertyField(spPitch, true);

		serializedObject.ApplyModifiedProperties();
	}

	int testIndex = 0;
	public static double lastTimeTested;
	public static UnityEngine.Object lastTargetPlayed;

	void TestSound()
	{
		FlxAudioClip instance = target as FlxAudioClip;

		if (EditorApplication.timeSinceStartup - lastTimeTested < instance.skipIfAlreadyPlaying)
		{
			return;
		}

		if (EditorApplication.timeSinceStartup - lastTimeTested > instance.resetAfterIdle)
		{
			testIndex = 0;
		}
		else
		{
			testIndex++;
		}

		lastTimeTested = EditorApplication.timeSinceStartup;
		lastTargetPlayed = target;

		var count = instance.clipCount;

		FlxEditorAudioUtility.Play(instance.GetClip(0), instance.GetVolumeModifier(testIndex), instance.GetPitchModifier(testIndex));
	}

	public override bool HasPreviewGUI()
	{
		return true;
	}

	Texture tex;

	public bool playAutomatically
	{
		get
		{
			return SpitfireEditorPreferences.preferences.playClipsAutomatically;
		}
	}

	public override void OnPreviewGUI(Rect r, GUIStyle background)
	{
		if (tex == null)
		{
			tex = EditorGUIUtility.Load("Flixel/Icon-Audio-Play.png") as Texture;
		}
		else
		{
			if (r.Contains(Event.current.mousePosition) && Event.current.type == EventType.mouseDown)
			{
				Event.current.Use();
				TestSound();
			}

			var texRect = new Rect(0, 0, 32f, 32f);
			texRect.x = r.x + (r.width - texRect.width) / 2f;
			texRect.y = r.y + (r.height - texRect.height) / 2f;

			GUI.Button(texRect, new GUIContent(tex));
		}

		if (playAutomatically && lastTargetPlayed != target && (EditorApplication.timeSinceStartup - lastTimeTested) > 0.01f)
		{
			FlxEditorAudioUtility.StopAll();
			TestSound();
		}
	}

	private void OnDisable()
	{
		if(lastTargetPlayed == target)
		{
			FlxEditorAudioUtility.StopAll();
			lastTargetPlayed = null;
		}
	}

	UList<int> pseudoRandomQueue;
}
