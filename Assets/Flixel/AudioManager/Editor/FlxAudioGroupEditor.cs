using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditorInternal;
using System.Linq;

[CustomEditor(typeof(FlxAudioGroup))]
public class FlxAudioGroupEditor : Editor
{
	
	SerializedProperty spListOrder;
	SerializedProperty spResetAfterIdle;
	SerializedProperty spSkipIfAlreadyPlaying;
	SerializedProperty spAudioAssets;


	SerializedProperty spVolumeBase;
	SerializedProperty spVolumeChange;
	SerializedProperty spVolumeRandom;
	SerializedProperty spVolumeMax;
	SerializedProperty spLoop;

	SerializedProperty spPitchBase;
	SerializedProperty spPitchChange;
	SerializedProperty spPitchRandom;
	SerializedProperty spPitchMax;

	SerializedProperty spMaxDeltas;

	SerializedProperty spContinuousIntroClips;
	SerializedProperty spContinuousOutroClips;
	SerializedProperty spContinuousMiddleLength;
	SerializedProperty spContinuousOverlap;
	SerializedProperty spContinuousMiddleOrder;

	ReorderableList rlAudioAssets;

	Texture2D introItemTexture;
	Texture2D outroItemTexture;
	Texture2D middleItemTexture;

	void OnEnable()
	{
		FlxAudioGroup instance = target as FlxAudioGroup;

		introItemTexture = new Texture2D(1, 1);
		introItemTexture.SetPixel(0, 0, new Color(0f, 1f, 0.1f, 0.2f));
		introItemTexture.Apply();

		outroItemTexture = new Texture2D(1, 1);
		outroItemTexture.SetPixel(0, 0, new Color(1f, 0.1f, 0f, 0.2f));
		outroItemTexture.Apply();

		middleItemTexture = new Texture2D(1, 1);
		middleItemTexture.SetPixel(0, 0, new Color(1f, 1f, 0f, 0.2f));
		middleItemTexture.Apply();

		spListOrder = serializedObject.FindProperty("_listOrder");
		spResetAfterIdle = serializedObject.FindProperty("_resetAfterIdle");
		spSkipIfAlreadyPlaying = serializedObject.FindProperty("_skipIfAlreadyPlaying");
		spAudioAssets = serializedObject.FindProperty("audioAssets");

		spVolumeBase = serializedObject.FindProperty("volumeBase");
		spVolumeChange = serializedObject.FindProperty("volumeDelta");
		spVolumeRandom = serializedObject.FindProperty("volumeRandom");
		spVolumeMax = serializedObject.FindProperty("volumeMaxValue");

		spPitchBase = serializedObject.FindProperty("pitchBase");
		spPitchChange = serializedObject.FindProperty("pitchDelta");
		spPitchRandom = serializedObject.FindProperty("pitchRandom");
		spPitchMax = serializedObject.FindProperty("pitchMaxValue");

		spMaxDeltas = serializedObject.FindProperty("maxDeltas");
		spLoop = serializedObject.FindProperty("loop");

		spContinuousIntroClips = serializedObject.FindProperty("continuousIntroClips");
		spContinuousOutroClips = serializedObject.FindProperty("continuousOutroClips");
		spContinuousMiddleLength = serializedObject.FindProperty("continuousMiddleLength");
		spContinuousOverlap = serializedObject.FindProperty("continuousOverlap");
		spContinuousMiddleOrder = serializedObject.FindProperty("continuousMiddleOrder");


		rlAudioAssets = new ReorderableList(serializedObject, spAudioAssets, true, true, true, true);

		rlAudioAssets.drawHeaderCallback = (Rect rect) => {
			EditorGUI.LabelField(rect, "Audio Clips");
		};

		rlAudioAssets.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
		{
			if (instance.listOrder == FlxAudioGroup.PlayMode.Continuous)
			{
				Rect bcgRect = rect;
				bcgRect.x -= 18f;
				bcgRect.width += 22f;

				if (index == 0)
				{
					bcgRect.y -= 2f;
					bcgRect.height += 2f;
				}

				if (index < spContinuousIntroClips.intValue)
				{
					GUI.DrawTexture(bcgRect, introItemTexture);
				}
				else if (spAudioAssets.arraySize - index <= spContinuousOutroClips.intValue)
				{
					GUI.DrawTexture(bcgRect, outroItemTexture);
				}
				else
				{
					GUI.DrawTexture(bcgRect, middleItemTexture);
				}
			}

			var element = spAudioAssets.GetArrayElementAtIndex(index);
			rect.y += 2f;
			rect.height = EditorGUIUtility.singleLineHeight;
			element.objectReferenceValue = EditorGUI.ObjectField(rect, element.objectReferenceValue, typeof(FlxAudioClip), false);
		};

		rlAudioAssets.onChangedCallback = (ReorderableList list) =>
		{
			list.serializedProperty.serializedObject.ApplyModifiedProperties();
			instance.audioClips = instance.audioAssets.Select(x => x == null ? null : x.reference).ToArray();
			EditorUtility.SetDirty(instance);
		};

	}

	public override void OnInspectorGUI()
	{
		FlxAudioGroup instance = target as FlxAudioGroup;

		serializedObject.Update();
		EditorGUILayout.Space();

		EditorGUILayout.LabelField("Volume", EditorStyles.boldLabel);

		EditorGUILayout.PropertyField(spVolumeBase, true);
		EditorGUILayout.PropertyField(spVolumeChange, true);
		EditorGUILayout.PropertyField(spVolumeRandom, true);
		EditorGUILayout.PropertyField(spVolumeMax, true);

		EditorGUILayout.Space();

		EditorGUILayout.LabelField("Volume", EditorStyles.boldLabel);

		EditorGUILayout.PropertyField(spPitchBase, true);
		EditorGUILayout.PropertyField(spPitchChange, true);
		EditorGUILayout.PropertyField(spPitchRandom, true);
		EditorGUILayout.PropertyField(spPitchMax, true);

		EditorGUILayout.Space();

		EditorGUILayout.PropertyField(spResetAfterIdle, true);
		EditorGUILayout.PropertyField(spSkipIfAlreadyPlaying, true);

		EditorGUILayout.Space();
		EditorGUILayout.Space();

		EditorGUILayout.PropertyField(spListOrder, true);

		rlAudioAssets.DoLayoutList();

		serializedObject.ApplyModifiedProperties();
		serializedObject.Update();
			
		EditorGUILayout.Space();

		if(instance.listOrder == FlxAudioAsset.PlayMode.Continuous)
		{
			EditorGUILayout.LabelField("Continuous List Settings", EditorStyles.boldLabel);

			EditorGUILayout.PropertyField(spContinuousMiddleOrder, new GUIContent("Order"));
			EditorGUILayout.PropertyField(spContinuousOverlap, new GUIContent("Overlap", "In seconds"));
			EditorGUILayout.PropertyField(spContinuousMiddleLength, new GUIContent("Middle Length", "In seconds"));

			spContinuousOverlap.floatValue = Mathf.Max(spContinuousOverlap.floatValue, 0f);
			spContinuousMiddleLength.floatValue = Mathf.Max(spContinuousMiddleLength.floatValue, 0f);

			EditorGUILayout.Space();

			spContinuousIntroClips.intValue = EditorGUILayout.IntSlider(new GUIContent("Intro Clips"), spContinuousIntroClips.intValue, 0, spAudioAssets.arraySize - spContinuousOutroClips.intValue);
			spContinuousIntroClips.intValue = Mathf.Clamp(spContinuousIntroClips.intValue, 0, spAudioAssets.arraySize - spContinuousOutroClips.intValue);

			spContinuousOutroClips.intValue = EditorGUILayout.IntSlider(new GUIContent("Outro Clips"), spContinuousOutroClips.intValue, 0, spAudioAssets.arraySize - spContinuousIntroClips.intValue);
			spContinuousOutroClips.intValue = Mathf.Clamp(spContinuousOutroClips.intValue, 0, spAudioAssets.arraySize - spContinuousIntroClips.intValue);

			EditorGUILayout.Space();

			serializedObject.ApplyModifiedProperties();
		}


		EditorGUILayout.Space();

		if (spAudioAssets.arraySize > 0)
		{
			if (testIndex > 0 && (EditorApplication.timeSinceStartup - lastTimeTested) > instance.resetAfterIdle)
			{
				testIndex = 0;
				Repaint();
			}

			EditorGUILayout.BeginHorizontal();

			if(instance.skipIfAlreadyPlaying > 0f)
			{
				if (GUILayout.RepeatButton(string.Format("Play Sound")))
				{
					TestSound();
				}
			}
			else
			{
				if (GUILayout.Button(string.Format("Play Sound")))
				{
					TestSound();
				}
			}

			EditorGUI.BeginDisabledGroup(instance.resetAfterIdle == 0);

			if (GUILayout.Button(string.Format("Reset Index ({0})", testIndex)))
			{
				testIndex = 0;
			}

			EditorGUI.EndDisabledGroup();


			EditorGUI.BeginDisabledGroup(!FlxEditorAudioUtility.isPlaying);

			if (GUILayout.Button("Stop All"))
			{
				FlxEditorAudioUtility.StopAll();
			}

			EditorGUI.EndDisabledGroup();

			EditorGUILayout.EndHorizontal();
		}

		Repaint();
	}

	int testIndex = 0;
	public static double lastTimeTested;
	public static UnityEngine.Object lastTargetPlayed;

	void TestSound()
	{
		FlxAudioGroup instance = target as FlxAudioGroup;

		if (EditorApplication.timeSinceStartup - lastTimeTested < instance.skipIfAlreadyPlaying)
		{
			return;
		}

		if (EditorApplication.timeSinceStartup - lastTimeTested > instance.resetAfterIdle)
		{
			testIndex = 0;
		}
		else
		{
			testIndex++;
		}

		lastTimeTested = EditorApplication.timeSinceStartup;
		lastTargetPlayed = target;

		var count = instance.clipCount;

		switch (instance.listOrder)
		{
			case FlxAudioAsset.PlayMode.Layered:
				for (int i = 0; i < count; i++)
				{
					FlxEditorAudioUtility.Play(instance.GetClip(i), instance.GetVolumeModifier(testIndex), instance.GetPitchModifier(testIndex));
				}
				break;
			case FlxAudioAsset.PlayMode.Continuous:
				FlxEditorAudioUtility.PlayContinuous(instance);
				break;
			default:
				FlxEditorAudioUtility.Play(instance.GetClip(GetClipIndex(testIndex)), instance.GetVolumeModifier(testIndex), instance.GetPitchModifier(testIndex));
				break;
		}
	}

	public override bool HasPreviewGUI()
	{
		return true;
	}

	Texture tex;

	public bool playAutomatically
	{
		get
		{
			return SpitfireEditorPreferences.preferences.playClipsAutomatically;
		}
	}

	public override void OnPreviewGUI(Rect r, GUIStyle background)
	{
		if(tex == null)
		{
			tex = EditorGUIUtility.Load("Flixel/Icon-Audio-Play.png") as Texture;
		}
		else
		{
			if (r.Contains(Event.current.mousePosition) && Event.current.type == EventType.mouseDown)
			{
				Event.current.Use();
				TestSound();
			}

			var texRect = new Rect(0, 0, 32f, 32f);
			texRect.x = r.x + (r.width - texRect.width) / 2f;
			texRect.y = r.y + (r.height - texRect.height) / 2f;

			GUI.Button(texRect, new GUIContent(tex));
		}


		if (playAutomatically && lastTargetPlayed != target && (EditorApplication.timeSinceStartup - lastTimeTested) > 0.02f)
		{
			FlxEditorAudioUtility.StopAll();
			TestSound();
		}
	}

	private void OnDisable()
	{
		if (lastTargetPlayed == target)
		{
			FlxEditorAudioUtility.StopAll();
			lastTargetPlayed = null;
		}
	}


	UList<int> pseudoRandomQueue;

	public int GetClipIndex(int previous)
	{
		FlxAudioGroup asset = target as FlxAudioGroup;

		int count = asset.clipCount;

		if (count == 0) return -1;
		if (count == 1) return 0;

		var index = previous;

		switch (asset.listOrder)
		{
			case FlxAudioAsset.PlayMode.PseudoRandom:
				if (pseudoRandomQueue == null || pseudoRandomQueue.Count != asset.clipCount)
				{
					pseudoRandomQueue = new UList<int>(asset.clipCount);

					for (int i = 0; i < count; i++)
					{
						pseudoRandomQueue.Add(i);
					}

					pseudoRandomQueue.Shuffle();
				}

				index = pseudoRandomQueue[count - 1];

				float chanceRnd = UnityEngine.Random.Range(0f, 0.99f);
				int countRnd = Mathf.RoundToInt((count - 2) * (chanceRnd * chanceRnd));

				pseudoRandomQueue.RemoveAt(count - 1);
				pseudoRandomQueue.Insert(countRnd, index);
				break;
			case FlxAudioAsset.PlayMode.Clamp:
				index = Mathf.Clamp(index, 0, count - 1);
				break;
			case FlxAudioAsset.PlayMode.Repeat:
				index = index % count;
				break;
			case FlxAudioAsset.PlayMode.PingPong:
				int countAlt = count - 1;
				index = index % (countAlt * 2);
				if (index > countAlt) index = countAlt - index % countAlt;
				break;
			default:
				index = index % count;
				break;
		}

		return index;
	}

}

public class FlxEditorContinuousGroupPlayer
{
	FlxAudioGroup flxAudio;
	HashSet<AudioSource> players = new HashSet<AudioSource>();

	public bool looping;
	public bool startedLoop;
	public bool ended;

	double loopLength;
	double loopStartedTime;
	PseudoRandom<int> pseudoRandomChance;
	int index = 0;

	public FlxEditorContinuousGroupPlayer(FlxAudioGroup flxAudio)
	{
		this.flxAudio = flxAudio;

		if (flxAudio == null || flxAudio.listOrder != FlxAudioAsset.PlayMode.Continuous) End();
		else
		{
			pseudoRandomChance = new PseudoRandom<int>();

			for (int i = 0; i < flxAudio.continuousMiddleClips; i++)
			{
				pseudoRandomChance.Add(i);
			}

			pseudoRandomChance.Shuffle();

			if (flxAudio.continuousIntroClips == 0)
			{
				StartLoop();
			}
			else
			{
				for (int i = 0; i < flxAudio.continuousIntroClips; i++)
				{
					players.Add(FlxEditorAudioUtility.Play(flxAudio.GetClip(i)));
				}
			}
		}
	}

	public void Update()
	{
		if (ended) return;

		bool playersStillPlaying = false;

		foreach (var p in players)
		{
			if (p.isPlaying && p.time + flxAudio.continuousOverlap < p.clip.length ) playersStillPlaying = true;
		}

		if (!startedLoop)
		{
			if(!playersStillPlaying) StartLoop();
		}
		else if(looping)
		{
			var time = EditorApplication.timeSinceStartup - loopStartedTime;

			if (time > loopLength) Stop();
			else if (players.Count == 0) Stop();
			else if (!playersStillPlaying) PlayNextLoop();
		}
		else
		{
			if (!playersStillPlaying) End();
		}
	}

	public void StartLoop()
	{
		startedLoop = true;
		looping = true;
		loopLength = flxAudio.continuousMiddleLength == 0 ? double.MaxValue : flxAudio.continuousMiddleLength;

		PlayNextLoop();

		loopStartedTime = EditorApplication.timeSinceStartup;
	}

	public void PlayNextLoop()
	{
		players.Clear();
		
		if(flxAudio.continuousMiddleOrder == FlxAudioAsset.ContinuousPlayMode.Layered)
		{
			for (int i = 0; i < flxAudio.continuousMiddleClips; i++)
			{
				players.Add(FlxEditorAudioUtility.Play(flxAudio.GetClip(i + flxAudio.continuousIntroClips)));
			}
		}
		else
		{
			var i = GetClipIndex();
			if(i >= 0)
			{
				players.Add(FlxEditorAudioUtility.Play(flxAudio.GetClip(i)));
			}
		}
		index++;
	}

	public void Stop()
	{
		looping = false;

		for (int i = 0; i < flxAudio.continuousOutroClips; i++)
		{
			players.Add(FlxEditorAudioUtility.Play(flxAudio.GetClip(flxAudio.clipCount - flxAudio.continuousOutroClips + i)));
		}
	}

	public void End()
	{
		ended = true;

		foreach(var p in players)
		{
			p.Stop();
		}
	}

	public int GetClipIndex()
	{
		int count = flxAudio.continuousMiddleClips;

		if (count == 0) return -1;
		if (count == 1) return flxAudio.continuousIntroClips;

		var index = this.index;

		switch (flxAudio.continuousMiddleOrder)
		{
			case FlxAudioAsset.ContinuousPlayMode.PseudoRandom:
				index = pseudoRandomChance.next;
				break;
			case FlxAudioAsset.ContinuousPlayMode.Clamp:
				index = Mathf.Clamp(index, 0, count - 1);
				break;
			case FlxAudioAsset.ContinuousPlayMode.Repeat:
				index = index % count;
				break;
			case FlxAudioAsset.ContinuousPlayMode.PingPong:
				int countAlt = count - 1;
				index = index % (countAlt * 2);
				if (index > countAlt) index = countAlt - index % countAlt;
				break;
			default:
				index = index % count;
				break;
		}

		return index + flxAudio.continuousIntroClips;
	}
}