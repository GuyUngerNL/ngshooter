using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

[CustomEditor(typeof(FlxAudioCollection))]
public class FlxAudioCollectionEditor : Editor
{
	SerializedProperty spFolders;
	ReorderableList folderList;

	SerializedProperty spGroupsFolder;
	string groupsFolder;
	

	int clipsInProject;
	int clipsInAsset;

	void OnEnable()
	{
		spFolders = serializedObject.FindProperty("folders");

		folderList = new ReorderableList(serializedObject, spFolders, true, true, true, true);

		folderList.drawHeaderCallback = (Rect rect) => {
			EditorGUI.LabelField(rect, "Audio Source Folders");
		};

		folderList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
		{
			var element = spFolders.GetArrayElementAtIndex(index);
			rect.y += 2f;
			rect.width -= 70f;
			rect.height = EditorGUIUtility.singleLineHeight;

			EditorGUI.BeginDisabledGroup(true);
			EditorGUI.TextField(rect, element.stringValue);
			EditorGUI.EndDisabledGroup();

			rect.x += rect.width + 10f;
			rect.width = 60f;

			var dirSeparator = System.IO.Path.DirectorySeparatorChar.ToString();

			if (GUI.Button(rect, "Browse"))
			{
				var sourceFolder = Application.dataPath + dirSeparator + element.stringValue.Replace("/", dirSeparator);
				var targetFolder = EditorUtility.OpenFolderPanel(name, sourceFolder, "Folder");

				if (string.IsNullOrEmpty(targetFolder))
				{

				}
				else if (!targetFolder.StartsWith(Application.dataPath))
				{
					EditorUtility.DisplayDialog("Invalid Folder", "Please select folder inside this project.", "Ok");
				}
				else
				{
					targetFolder = targetFolder.Substring(Application.dataPath.Length - 6);
					targetFolder = targetFolder.Replace(dirSeparator, "/");
					element.stringValue = targetFolder;
				}
			}
		};


		UpdateAssetCount();


		spGroupsFolder = serializedObject.FindProperty("groupsFolder");
		if(spGroupsFolder.stringValue != null)
			groupsFolder = spGroupsFolder.stringValue;
	}

	void UpdateAssetCount()
	{
		FlxAudioCollection instance = target as FlxAudioCollection;
		clipsInProject = AssetDatabase.FindAssets("t:AudioClip", instance.folders.ToArray()).Count() + AssetDatabase.FindAssets("t:FlxAudioGroup", instance.folders.ToArray()).Count();
		clipsInAsset = instance.soundAssets.Length + instance.soundGroups.Length;

		FindGeneratedGroups();
	}

	public override void OnInspectorGUI()
	{
		FlxAudioCollection instance = target as FlxAudioCollection;

		serializedObject.Update();
		EditorGUILayout.Space();

		folderList.DoLayoutList();

		EditorGUILayout.Space();

		serializedObject.ApplyModifiedProperties();

		if(GUI.changed)
		{
			UpdateAssetCount();
		}

		EditorGUILayout.HelpBox(string.Format("Audio files in project: {0}\nCached audio clips: {1}", clipsInProject, clipsInAsset), clipsInProject == clipsInAsset ? MessageType.Info : MessageType.Warning);

		EditorGUI.BeginDisabledGroup(EditorApplication.isCompiling);
		if (GUILayout.Button("Generate Class"))
		{
			GenerateClass(instance);
			AssetDatabase.Refresh();
		}
		EditorGUI.EndDisabledGroup();

		if (EditorApplication.isCompiling)
		{
			EditorGUILayout.HelpBox("Cannot generate class while scripts are compiling.", MessageType.Warning);
		}

		GUILayout.Space(10);

		DrawGroupSelect();

		if (GUILayout.Button("Generate Groups"))
		{
			GenerateGroups(instance);
		}
	}


	[MenuItem("Gungrounds/Audio Settings")]
	public static void EditSettings()
	{
		var assets = AssetDatabase.FindAssets("t:FlxAudioCollection");

		foreach (var a in assets)
		{
			var asset = AssetDatabase.LoadAssetAtPath<FlxAudioCollection>(AssetDatabase.GUIDToAssetPath(a));

			if (asset)
			{
				Selection.activeObject = asset;
			}
		}
	}

	public static void GenerateClass(FlxAudioCollection settings)
	{
		var clips = AssetDatabase.FindAssets("t:AudioClip", settings.folders.ToArray()).Select(x => AssetDatabase.LoadAssetAtPath<AudioClip>(AssetDatabase.GUIDToAssetPath(x))).ToArray();

		var existingClipAssets = AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(settings)).Select(x => x as FlxAudioClip).Where(x => x != null);

		foreach(var clip in clips)
		{
			FlxAudioClip newAsset = null;

			foreach (var ca in existingClipAssets)
			{
				if(ca.reference == clip)
				{
					newAsset = ca;
					break;
				}
			}

			if(newAsset == null)
			{
				newAsset = ScriptableObject.CreateInstance<FlxAudioClip>();
				newAsset.reference = clip;
				AssetDatabase.AddObjectToAsset(newAsset, settings);
			}

			newAsset.name = clip.name;
		}

		var unusedClipAssets = AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(settings)).Select(x => x as FlxAudioClip).Where(x => x != null && x.reference == null);

		foreach(var ca in unusedClipAssets)
		{
			GameObject.DestroyImmediate(ca, true);
		}

		EditorUtility.SetDirty(settings);
		AssetDatabase.SaveAssets();
		AssetDatabase.Refresh();

		var clipAssets = AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(settings)).Select(x => x as FlxAudioClip).Where(x => x != null).ToArray();

		var groups = AssetDatabase.FindAssets("t:FlxAudioGroup", settings.folders.ToArray()).Select(x => AssetDatabase.LoadAssetAtPath<FlxAudioGroup>(AssetDatabase.GUIDToAssetPath(x))).ToArray();

		settings.soundAssets = clipAssets;
		settings.soundGroups = groups;

		EditorUtility.SetDirty(settings);

		string classContents = GetClassContents(settings, clipAssets, groups);

		string settingsPath = AssetDatabase.GetAssetPath(settings);
		string settingsDir = System.IO.Path.GetDirectoryName(settingsPath);
		string classPath = settingsDir + System.IO.Path.DirectorySeparatorChar + "Sounds.cs";

		File.WriteAllText(classPath, classContents);
		AssetImporter.GetAtPath(classPath);
		
	}


	private static string GetClassContents(FlxAudioCollection settings, FlxAudioClip[] clips, FlxAudioGroup[] groups)
	{
		StringBuilder text = new StringBuilder();

		text.AppendLine("using UnityEngine;");
		text.AppendLine("using System.Collections.Generic;");
		text.AppendLine();
		text.AppendLine("/// <summary>");
		text.AppendLine("/// This is auto-generated class with the list of sound assets in project.");
		text.AppendLine("/// DO NOT edit this class.");
		text.AppendLine("/// </summary>");

		text.AppendLine("public static class Sounds");
		text.AppendLine("{");

		for (int i = 0; i < clips.Length; i++)
		{
			text.AppendLine(string.Format("  public static FlxAudioAsset {0} {{ get {{ if(__{0} == null) {{ __{0} = GameObject.Instantiate(FlxAudioCollection.main.soundAssets[{1}]); }} return __{0}; }} }} static FlxAudioAsset __{0}; ", GetVariableName(clips[i].name), i));
		}

		text.AppendLine("  ");

		for (int i = 0; i < groups.Length; i++)
		{
			text.AppendLine(string.Format("  public static FlxAudioAsset {0} {{ get {{ if(__{0} == null) {{ __{0} = GameObject.Instantiate(FlxAudioCollection.main.soundGroups[{1}]); }} return __{0}; }} }} static FlxAudioAsset __{0}; ", GetVariableName(groups[i].name), i));
		}

		text.AppendLine("}");

		return text.ToString();
	}

	static string GetVariableName(string name)
	{
		var index = name.LastIndexOf(System.IO.Path.DirectorySeparatorChar);
		name = index < 0 ? name : name.Substring(index);
		char[] arr = name.Where(c => ((int)c) < 128 && (char.IsLetterOrDigit(c))).ToArray();
		return new string(arr);
	}


	public static void GenerateMainAudioGroups()
	{
		var mainCollection = AssetDatabase.LoadAssetAtPath<FlxAudioCollection>(FlxAudioCollection.DEFAULT_ASSET_PATH);
		GenerateGroups(mainCollection);
	}

	void GenerateCurrentGroups()
	{
		FlxAudioCollection instance = target as FlxAudioCollection;
		GenerateGroups(instance);
		UpdateAssetCount();
	}

	static void GenerateGroups(FlxAudioCollection settings)
	{
		EditorUtility.DisplayProgressBar("Generate Audio Groups", "Generating Audio Groups", 0f);

		var existingClipAssets = AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(settings)).Select(x => x as FlxAudioClip).Where(x => x != null);
		Dictionary<string, List<FlxAudioClip>> groups = new Dictionary<string, List<FlxAudioClip>>();

		foreach(var a in existingClipAssets)
		{
			var result = Regex.Match(a.name, @"\d+$").Value;
			if (string.IsNullOrEmpty(result)) continue;
			var groupName = a.name.Substring(0, a.name.Length - result.Length - 1);
			if (!groups.ContainsKey(groupName)) groups.Add(groupName, new List<FlxAudioClip>());
			groups[groupName].Add(a);
		}

		foreach(var g in groups)
		{
			var groupPath = settings.groupsFolder + "/" + g.Key + ".asset";
			var asset = AssetDatabase.LoadAssetAtPath<FlxAudioGroup>(groupPath);

			if(!asset)
			{
				asset = ScriptableObject.CreateInstance<FlxAudioGroup>();
				asset.listOrder = FlxAudioGroup.PlayMode.PseudoRandom;
				AssetDatabase.CreateAsset(asset, groupPath);
			}

			asset.audioAssets = g.Value;
			asset.audioClips = asset.audioAssets.Select(x => x ? x.reference : null).ToArray();

			EditorUtility.SetDirty(asset);
		}

		GenerateClass(settings);

		EditorUtility.ClearProgressBar();
	}

	Dictionary<string, List<String>> FindGeneratedGroups()
	{
		FlxAudioCollection instance = target as FlxAudioCollection;

		string[] soundFiles = AssetDatabase.FindAssets("t:AudioClip", instance.folders.ToArray());
		Dictionary<string, List<String>> soundGroups = new Dictionary<string, List<String>>();
		for (int i = 0; i < soundFiles.Length; i++)
		{
			string filePath = AssetDatabase.GUIDToAssetPath(soundFiles[i]);
			string filePathWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(filePath);
			
			char character = filePathWithoutExtension.charAt(filePathWithoutExtension.Length - 1);
			
			if (System.Char.IsDigit(character))
			{
				string fileGroupName = filePathWithoutExtension.Substring(0, filePathWithoutExtension.Length - 2);
				
				if (soundGroups.ContainsKey(fileGroupName) == false)
					soundGroups.Add(fileGroupName, new List<string>());

				soundGroups[fileGroupName].Add(filePath);
				
			}
		}
		
		return soundGroups;
	}
	
	void DrawGroupSelect()
	{
		EditorGUILayout.LabelField("Group Output Folder", EditorStyles.boldLabel);

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField(groupsFolder);

		if (GUILayout.Button("Browse"))
		{
			var dirSeparator = System.IO.Path.DirectorySeparatorChar.ToString();
			var sourceFolder = Application.dataPath + dirSeparator + groupsFolder.Replace("/", dirSeparator);
			var targetFolder = EditorUtility.OpenFolderPanel(name, sourceFolder, "Folder");

			if (string.IsNullOrEmpty(targetFolder))
			{

			}
			else if (!targetFolder.StartsWith(Application.dataPath))
			{
				EditorUtility.DisplayDialog("Invalid Folder", "Please select folder inside this project.", "Ok");
			}
			else
			{
				targetFolder = targetFolder.Substring(Application.dataPath.Length - 6);
				targetFolder = targetFolder.Replace(dirSeparator, "/");
				groupsFolder = targetFolder;
				spGroupsFolder.stringValue = targetFolder;
				serializedObject.ApplyModifiedProperties();
			}
		}

		EditorGUILayout.EndHorizontal();

	}
}