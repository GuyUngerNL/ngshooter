﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FlxAudioAsset : ScriptableObject
{
	public enum PlayMode
	{
		PseudoRandom = 0,
		Clamp = 1,
		Repeat = 2,
		PingPong = 3,
		Layered = 4,
		Continuous = 5,
	}

	public enum ContinuousPlayMode
	{
		PseudoRandom = 0,
		Clamp = 1,
		Repeat = 2,
		PingPong = 3,
		Layered = 4,
	}

	public virtual PlayMode listOrder { get { return PlayMode.Repeat; } set { } }
	public virtual int clipCount { get { return 1; } }
	public virtual float resetAfterIdle { get { return 0f; } set { } }
	public virtual float skipIfAlreadyPlaying
	{
		get
		{
#if UNITY_EDITOR
			return Application.isPlaying ? Managers.AudioPlayer.skipIfAlreadyPlaying : 0f;
#else
			return Managers.AudioPlayer.skipIfAlreadyPlaying;
#endif
		}
		set { }
	}

	public abstract AudioClip GetClip(int index);
	public abstract float GetVolumeModifier(int index);
	public abstract float GetPitchModifier(int index);

	#region Playback

	public bool isPlaying => p_player != null;
	public SoundChannel channel => p_channel;

	[NonSerialized] int p_index = 0;

	[NonSerialized] bool p_loop = false;
	[NonSerialized] SoundChannel p_channel;

	[NonSerialized] float p_volume = 1f;
	[NonSerialized] float p_pitch = 1f;

	[NonSerialized] float p_delay = 0f;

	[NonSerialized] bool p_paused = false;
	[NonSerialized] bool p_stopped = false;

	[NonSerialized] float p_timeStarted = -1f;
	[NonSerialized] float p_elapsed;

	[NonSerialized] GameObject p_target = null;
	[NonSerialized] AudioClip p_lastPlayedClip = null;
	[NonSerialized] FlxSoundPlayer p_player = null;

	[NonSerialized] float p_fadeFrom = 1f;
	[NonSerialized] float p_fadeTo = 1f;

	[NonSerialized] float p_fadeDuration = 0f;
	[NonSerialized] float p_fadeStart = 0f; // Time

	float fadeModifier
	{
		get
		{
			if (p_fadeDuration > 0f)
			{
				float start = p_fadeStart + p_delay;

				if (Time.unscaledTime == start) return p_fadeFrom;

				float fadeModifier = (Time.unscaledTime - start) / p_fadeDuration;
				return Mathf.Lerp(p_fadeFrom, p_fadeTo, fadeModifier);
			}

			return p_fadeTo;
		}
	}

	public void Play
	(
		SoundChannel channel = SoundChannel.SoundEffect,
		float volume = 1f,
		float pitch = 1f,
		bool loop = false,
		float delay = 0f,
		float fadeIn = 0f,
		GameObject targetObject = null,
		bool stopIfAlreadyPlaying = false
	)
	{
		float time = Time.unscaledTime;

		if (time - p_timeStarted < skipIfAlreadyPlaying) return;

		if (stopIfAlreadyPlaying && p_player != null)
		{
			p_player.Stop();
			Clear();
		}

		p_index = (time - p_timeStarted > resetAfterIdle) ? 0 : (p_index + 1);

		p_timeStarted = time;
		p_elapsed = 0f;

		p_channel = channel;

		p_volume = volume * GetVolumeModifier(p_index);
		p_pitch = pitch * GetPitchModifier(p_index);

		p_delay = delay;

		p_fadeFrom = 0f;
		p_fadeTo = 1f;
		p_fadeDuration = fadeIn;

		p_fadeStart = time;

		p_loop = loop;
		p_target = targetObject;

		p_paused = false;
		p_stopped = false;

		if (p_player != null)
		{
			p_player.asset = null;
		}

		p_player = Prefabs.FlxSoundPlayer.Recycle<FlxSoundPlayer>();
		p_lastPlayedClip = GetClip(p_index);

		Managers.AudioPlayer.SetAssetActive(this, true);
	}

	public void Pause(float delay = 0f, float fadeOut = 0f)
	{
		if (p_player == null) return;

		p_paused = true;

		p_fadeFrom = fadeModifier;
		p_fadeTo = 0f;
		p_fadeStart = Time.unscaledTime;
		p_fadeDuration = fadeOut;
		p_delay = delay;
	}

	public void Resume(float delay = 0f, float fadeIn = 0f)
	{
		if (p_player == null) return;

		p_paused = false;

		p_fadeFrom = fadeModifier;
		p_fadeTo = 1f;
		p_fadeStart = Time.unscaledTime;
		p_fadeDuration = fadeIn;
		p_delay = delay;
	}

	public void Stop(float delay = 0f, float fadeOut = 0f)
	{
		if (p_player == null) return;

		if(delay == 0f && fadeOut == 0f)
		{
			p_player.Stop();
			Clear();
			return;
		}

		p_stopped = true;

		p_fadeFrom = fadeModifier;
		p_fadeTo = 0f;
		p_fadeStart = Time.unscaledTime;
		p_fadeDuration = fadeOut;
		p_delay = delay;
	}

	public FlxAudioAsset Fade(float from, float to, float duration)
	{
		p_fadeFrom = from;
		p_fadeTo = to;
		p_fadeDuration = duration;
		p_fadeStart = Time.unscaledTime;

		return this;
	}

	public FlxAudioAsset Loop(bool loop = true)
	{
		p_loop = loop;
		return this;
	}

	public FlxAudioAsset Volume(float volume)
	{
		p_volume = volume * GetVolumeModifier(p_index);
		return this;
	}

	public FlxAudioAsset Pitch(float pitch)
	{
		p_pitch = pitch * GetPitchModifier(p_index);
		return this;
	}

	void Clear()
	{
		if (p_player == null) return;

		p_target = null;
		p_player.asset = null;
		p_player = null;
	}

	public FlxAudioAsset Clone(bool resetIndex = true)
	{
		var clone = Instantiate(this);

		clone.p_player = null;
		clone.p_lastPlayedClip = null;
		clone.p_target = null;

		if (resetIndex)
		{
			clone.ResetIndex();
		}

		return clone;
	}

	public void ResetIndex()
	{
		p_index = 0;
	}

	public void Update()
	{
		if (p_player == null || p_lastPlayedClip == null)
		{
			Managers.AudioPlayer.SetAssetActive(this, false);
			return;
		}

		float time = Time.unscaledDeltaTime;

		var fade = fadeModifier;

		p_player.volume = p_volume * fade;
		p_player.pitch = p_pitch;
		p_player.loop = p_loop;

		if (p_delay > 0f)
		{
			p_delay -= time;
			p_fadeStart += time;
			return;
		}

		if(!p_player.isStarted)
		{
			p_player.Play(this, p_lastPlayedClip, p_channel);
		}

		if (p_player.isFinished)
		{
			Clear();

			return;
		}

		if (fade == p_fadeTo && p_stopped)
		{
			p_player.Stop();
			Clear();
			return;
		}

		if (fade == p_fadeTo && p_paused && !p_player.isPaused)
		{
			p_player.isPaused = p_paused;
			return;
		}

		if (!p_paused && p_player.isPaused)
		{
			p_player.isPaused = p_paused;
			return;
		}
	}

	#endregion

}
