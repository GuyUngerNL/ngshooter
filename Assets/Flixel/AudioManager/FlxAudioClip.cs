﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlxAudioClip : FlxAudioAsset
{
	[ReadOnly]
	public AudioClip reference;

	public float volume = 1f;
	public float pitch = 1f;

	public override AudioClip GetClip(int index)
	{
		return reference;
	}

	public override float GetVolumeModifier(int index)
	{
		return volume;
	}

	public override float GetPitchModifier(int index)
	{
		return pitch;
	}
}