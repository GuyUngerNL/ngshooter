﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlxSoundPlayer : FlxComponent
{
	static int s_instanceID;

	public AudioSource audioSource;
	public FlxAudioAsset asset;

	public bool isStarted { get { return timeStarted > 0f; } }
	public bool isFinished { get; private set; }

	double timeStarted;

	public float volume { set { audioSource.volume = value; } }
	public float pitch { set { audioSource.pitch = value; } }
	public bool loop { set { audioSource.loop = value; } }

	public bool isPaused
	{
		get
		{
			return isStarted && !isFinished && !audioSource.isPlaying;
		}
		set
		{
			if (value == isPaused)
			{
				return;
			}
			else
			{
				if (value)
				{
					audioSource.Pause();
				}
				else
				{
					audioSource.UnPause();
				}
			}
		}
	}

	private void Awake()
	{
		DontDestroyOnLoad(gameObject);
	}

	public void Play(FlxAudioAsset asset, AudioClip clip, SoundChannel channel)
	{
		if(!isActiveAndEnabled)
		{
			throw new System.Exception("UNABLE TO PLAY AUDIO CLIP. YOU ARE REFERENCING NON-RECYCLED COMPONENT. TELL IGGI ABOUT THIS!");
		}

		this.asset = asset;

		timeStarted = Time.unscaledTime;

		if(clip == null || clip.length == 0f)
		{
			isFinished = true;
		}
		else
		{
			audioSource.outputAudioMixerGroup = Managers.AudioPlayer.GetMixer(channel);
			audioSource.clip = clip;
			audioSource.time = 0f;
			audioSource.Play();

#if UNITY_EDITOR
			s_instanceID++;
			name = $"Clip: {clip.name} ({s_instanceID})";
#endif
		}
	}

	public void Stop()
	{
		if (isStarted && !isFinished)
		{
			audioSource.Stop();
			isFinished = true;
		}
	}

	private void Update()
	{
		if(isStarted && !isFinished)
		{
			float currentClipTime = audioSource.time;
			float clipLength = audioSource.clip.length;

			if (timeStarted < Time.unscaledTime && !audioSource.isPlaying && ((currentClipTime >= clipLength) || (currentClipTime == 0)))
			{
				isFinished = true;
			}
		}

		if(asset == null && isFinished && isStarted)
		{
			flx.exists = false;
		}
	}

	public override void OnRecycle()
	{
		base.OnRecycle();

		asset = null;
		timeStarted = 0f;
		isFinished = false;
		loop = false;
		volume = 1f;
		pitch = 1f;
	}

	public override void Reset()
	{
		base.Reset();
		audioSource = GetComponent<AudioSource>();
	}
}

