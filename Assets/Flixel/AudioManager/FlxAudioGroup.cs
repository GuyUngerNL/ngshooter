﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(menuName = "Audio Group")]
public class FlxAudioGroup : FlxAudioAsset
{
	[SerializeField]
	private PlayMode _listOrder;

	public override PlayMode listOrder
	{
		get
		{
			return _listOrder;
		}
		set
		{
			_listOrder = value;
		}
	}

	public override int clipCount
	{
		get
		{
			return audioClips.Length;
		}
	}

	public int continuousIntroClips = 1;
	public int continuousOutroClips = 1;
	public int continuousMiddleClips { get { return clipCount - continuousIntroClips - continuousOutroClips; } }
	public float continuousMiddleLength = 0f;
	public float continuousOverlap = 0f;
	public ContinuousPlayMode continuousMiddleOrder;

	[SerializeField, Tooltip("Will reset audio clip index to zero after this time. Leave zero to never reset. Usefull for Clamp play mode.")]
	float _resetAfterIdle;

	public override float resetAfterIdle
	{
		get
		{
			return _resetAfterIdle;
		}
		set
		{
			_resetAfterIdle = value;
		}
	}

	[SerializeField, Tooltip("Minimum time it needs to pass before you play another instance of this audio group.")]
	float _skipIfAlreadyPlaying;

	public override float skipIfAlreadyPlaying
	{
		get
		{
#if UNITY_EDITOR
			return Application.isPlaying ? Mathf.Max(Managers.AudioPlayer.skipIfAlreadyPlaying, _skipIfAlreadyPlaying) : _skipIfAlreadyPlaying;
#else
			return Mathf.Max(Managers.AudioPlayer.skipIfAlreadyPlaying, _skipIfAlreadyPlaying);
#endif
		}

		set
		{
			_skipIfAlreadyPlaying = value;
		}
	}

	public List<FlxAudioClip> audioAssets;
	public AudioClip[] audioClips;

	public float volumeBase = 1f;
	public float volumeDelta = 0f;
	public float volumeRandom = 0f;
	public float volumeMaxValue = 0f;

	public override float GetVolumeModifier(int index)
	{
		float finalVolume = volumeBase + volumeDelta * index;
		if (volumeMaxValue != 0f && finalVolume > volumeMaxValue) finalVolume = volumeMaxValue;

		if (volumeRandom != 0f)
		{
			finalVolume += Math.randomSpread(volumeRandom);
		}

		return finalVolume;
	}

	public float pitchBase = 1f;
	public float pitchDelta = 0f;
	public float pitchRandom = 0f;
	public float pitchMaxValue = 0f;

	public int maxDeltas;

	public override float GetPitchModifier(int index)
	{
		float finalPitch = pitchBase + pitchDelta * index;
		if (pitchMaxValue != 0f && finalPitch > pitchMaxValue) finalPitch = pitchMaxValue;


		if(pitchRandom != 0f)
		{
			finalPitch += Math.randomSpread(pitchRandom);
		}

		return finalPitch;
	}

	void OnValidate()
	{
		_resetAfterIdle = Mathf.Max(0f, _resetAfterIdle);
		_skipIfAlreadyPlaying = Mathf.Max(0f, _skipIfAlreadyPlaying);
		maxDeltas = Mathf.Max(0, maxDeltas);
	}

	PseudoRandom<int> pseudoRandomQueue;

	public override AudioClip GetClip(int index)
	{
		int count = audioClips.Length;

		if (count == 0) return null;
		if (count == 1) return audioClips[0];

		var clipIndex = index;

		switch (listOrder)
		{
			case PlayMode.PseudoRandom:
				if (pseudoRandomQueue == null)
				{
					pseudoRandomQueue = PseudoRandom.Range(0, count, 1);
					pseudoRandomQueue.Shuffle();
				}
				clipIndex = pseudoRandomQueue.next;
				break;
			case PlayMode.Clamp:
				clipIndex = Mathf.Clamp(index, 0, count - 1);
				break;
			case PlayMode.Repeat:
				clipIndex = index % count;
				break;
			case PlayMode.PingPong:
				int countAlt = count - 1;
				clipIndex = clipIndex % (countAlt * 2);
				if (clipIndex > countAlt) clipIndex = countAlt - clipIndex % countAlt;
				break;
			default:
				clipIndex = clipIndex % count;
				break;
		}

		return audioClips[clipIndex];
	}
}
