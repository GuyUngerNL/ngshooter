﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlxAudioCollection : ScriptableObject
{
#if UNITY_EDITOR
	public const string DEFAULT_ASSET_PATH = "Assets/Flixel/AudioManager/Resources/MainAudioCollection.asset";
#endif

	static FlxAudioCollection m_main;

	public static FlxAudioCollection main
	{
		get
		{
			if (m_main == null)
			{
				m_main = Resources.Load<FlxAudioCollection>("MainAudioCollection");

#if UNITY_EDITOR
				if (m_main == null)
				{
					m_main = UnityEditor.AssetDatabase.LoadAssetAtPath<FlxAudioCollection>(DEFAULT_ASSET_PATH);
				}

				if (m_main == null)
				{
					m_main = ScriptableObject.CreateInstance<FlxAudioCollection>();
					UnityEditor.AssetDatabase.CreateAsset(m_main, DEFAULT_ASSET_PATH);
					UnityEditor.AssetDatabase.SaveAssets();
					UnityEditor.AssetDatabase.Refresh();
				}
#endif

				m_main = Instantiate(m_main);
			}

			return m_main;
		}
	}

	public List<string> folders;

	public string groupsFolder;

	public FlxAudioGroup[] soundGroups;
	public FlxAudioClip[] soundAssets;
}