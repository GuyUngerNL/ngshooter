﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System;
using System.Collections;
using System.Collections.Generic;

public static class ComponentUtilityAddons
{
	public static void MoveComponentToTop(Component component)
	{
		for (int i = 0, l = GetComponentIndex (component); i < l; i++) {
			ComponentUtility.MoveComponentUp (component);
		}
	}

	public static void MoveComponentToBottom(Component component)
	{
		for (int i = 0, l = component.GetComponents <Component>().Length; i < l; i++) {
			ComponentUtility.MoveComponentDown (component);
		}
	}

	public static void MoveComponentToIndex(Component component, int index)
	{
		var components = component.GetComponents <Component>();

		if(index >= components.Length || index < 0) {
			return;
		}

		var currentIndex = GetComponentIndex (component); 

		while(currentIndex != index) {
			if(index > currentIndex) {
				ComponentUtility.MoveComponentUp (component);
				currentIndex++;
			} else {
				ComponentUtility.MoveComponentDown (component);
				currentIndex--;
			}
		}
	}

	public static int GetComponentIndex(Component component)
	{
		return Array.IndexOf <Component> (component.GetComponents <Component>(), component);
	}
}
