﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class AbstractProperty<TValue, TObject> where TObject : UnityEngine.Object
{
	public Func<TObject[], TValue, TValue> inspectorGUI;
	public Action<TObject[], TValue> sceneGUI;
	public Action<TObject[], TValue> changed;

	public Func<TObject, TValue> updateValue;

	public TObject[] targets;
	public TValue[] values;

	public bool hasMultipleDifferentValues
	{
		get
		{
			if(values.Length > 0) {
				TValue originalValue = values [0];

				foreach(var v in values)
				{
					if(!v.Equals (originalValue)) {
						return true;
					}
				}
			}	

			return false;
		}
	}

    public AbstractProperty(UnityEngine.Object[] targets)
    {
        this.targets = targets.Cast<TObject>().ToArray();
        values = new TValue[targets.Length];
    }


    public AbstractProperty(
        UnityEngine.Object[] targets,
        Func<TObject, TValue> updateValue,
        Action<TObject[], TValue> changed,
        Func<TObject[], TValue, TValue> inspectorGUI,
        Action<TObject[], TValue> sceneGUI = null
        )
    {
        this.targets = targets.Cast<TObject>().ToArray();
        values = new TValue[targets.Length];

        this.updateValue = updateValue;
        this.changed = changed;
        this.inspectorGUI = inspectorGUI;
        this.sceneGUI = sceneGUI;
    }

    public void Update()
	{
		if(updateValue != null) {
			for(int i = 0; i < targets.Length; i++)
			{
				values [i] = updateValue (targets [i]);
			}
		}
	}

	public void OnInspectorGUI()
	{
		Update ();

		if(values.Length == 0) {
			return;
		}

		EditorGUI.BeginChangeCheck ();
		EditorGUI.showMixedValue = hasMultipleDifferentValues;

		if(inspectorGUI != null) {
			values[0] = inspectorGUI.Invoke (targets, values[0]);
		}

		EditorGUI.showMixedValue = false;
		if(EditorGUI.EndChangeCheck () && changed != null) {
			changed.Invoke (targets, values[0]);
		}
	}

}
