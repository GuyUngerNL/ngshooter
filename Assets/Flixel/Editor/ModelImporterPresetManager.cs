﻿/*[[Category: Editor]]
Author: Xadhoom

== Description ==

Adds a new editor window at menu '''Custom→ModelImportManager'''. The editor window allows selecting/creating/editing/saving model import settings presets which are applied if a new model (e.g. from Fbx file) is imported into Unity. The script file contains two classes. The EditorWindow providing the functionality to manage the presets and the AssetPostProcessor which applies the selected settings. Presets and settings are stored in EditorPrefs.

Background information can be found here: http://trianglestrip.blogspot.de/2012/07/model-import-presets.html

== Usage ==

You must place the script in a folder named '''Editor''' in your project's Assets folder for it to work properly.

Click on '''Custom->ModelImportManager'''

Change the import settings presented on the right as you wish and and press '''Save As''' providing a preset name on the right. By clicking on the presets at left they are loaded and will be applied on model import if the global '''Use Import Settings''' checkbox at the top is selected.

Note: The '''default''' preset cannot be overriden. Use this prefab to apply default import settings or save the changes made as a new preset. 


== C# - ModelImportManager.cs (Developed for Unity 3.5) ==

<syntaxhighlight lang="csharp">*/
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;


/// <summary>
/// Provides most of the settings of Unitys model import dialog in an editor window to define them before
/// importing assets. Presets can be defined/selected/edited/saved and are stored in EditorPrefs.
/// The saved parameters are applied on model import by the ModelImportProcessor script.
/// </summary>
public class ModelImportManager : EditorWindow
{
	/// PRIVATE ===============================================================

	/// <summary>
	/// Defines wether to use the selected import settings or not.
	/// </summary>
	private bool mUseImportSettings;

	/// <summary>
	/// Contains all preset names read from registry.
	/// </summary>
	private List<string> mPresetList;

	/// <summary>
	/// Name of the currently selected preset.
	/// </summary>
	private string mCurrentPresetName;

	/// <summary>
	/// Name of the new preset name when saving.
	/// </summary>
	private string mNewPresetName = "MySettings";

	/// <summary>
	/// Constant definition of the default preset name.
	/// </summary>
	private static readonly string mDefaultPresetName = "Default";

	// Meshes //

	/// <summary>
	/// Global scale factor for importing.
	/// </summary>
	private float mGlobalScale;

	/// <summary>
	/// Mesh compression setting.
	/// </summary>
	private ModelImporterMeshCompression mMeshCompression;

    /// <summary>
    // mesh vertices and indices accessible from script
    /// </summary>
    private bool mIsReadable;

	/// <summary>
	/// Vertex optimization setting
	/// </summary>
	private bool mOptimizeMesh;

    /// <summary>
    // BlendShapes import control
    /// </summary>
    private bool mImportBlendShapes;

	/// <summary>
	// Add mesh colliders to imported meshes.
	/// </summary>
	private bool mAddCollider;

	/// <summary>
	// Keep quads in imported meshes.
	/// </summary>
    private bool mKeepQuads;

	/// <summary>
	/// Swap primary and secondary UV channels when importing.
	/// </summary>
	private bool mSwapUVChannels;

	/// <summary>
	/// Generate secondary UV set for lightmapping.
	/// </summary>
	private bool mGenerateSecondaryUV ;

    private float mSecondaryUVHardAngle;
    private float mSecondaryUVPackMargin;
    private float mSecondaryUVAngleDistortion;
    private float mSecondaryUVAreaDistortion;

	// Normals & Tangents //

	/// <summary>
	/// Normals import mode.
	/// </summary>
	private ModelImporterNormals mNormalImportMode;

	/// <summary>
	/// Tangents import mode.
	/// </summary>
    private ModelImporterTangents mTangentImportMode;

	/// <summary>
	// Smoothing angle for calculating normals.
	/// </summary>
	private float mNormalSmoothingAngle;

	// Materials //

	/// <summary>
	/// Import materials from file.
	/// </summary>
	private bool mImportMaterials;

	/// <summary>
	/// Material naming setting.
	/// </summary>
	private ModelImporterMaterialName mMaterialName;

	/// <summary>
	/// Existing material search setting.
	/// </summary>
	private ModelImporterMaterialSearch mMaterialSearch;

    private bool showAdvancedUV = false;

    private int propertyGroup = 0;
    private enum PropertyGroup { Mesh, Rig, Animations };
             

	/// METHODS ===============================================================

	/// -----------------------------------------------------------------------
	/// <summary>
	/// Adds menu named "ModelImportManager" to the "Custom" menu which creates and initializes a new instance of this class.
	/// </summary>
	/// -----------------------------------------------------------------------
	[MenuItem("Window/Model Import Manager")]
	public static void Init()
	{
		ModelImportManager win = EditorWindow.GetWindow( typeof(ModelImportManager) ) as ModelImportManager;

		win.init();
		win.Show();
        win.reset();
	}

	/// -----------------------------------------------------------------------
	/// <summary>
	/// Draws the GUI window.
	/// </summary>
	/// -----------------------------------------------------------------------
	private void OnGUI()
	{
		GUI.SetNextControlName( "ToggleGroup" );
		bool useImportSettings = EditorGUILayout.BeginToggleGroup( "Use '" + mCurrentPresetName + "' Import Settings", mUseImportSettings );
		if( useImportSettings != mUseImportSettings )
		{
			mUseImportSettings = useImportSettings;
			EditorPrefs.SetBool( "ModelImportManager.UseImportSettings", mUseImportSettings );
		}

		EditorGUILayout.Space();
		GUILayout.BeginHorizontal();
		EditorGUILayout.Space();
		drawPresetList();
		EditorGUILayout.Space();
		drawSettingDialog();
		EditorGUILayout.Space();
		GUILayout.EndHorizontal();

		EditorGUILayout.EndToggleGroup();
	}

	/// -----------------------------------------------------------------------
	/// <summary>
	/// Draws the preset list.
	/// </summary>
	/// -----------------------------------------------------------------------
	private void drawPresetList()
	{
		GUILayout.BeginVertical( GUILayout.Width( 120 ) );

		GUILayout.Label( "Presets", EditorStyles.boldLabel );

		// default
		if( GUILayout.Button( mDefaultPresetName, mCurrentPresetName == mDefaultPresetName ? EditorStyles.toolbarButton : EditorStyles.miniButtonMid ) )
		{
			reset();
			GUI.FocusControl( "ToggleGroup" );
		}

		// all custom presets
		foreach( string presetName in mPresetList )
		{
			if( GUILayout.Button( presetName, presetName == mCurrentPresetName ? EditorStyles.toolbarButton : EditorStyles.miniButtonMid ) )
			{
				loadPreset( presetName );
				GUI.FocusControl( "ToggleGroup" );
			}
		}

		GUILayout.EndVertical();
	}

	/// -----------------------------------------------------------------------
	/// <summary>
	/// Draws the setting dialog.
	/// </summary>
	/// -----------------------------------------------------------------------
	private void drawSettingDialog()
	{       
        GUILayout.BeginVertical( GUILayout.MinWidth( 300 ), GUILayout.MaxWidth( 1000 ), GUILayout.ExpandWidth( true ) );

        EditorGUI.BeginDisabledGroup(true); //other sections to do
        propertyGroup = (int)GUILayout.SelectionGrid((int)propertyGroup, PropertyGroup.GetNames(typeof(PropertyGroup)), 3, GUILayout.Width(240));
        EditorGUI.EndDisabledGroup();

		GUILayout.Label( "Meshes", EditorStyles.boldLabel );
        

		// Global scale factor for importing.
		mGlobalScale = EditorGUILayout.FloatField( "Scale Factor", mGlobalScale );

		// Mesh compression setting.
		mMeshCompression = (ModelImporterMeshCompression)EditorGUILayout.EnumPopup( "Mesh Compression", mMeshCompression );

        // mesh vertices and indices accessible from script
        mIsReadable = EditorGUILayout.Toggle("Read/Write Enabled", mIsReadable);

        // BlendShapes import control
        mImportBlendShapes = EditorGUILayout.Toggle("Import BlendShapes", mImportBlendShapes);

		// Vertex optimization setting
		mOptimizeMesh = EditorGUILayout.Toggle( "Optimize Mesh", mOptimizeMesh );

		// Add mesh colliders to imported meshes.
		mAddCollider = EditorGUILayout.Toggle( "Generate Colliders", mAddCollider );

        // Keep Quads.
        mKeepQuads = EditorGUILayout.Toggle("KeepQuads", mKeepQuads);

		// Swap primary and secondary UV channels when importing.
		mSwapUVChannels = EditorGUILayout.Toggle( "Swap UVs", mSwapUVChannels );

		// Generate secondary UV set for lightmapping.
		mGenerateSecondaryUV = EditorGUILayout.Toggle( "Generate Lightmap UVs", mGenerateSecondaryUV );

        if (mGenerateSecondaryUV)
        {
            showAdvancedUV = EditorGUILayout.Foldout(showAdvancedUV, "Advanced");
            if (showAdvancedUV)
            {
                mSecondaryUVHardAngle = (float)EditorGUILayout.Slider("Hard Angle", (float)mSecondaryUVHardAngle, 0, 180);
                mSecondaryUVPackMargin = (float)EditorGUILayout.Slider("Pack Margin", (float)mSecondaryUVPackMargin, 1, 64);
                mSecondaryUVAngleDistortion = (float)EditorGUILayout.Slider("Angle Error", (float)mSecondaryUVAngleDistortion, 1, 75);
                mSecondaryUVAreaDistortion = (float)EditorGUILayout.Slider("Area Error", (float)mSecondaryUVAreaDistortion, 1, 75);
            }
        }

		EditorGUILayout.Space();
		GUILayout.Label( "Normals & Tangents", EditorStyles.boldLabel );

		// Normals import mode.
		mNormalImportMode = (ModelImporterNormals)EditorGUILayout.EnumPopup( "Normals", mNormalImportMode );

        EditorGUI.BeginDisabledGroup(mNormalImportMode != ModelImporterNormals.Calculate);

        // Smoothing angle for calculating normals.
        mNormalSmoothingAngle = (float)EditorGUILayout.Slider("Smoothing Angle", (float)mNormalSmoothingAngle, 0, 180);

        EditorGUI.EndDisabledGroup();

		// Tangents import mode.
        mTangentImportMode = (ModelImporterTangents)EditorGUILayout.EnumPopup("Tangents", mTangentImportMode);

		EditorGUILayout.Space();
		GUILayout.Label( "Materials", EditorStyles.boldLabel );

		// Import materials from file.
		mImportMaterials = EditorGUILayout.Toggle( "Import Materials", mImportMaterials );

		EditorGUI.BeginDisabledGroup( !mImportMaterials );

		// Material naming setting.
		mMaterialName = (ModelImporterMaterialName)EditorGUILayout.EnumPopup( "Material Naming", mMaterialName );

		// Existing material search setting.
		mMaterialSearch = (ModelImporterMaterialSearch)EditorGUILayout.EnumPopup( "Material Search", mMaterialSearch );

		EditorGUI.EndDisabledGroup();

		EditorGUILayout.Space();
		EditorGUILayout.BeginHorizontal();
		if( mCurrentPresetName != mDefaultPresetName )
		{
			if( GUILayout.Button( "Save" ) )
				savePreset( mCurrentPresetName );

			if( GUILayout.Button( "Delete" ) )
				deletePreset( mCurrentPresetName );
		}
		else
		{
			if( GUILayout.Button( "Save As", GUILayout.Width( 200 ) ) && ( mNewPresetName.Length > 0 ) && ( mNewPresetName != mDefaultPresetName ) )
			{
				savePreset( mNewPresetName );
				loadPreset( mNewPresetName );
			}

			// only characters allowed
			mNewPresetName = Regex.Replace( GUILayout.TextField( mNewPresetName ), @"[^\w]", string.Empty );
		}
		EditorGUILayout.EndHorizontal();

		GUILayout.EndVertical();
	}

	/// -----------------------------------------------------------------------
	/// <summary>
	/// Loads the preset list from registry.
	/// </summary>
	/// -----------------------------------------------------------------------
	private void loadPresetList()
	{
		mPresetList = new List<string>();

		// try to read the list of presets
		string presetListStr = EditorPrefs.GetString( "ModelImportManager.Presets", string.Empty );
		if( presetListStr != string.Empty )
		{
			string[] presetList = presetListStr.Split( ';' );
			mPresetList.AddRange( presetList );
		}
	}

	/// -----------------------------------------------------------------------
	/// <summary>
	/// Saves the preset list to registry.
	/// </summary>
	/// -----------------------------------------------------------------------
	private void savePresetList()
	{
		StringBuilder presetListStr = new StringBuilder();

		if( mPresetList.Count > 0 )
		{
			foreach( string preset in mPresetList )
				presetListStr.Append( preset ).Append( ';' );

			EditorPrefs.SetString( "ModelImportManager.Presets", presetListStr.ToString( 0, presetListStr.Length - 1 ) );
		}
		else
		{
			EditorPrefs.SetString( "ModelImportManager.Presets", string.Empty );
		}
	}

	/// -----------------------------------------------------------------------
	/// <summary>
	/// Save the current settings to Registry.
	/// </summary>
	/// -----------------------------------------------------------------------
	private void savePreset( string presetName )
	{
		string prefix = "ModelImportManager." + presetName;

		// Meshes
		EditorPrefs.SetFloat( prefix + ".GlobalScale", mGlobalScale );
		EditorPrefs.SetString( prefix + ".MeshCompression", mMeshCompression.ToString() );
        EditorPrefs.SetBool(prefix + ".IsReadable", mIsReadable);
		EditorPrefs.SetBool( prefix + ".OptimizeMesh", mOptimizeMesh );
        EditorPrefs.SetBool( prefix + ".ImportBlendShapes", mImportBlendShapes );
		EditorPrefs.SetBool( prefix + ".AddCollider", mAddCollider );
        EditorPrefs.SetBool(prefix + ".KeepQuads", mKeepQuads);
		EditorPrefs.SetBool( prefix + ".SwapUVChannels", mSwapUVChannels );
		EditorPrefs.SetBool( prefix + ".GenerateSecondaryUV", mGenerateSecondaryUV );

        EditorPrefs.SetFloat( prefix + ".SecondaryUVHardAngle", mSecondaryUVHardAngle );
        EditorPrefs.SetFloat( prefix + ".SecondaryUVPackMargin", mSecondaryUVPackMargin );
        EditorPrefs.SetFloat( prefix + ".SecondaryUVAngleDistortion", mSecondaryUVAngleDistortion );
        EditorPrefs.SetFloat( prefix + ".SecondaryUVAreaDistortion", mSecondaryUVAreaDistortion );

		// Normals & Tangents
		EditorPrefs.SetString( prefix + ".NormalImportMode", mNormalImportMode.ToString() );
		EditorPrefs.SetString( prefix + ".TangentImportMode", mTangentImportMode.ToString() );
		EditorPrefs.SetFloat( prefix + ".NormalSmoothingAngle", mNormalSmoothingAngle );

		// Materials
		EditorPrefs.SetBool( prefix + ".ImportMaterials", mImportMaterials );
		EditorPrefs.SetString( prefix + ".MaterialName", mMaterialName.ToString() );
		EditorPrefs.SetString( prefix + ".MaterialSearch", mMaterialSearch.ToString() );

		// add preset to preset list if its a new one
		if( !mPresetList.Contains( presetName ) )
		{
			mPresetList.Add( presetName );
			savePresetList();
		}

		Debug.Log( "ModelImportManager::savePreset, Settings have been saved to preset '" + presetName + "'." );
	}

	/// -----------------------------------------------------------------------
	/// <summary>
	/// Load settings from Registry.
	/// </summary>
	/// -----------------------------------------------------------------------
	private void loadPreset( string presetName )
	{
		string prefix = "ModelImportManager." + presetName;

		// Meshes
		mGlobalScale = EditorPrefs.GetFloat( prefix + ".GlobalScale", 1.0f );
		mMeshCompression = (ModelImporterMeshCompression)System.Enum.Parse( typeof(ModelImporterMeshCompression),
								EditorPrefs.GetString( prefix + ".MeshCompression", "Off" ) );
        mIsReadable = EditorPrefs.GetBool(prefix + ".IsReadable", true);
		mOptimizeMesh = EditorPrefs.GetBool( prefix + ".OptimizeMesh", true );
        mImportBlendShapes = EditorPrefs.GetBool(prefix + ".ImportBlendShapes", true);
		mAddCollider = EditorPrefs.GetBool( prefix + ".AddCollider", false );
        mKeepQuads = EditorPrefs.GetBool(prefix + ".KeepQuads", false);
		mSwapUVChannels = EditorPrefs.GetBool( prefix + ".SwapUVChannels", false );
		mGenerateSecondaryUV = EditorPrefs.GetBool( prefix + ".GenerateSecondaryUV", false );

        EditorPrefs.GetFloat(prefix + ".SecondaryUVHardAngle", 88.0f);
        EditorPrefs.GetFloat(prefix + ".SecondaryUVPackMargin", 4.0f);
        EditorPrefs.GetFloat(prefix + ".SecondaryUVAngleDistortion", 8.0f);
        EditorPrefs.GetFloat(prefix + ".SecondaryUVAreaDistortion", 15.0f);


		// Normals & Tangents
		mNormalImportMode = (ModelImporterNormals)System.Enum.Parse( typeof(ModelImporterNormals),
								EditorPrefs.GetString( prefix + ".NormalImportMode", "Import" ) );
        mTangentImportMode = (ModelImporterTangents)System.Enum.Parse(typeof(ModelImporterTangents),
                                EditorPrefs.GetString(prefix + ".TangentImportMode", "CalculateLegacyWithSplitTangents"));
		mNormalSmoothingAngle = EditorPrefs.GetFloat( prefix + ".NormalSmoothingAngle", 60.0f );

		// Materials
		mImportMaterials = EditorPrefs.GetBool( prefix + ".ImportMaterials", false );
		mMaterialName = (ModelImporterMaterialName)System.Enum.Parse( typeof(ModelImporterMaterialName),
								EditorPrefs.GetString( prefix + ".MaterialName", "BasedOnTextureName" ) );
		mMaterialSearch = (ModelImporterMaterialSearch)System.Enum.Parse( typeof(ModelImporterMaterialSearch),
								EditorPrefs.GetString( prefix + ".MaterialSearch", "RecursiveUp" ) );

		// remember current preset selection
		mCurrentPresetName = presetName;
		EditorPrefs.SetString( "ModelImportManager.CurrentPreset", mCurrentPresetName );
	}

	/// -----------------------------------------------------------------------
	/// <summary>
	/// Remove all setting information from registry.
	/// </summary>
	/// -----------------------------------------------------------------------
	private void deletePreset( string presetName )
	{
		string prefix = "ModelImportManager." + presetName;

		EditorPrefs.DeleteKey( prefix + ".GlobalScale" );
		EditorPrefs.DeleteKey( prefix + ".MeshCompression" );
        EditorPrefs.DeleteKey(prefix + ".ImportBlendShapes");
		EditorPrefs.DeleteKey( prefix + ".OptimizeMesh" );
        EditorPrefs.DeleteKey(prefix + ".IsReadable");
		EditorPrefs.DeleteKey( prefix + ".AddCollider" );
        EditorPrefs.DeleteKey( prefix + ".KeepQuads" );
		EditorPrefs.DeleteKey( prefix + ".SwapUVChannels" );
		EditorPrefs.DeleteKey( prefix + ".GenerateSecondaryUV" );

        EditorPrefs.DeleteKey(prefix + ".SecondaryUVHardAngle");
        EditorPrefs.DeleteKey(prefix + ".SecondaryUVPackMargin");
        EditorPrefs.DeleteKey(prefix + ".SecondaryUVAngleDistortion");
        EditorPrefs.DeleteKey(prefix + ".SecondaryUVAreaDistortion");

		EditorPrefs.DeleteKey( prefix + ".NormalImportMode" );
		EditorPrefs.DeleteKey( prefix + ".TangentImportMode" );
		EditorPrefs.DeleteKey( prefix + ".NormalSmoothingAngle" );
		EditorPrefs.DeleteKey( prefix + ".SplitTangentsAcrossSeams" );
		EditorPrefs.DeleteKey( prefix + ".ImportMaterials" );
		EditorPrefs.DeleteKey( prefix + ".MaterialName" );
		EditorPrefs.DeleteKey( prefix + ".MaterialSearch" );

		mPresetList.Remove( presetName );
		savePresetList();
		reset();

		Debug.Log( "ModelImportManager::deletePreset, Preset '" + presetName + "' has been deleted." );
	}

	/// -----------------------------------------------------------------------
	/// <summary>
	/// Resets settings to default values.
	/// </summary>
	/// -----------------------------------------------------------------------
	private void reset()
	{
		// Meshes
		mGlobalScale = 1.0f;
		mMeshCompression = ModelImporterMeshCompression.Off;
        mIsReadable = true;
		mOptimizeMesh = true;
        mImportBlendShapes = true;
		mAddCollider = false;
        mKeepQuads = false;
		mSwapUVChannels = false;
		mGenerateSecondaryUV = false;
        mSecondaryUVHardAngle = 88.0f;
        mSecondaryUVPackMargin = 4.0f;
        mSecondaryUVAngleDistortion = 8.0f;
        mSecondaryUVAreaDistortion = 15.0f;

		// Normals & Tangents
		mNormalImportMode = ModelImporterNormals.Import;
        mTangentImportMode = ModelImporterTangents.CalculateLegacy;
		mNormalSmoothingAngle = 60.0f;

		// Materials
		mImportMaterials = true;
		mMaterialName = ModelImporterMaterialName.BasedOnTextureName;
		mMaterialSearch = ModelImporterMaterialSearch.RecursiveUp;


		// remember current preset selection
		mCurrentPresetName = mDefaultPresetName;
		EditorPrefs.SetString( "ModelImportManager.CurrentPreset", mCurrentPresetName );
	}

	/// -----------------------------------------------------------------------
	/// <summary>
	/// Init this instance.
	/// </summary>
	/// -----------------------------------------------------------------------
	private void init()
	{
		loadPresetList();

		mUseImportSettings = EditorPrefs.GetBool( "ModelImportManager.UseImportSettings", true );

		string currentPresetName = EditorPrefs.GetString( "ModelImportManager.CurrentPreset", mDefaultPresetName );
		loadPreset( currentPresetName );
	}
}

/// ---------------------------------------------------------------------------
/// <summary>
/// Import script applies model import settings based on the settings made in the ModelImportManager window.
/// </summary>
/// ---------------------------------------------------------------------------
public class ModelImportProcessor : AssetPostprocessor
{
    /// -----------------------------------------------------------------------
    /// <summary>
    /// Applies the import settings of the model based on ModelImportManager settings.
    /// </summary>
    /// -----------------------------------------------------------------------
    private void OnPreprocessModel()
    {
        ModelImporter modelImporter = assetImporter as ModelImporter;
        if (modelImporter != null)
        {
            bool useImportSettings = EditorPrefs.GetBool("ModelImportManager.UseImportSettings", false);
            if (useImportSettings)
            {
                string presetName = EditorPrefs.GetString("ModelImportManager.CurrentPreset", string.Empty);

                string prefix = "ModelImportManager." + presetName;

                // Meshes
                modelImporter.globalScale = EditorPrefs.GetFloat(prefix + ".GlobalScale", 1.0f);
                modelImporter.meshCompression = (ModelImporterMeshCompression)System.Enum.Parse(typeof(ModelImporterMeshCompression),
                                                EditorPrefs.GetString(prefix + ".MeshCompression", "Off"));
                modelImporter.isReadable = EditorPrefs.GetBool(prefix + ".IsReadable", true);
                modelImporter.optimizeMesh = EditorPrefs.GetBool(prefix + ".OptimizeMesh", true);
                modelImporter.importBlendShapes = EditorPrefs.GetBool(prefix + ".ImportBlendShapes", true);
                modelImporter.addCollider = EditorPrefs.GetBool(prefix + ".AddCollider", false);
                //modelImporter.keepQuads = EditorPrefs.GetBool(prefix + ".KeepQuads", false);
                modelImporter.swapUVChannels = EditorPrefs.GetBool(prefix + ".SwapUVChannels", false);
                modelImporter.generateSecondaryUV = EditorPrefs.GetBool(prefix + ".GenerateSecondaryUV", false);

                modelImporter.secondaryUVHardAngle = EditorPrefs.GetFloat(prefix + ".SecondaryUVHardAngle", 88.0f);
                modelImporter.secondaryUVPackMargin = EditorPrefs.GetFloat(prefix + ".SecondaryUVPackMargin", 4.0f);
                modelImporter.secondaryUVAngleDistortion = EditorPrefs.GetFloat(prefix + ".SecondaryUVAngleDistortion", 8.0f);
                modelImporter.secondaryUVAreaDistortion = EditorPrefs.GetFloat(prefix + ".SecondaryUVAreaDistortion", 15.0f);

                // Normals & Tangents
                modelImporter.importNormals = (ModelImporterNormals)System.Enum.Parse(typeof(ModelImporterNormals),
                                EditorPrefs.GetString(prefix + ".NormalImportMode", "Import"));
                modelImporter.importTangents = (ModelImporterTangents)System.Enum.Parse(typeof(ModelImporterTangents),
                                EditorPrefs.GetString(prefix + ".TangentImportMode", "CalculateLegacyWithSplitTangents"));
                modelImporter.normalSmoothingAngle = EditorPrefs.GetFloat(prefix + ".NormalSmoothingAngle", 60.0f);
                //modelImporter.splitTangentsAcrossSeams = EditorPrefs.GetBool(prefix + ".SplitTangentsAcrossSeams", false);

                // Materials
                modelImporter.importMaterials = EditorPrefs.GetBool(prefix + ".ImportMaterials", false);
                modelImporter.materialName = (ModelImporterMaterialName)System.Enum.Parse(typeof(ModelImporterMaterialName),
                                EditorPrefs.GetString(prefix + ".MaterialName", "BasedOnTextureName"));
                modelImporter.materialSearch = (ModelImporterMaterialSearch)System.Enum.Parse(typeof(ModelImporterMaterialSearch),
                                EditorPrefs.GetString(prefix + ".MaterialSearch", "RecursiveUp"));

                Debug.Log("ModelImportProcessor::OnPreprocessModel, Using custom import settings of preset '" + presetName + "'.");
            }
        }
    }
}