using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[DisallowMultipleComponent]
[RequireComponent(typeof(Camera))]
[RequireComponent(typeof(FlxActor))]
[ExecuteInEditMode]
public abstract class FlxCamera : FlxComponent
{
	public static FlxCamera main
	{
		get
		{
			return Camera.main ? Camera.main.GetComponent<FlxCamera>() : null;
		}
	}
	public static FlxActor mainActor
	{
		get
		{
			return Camera.main ? Camera.main.GetComponent<FlxActor>() : null;
		}
	}

	public delegate void OnResizeHandler(FlxCamera camera);
	public static event OnResizeHandler OnResize;

	public static void TriggerOnResize(FlxCamera camera)
	{
		if (OnResize != null)
		{
			OnResize(camera);
		}
	}

	protected new Camera camera 
	{
		get
		{
			return GetComponent<Camera>();
		}
	}

	protected float lastUsedAspectRatio;
	protected float lastUsedZoom;
	protected Vector2 lastUsedSize;

	#region Public getters

	public Vector2 screenSize;
	public float zoom = 1.0f;
	public Vector3 targetPosition;

	public abstract Vector3 topLeftPoint { get; }
	public abstract Vector3 topRightPoint { get; }
	public abstract Vector3 bottomLeftPoint { get; }
	public abstract Vector3 bottomRightPoint { get; }
	public abstract Vector2 center { get; }
	public abstract float width { get; }
	public abstract float height { get; }


	public virtual Vector2 mousePosition
	{
		get
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			// create a plane at 0,0,0 whose normal points to +Y:
			Plane hPlane = new Plane(Vector3.up, Vector3.zero);
			// Plane.Raycast stores the distance from ray.origin to the hit point in this variable:
			float distance = 0;
			// if the ray hits the plane...
			if (hPlane.Raycast(ray, out distance))
			{
				// get the hit point:
				return actor.transformer.PositionFromUnity(ray.GetPoint(distance));
			}

			return Vector2.zero;
		}
	}

	#endregion

	protected virtual void Awake()
	{
	}

	protected virtual void LateUpdate()
	{
	}

	protected virtual void OnValidate()
	{
		zoom = Mathf.Max(zoom, 0.001f);
		screenSize.x = Mathf.Max(screenSize.x, 0.001f);
		screenSize.y = Mathf.Max(screenSize.y, 0.001f);
	}
}

