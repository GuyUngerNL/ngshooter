using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class FlxActor : FlxBasic
{
	#region Position

	[SerializeField] protected Vector3 _offset;
	[SerializeField] protected Vector3 _shake;

	/// <summary>
	/// Position in the world
	/// </summary>
	public override Vector3 position
	{
		get
		{
			return transformer.PositionFromUnity(transform.position - _offset - _shake);
		}
		set
		{
			transform.position = transformer.PositionToUnity(value) + _offset + _shake;
		}
	}

	/// <summary>
	/// Position relative to the parent.
	/// </summary>
	public override Vector3 localPosition
	{
		get
		{
			return transformer.PositionFromUnity(transform.localPosition - _offset - _shake);
		}
		set
		{
			transform.localPosition = transformer.PositionToUnity(value) + _offset + _shake;
		}
	}

	/// <summary>
	/// Offset moves the rendering position of the object without changing position X,Y,Z
	/// </summary>
	public virtual Vector3 offset
	{
		get
		{
			return transformer.PositionFromUnity(_offset);
		}
		set
		{
			var oldPosition = localPosition;
			_offset = transformer.PositionToUnity(value);
			localPosition = oldPosition;
		}
	}

	public float offsetX { get { return offset.x; } set { offset = new Vector3(value, offset.y, offset.z); } }
	public float offsetY { get { return offset.y; } set { offset = new Vector3(offset.x, value, offset.z); } }
	public float offsetZ { get { return offset.z; } set { offset = new Vector3(offset.x, offset.y, value); } }

	/// <summary>
	/// Shake moves the rendering position of the object without changing position X,Y,Z
	/// </summary>
	public virtual Vector3 shake
	{
		get
		{
			return transformer.PositionFromUnity(_shake);
		}
		set
		{
			var oldPosition = localPosition;
			_shake = transformer.PositionToUnity(value);
			localPosition = oldPosition;
		}
	}

	public float shakeX { get { return shake.x; } set { shake = new Vector3(value, shake.y, shake.z); } }
	public float shakeY { get { return shake.y; } set { shake = new Vector3(shake.x, value, shake.z); } }
	public float shakeZ { get { return shake.z; } set { shake = new Vector3(shake.x, shake.y, value); } }

	public void AddShake(IShake shake)
	{
		gameObject.GetOrAddComponent<ShakeManager>().AddShake(shake);
	}

	#endregion


	#region Rotation

	[SerializeField]
	protected Vector3 _shakeRotation;

	public virtual Vector3 shakeRotation
	{
		get
		{
			return transformer.RotationFromUnity(_shakeRotation);
		}
		set
		{
			var oldRotation = localRotation;
			_shakeRotation = transformer.RotationToUnity(value);
			localRotation = oldRotation;
		}
	}

	/// <summary>
	/// Position in the world
	/// </summary>
	public override Vector3 rotation
	{
		get
		{
			return transformer.RotationFromUnity(transform.eulerAngles - _shakeRotation);
		}
		set
		{
			transform.eulerAngles = transformer.RotationToUnity(value) + _shakeRotation;
		}
	}

	/// <summary>
	/// Position relative to the parent.
	/// </summary>
	public override Vector3 localRotation
	{
		get
		{
			return transformer.RotationFromUnity(transform.localEulerAngles - _shakeRotation);
		}
		set
		{
			transform.localEulerAngles = transformer.RotationToUnity(value) + _shakeRotation;
		}
	}

	#endregion


	#region Motion

	//FlxMotion[] _motions;
	UList<FlxMotion> _motions = new UList<FlxMotion>(2);

	/// <summary>
	/// Gets the motion. 
	/// It will never return NULL. 
	/// If the motion does not exist on this object, it will be added and returned. 
	/// </summary>
	/// <value>FlxMotion.</value>
	public FlxMotion motion
	{
		get
		{
#if UNITY_EDITOR
			if (!Application.isPlaying) return GetComponent<FlxMotion>();
#endif

			if (_motions.Count > 0)
			{
				return _motions[0];
			}
			else
			{
				FlxMotion _motion = this.gameObject.AddComponent<FlxMotion>();

				return _motion;
			}
		}
	}

	/// <summary>
	/// Gets the array of motion components.
	/// </summary>
	/// <value>The motion components.</value>
	public UList<FlxMotion> motions
	{
		get
		{
			return _motions;
		}
	}

	/// <summary>
	/// Adds the new motion component.
	/// </summary>
	/// <returns>The new motion component.</returns>
	public FlxMotion AddNewMotion()
	{
		return this.gameObject.AddComponent<FlxMotion>();
	}

	Vector3 totalVelocity
	{
		get
		{
			Vector3 total = default(Vector3);
			FlxMotion motion;

			for (int i = 0; i < _motions.Count; i++)
			{
				motion = _motions[i];
				if (motion.enabled)
				{
					total += motion.velocity;
				}
			}

			return total;
		}
	}

	Vector3 totalAngularVelocity
	{
		get
		{
			Vector3 total = default(Vector3);
			FlxMotion motion;

			for (int i = 0; i < _motions.Count; i++)
			{
				motion = _motions[i];
				if (motion.enabled)
				{
					total += motion.angularVelocity;
				}
			}

			return total;
		}
	}

	public float totalDirection
	{
		get
		{
			var v = totalVelocity;
			return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
		}
	}

	public float totalMagnitude
	{
		get
		{
			return totalVelocity.magnitude;
		}
	}

	#endregion

	/// <summary>
	/// For now we just update motions in update
	/// </summary>
	protected virtual void Update()
	{
		if (_motions.Count == 0)
		{
			return;
		}

		//Vector3 velocity = default(Vector3);
		//Vector3 angularVelocity = default(Vector3);
		FlxMotion motion;

		for (int i = 0; i < _motions.Count; i++)
		{
			motion = _motions[i];
			if (motion != null && motion.enabled)
			{
				motion.UpdateMotion();
				motion.Apply();
				//velocity += motion.velocity;
				//angularVelocity += motion.angularVelocity;
			}
		}

		//position += velocity * Time.deltaTime;
		//rotation += angularVelocity * Time.deltaTime;
	}

	override public void Reset()
	{
		base.Reset();

		offset = Vector3.zero;
		shake = Vector3.zero;
	}

	public override void OnRecycle()
	{
		base.OnRecycle();
		offset = Vector3.zero;
		shake = Vector3.zero;
		actor.visible = true;
	}
}
