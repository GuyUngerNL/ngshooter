﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FlxManager : FlxComponent
{
	public bool persistAcrossScenes;
	public bool singleton;

	public bool destroyed { get; private set; }

	protected virtual void Awake()
	{
		var type = GetType();

		if(singleton && Managers.Instances.ContainsKey(type) && Managers.Instances[type] != null)
		{
			gameObject.SetActive(false);
			Destroy(gameObject);
			destroyed = true;
			return;
		}

		Managers.Instances[type] = this;
	}

	protected virtual void OnDestroy()
	{
		var type = GetType();

		if (singleton && Managers.Instances.ContainsKey(type) && Managers.Instances[type] == (object)this)
		{
			Managers.Instances.Remove(type);
		}

		destroyed = true;
	}
}
