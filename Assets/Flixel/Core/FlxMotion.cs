using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(FlxActor))]
public class FlxMotion : FlxComponent
{
	public Vector3 velocity;
	public Vector3 acceleration;
	public Vector3 deceleration;

	public Vector3 angularVelocity;
	public Vector3 angularAcceleration;
	public Vector3 angularDeceleration;

	[HideInInspector]
	public float life = 0;
	public float duration = 0;
	public bool cannotDie = false;
	public float disableTimer = 0;
	 
	public float direction
	{
		get
		{
			return Mathf.Atan2(velocity.y, velocity.x) * Mathf.Rad2Deg;
		}
		set
		{
			float angle = value * Mathf.Deg2Rad;
			float strength = ((Vector2)velocity).magnitude;

			velocity.x = Mathf.Cos(angle) * strength;
			velocity.y = Mathf.Sin(angle) * strength;
		}
	}

	public override void OnRecycle()
	{
		base.OnRecycle();
		life = 0;
	}

	public float magnitude
	{
		get
		{
			return velocity.magnitude;
		}
		set
		{
			if (velocity.sqrMagnitude == 0)
			{
				Set(value, 0f);
			}
			else
			{
				velocity = velocity.normalized * value;
			}
		}
	}

	public virtual void Init()
	{

	}

	/// <summary>
	/// A quick way of checking if velocity has magnitude because it doesn't call Square Root function
	/// </summary>
	public float sqrMagnitude
	{
		get
		{
			return velocity.sqrMagnitude;
		}
	}

	public void Add(Vector3 velocity)
	{
		this.velocity += velocity;
	}

	public void Add(float x, float y, float z)
	{
		this.velocity += new Vector3(x, y, z);
	}

	public void Add(float velocity, float direction)
	{
		this.velocity.x += velocity * Mathf.Cos(direction * Mathf.Deg2Rad);
		this.velocity.y += velocity * Mathf.Sin(direction * Mathf.Deg2Rad);
	}
	
	public void Set(Vector3 velocity)
	{
		this.velocity = velocity;
	}

	public void Set(float x, float y, float z)
	{
		this.velocity = new Vector3(x, y, z);
	}

	public void Set(float velocity, float direction)
	{
		this.velocity.x = velocity * Mathf.Cos(direction * Mathf.Deg2Rad);
		this.velocity.y = velocity * Mathf.Sin(direction * Mathf.Deg2Rad);
	}

	public void Limit(float value)
	{
		if (velocity.magnitude > value)
		{
			velocity = velocity.normalized * value;
		}
	}

	public void friction(float x, float y)
	{
		if (velocity.x > 0)
		{
			velocity.x -= Mathf.Abs(velocity.x) * x * elapsed;
			if (velocity.x < 0) velocity.x = 0;
		}
		else if (velocity.x < 0)
		{
			velocity.x += Mathf.Abs(velocity.x) * x * elapsed;
			if (velocity.x > 0) velocity.x = 0;
		}
		if (velocity.y > 0)
		{
			velocity.y -= Mathf.Abs(velocity.y) * y * elapsed;
			if (velocity.y < 0) velocity.y = 0;
		}
		else if (velocity.y < 0)
		{
			velocity.y += Mathf.Abs(velocity.y) * y * elapsed;
			if (velocity.y > 0) velocity.y = 0;
		}
	}
	
	public virtual void Apply()
	{
		actor.position += velocity * Time.deltaTime;
		actor.rotation += angularVelocity * Time.deltaTime;
	}


	private int _actorMotionIndex;

	protected virtual void Awake()
	{
		actor.motions.Add(this, out _actorMotionIndex);
	}

	protected virtual void OnDestroy()
	{
		FlxMotion replacement;
		actor.motions.RemoveAt(_actorMotionIndex, out replacement);
		if(replacement != null)
		{
			replacement._actorMotionIndex = _actorMotionIndex;
		}
	}


	/// <summary>
	/// Applies accelerations & decelerations to velocities
	/// </summary>
	public void UpdateMotion()
	{ 
		if(disableTimer > 0)
		{
			disableTimer -= elapsed;
			return;
		}
		velocity.x = Math.ComputeVelocity(velocity.x, acceleration.x, deceleration.x, Time.deltaTime);
		velocity.y = Math.ComputeVelocity(velocity.y, acceleration.y, deceleration.y, Time.deltaTime);
		velocity.z = Math.ComputeVelocity(velocity.z, acceleration.z, deceleration.z, Time.deltaTime);

		angularVelocity.x = Math.ComputeVelocity(angularVelocity.x, angularAcceleration.x, angularDeceleration.x, Time.deltaTime);
		angularVelocity.y = Math.ComputeVelocity(angularVelocity.y, angularAcceleration.y, angularDeceleration.y, Time.deltaTime);
		angularVelocity.z = Math.ComputeVelocity(angularVelocity.z, angularAcceleration.z, angularDeceleration.z, Time.deltaTime);

		reduceLife();
	}

	protected virtual void reduceLife()
	{
		if (duration > 0 && cannotDie == false)
		{
			life += elapsed;
			if (life >= duration) enabled = false;
		}
	}

	override public void Reset()
	{
		base.Reset();
		velocity = Vector3.zero;
		acceleration = Vector3.zero;
		deceleration = Vector3.zero;
		angularVelocity = Vector3.zero;
		angularAcceleration = Vector3.zero;
		angularDeceleration = Vector3.zero;
		life = 0;
	}
}