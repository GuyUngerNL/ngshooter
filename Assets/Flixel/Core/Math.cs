﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Math
{
	public static float PI_2
	{
		get { return Mathf.PI * 2f; }
	}

	public static float max(params float[] values)
	{
		float maxValue = values[0];
		foreach (float value in values)
		{
			if (value > maxValue) maxValue = value;
		}
		return maxValue;
	}
  
	public static float min(params float[] values)
	{
		float minValue = values[0];
		foreach (float value in values)
		{
			if (value < minValue) minValue = value;
		}
		return minValue;
	}

	internal static float bound(float Value, float Min, float Max)
	{
		float lowerBound = (Value<Min) ? Min : Value;
		return (lowerBound > Max) ? Max : lowerBound;
	}

	public static int toInt(float f)
	{
		return (int)f;
	}

	public static int round(float f)
	{
		return Mathf.RoundToInt(f);
	}

	public static float floorToGrid(float value, float gridSize)
	{
		return value > 0 ? (value - value % gridSize) : (value - (gridSize - Mathf.Abs(value) % gridSize));

	}

	public static float roundToGrid(float value, float gridSize)
	{
		float diff = Mathf.Abs(value) % gridSize;
		float result = 0;
		if(diff < gridSize * 0.5f)
		{
			if(value < 0) result = value + diff;
			else result = value - diff;
		}
		else
		{
			if (value < 0) result = value - (gridSize - diff);
			else result = value + (gridSize-diff);
		} 
		return result;
	}

	public static float ComputeVelocity(float velocity, float acceleration, float deceleration, float elapsed, float max = 0f)
	{
		if (acceleration != 0f)
		{
			velocity += acceleration * elapsed;
		}
		else if (deceleration != 0f)
		{
			float drag = deceleration * elapsed;

			if (velocity - drag > 0f)
			{
				velocity -= drag;
			}
			else if (velocity + drag < 0f)
			{
				velocity += drag;
			}
			else
			{
				velocity = 0f;
			}
		}

		if (velocity != 0f && max != 0f)
		{
			if (velocity > max)
			{
				velocity = max;
			}
			else if (velocity < -max)
			{
				velocity = -max;
			}

		}

		return velocity;
	}

	public static float Lerp(float from, float to, float t = 0.1f)
	{
		from += (to - from) * t;
		return from;
	}

	public static Vector3 LerpEulerAngles(Vector3 from, Vector3 to, float t)
	{
		return new Vector3(Mathf.LerpAngle(from.x, to.x, t), Mathf.LerpAngle(from.y, to.y, t), Mathf.LerpAngle(from.z, to.z, t));
	}

	public static float LengthBetweenTwoEdgesOfSquare(float l1, float l2)
	{
		return Mathf.Pow(Mathf.Pow(l1, 2) + Mathf.Pow(l2, 2), 0.5f);
	}

	static public bool PointRect(float pointX, float pointY, float rectX, float rectY, float rectWidth, float rectHeight)
	{
		if (pointX >= rectX && pointX <= rectX + rectWidth && pointY >= rectY && pointY <= rectY + rectHeight)
		{
			return true;
		}
		return false;
	}

	static public bool PointRect(float pointX, float pointY, FlxRect rect)
	{
		return (PointRect(pointX, pointY, rect.x, rect.y, rect.width, rect.height));
	}

	static public bool PointInCoordinates(float pointX, float pointY, float rectX, float rectY, float rectWidth, float rectHeight)
	{
		if (pointX >= rectX && pointX <= (rectX + rectWidth))
		{
			if (pointY >= rectY && pointY <= (rectY + rectHeight))
			{
				return true;
			}
		}
		return false;
	}

	public static bool CircleOverlaps(float centerX1, float centerY1, float radius1, float centerX2, float centerY2, float radius2)
	{
		float distanceX = centerX1 - centerX2;
		float distanceY = centerY1 - centerY2;
		float radiusSum = radius1 + radius2;
		return distanceX * distanceX + distanceY * distanceY <= radiusSum * radiusSum;
	}

	public static bool CircleContainsOtherCircle(float centerX1, float centerY1, float radius1, float centerX2, float centerY2, float radius2)
	{
		if (radius2 > radius1)
			return false;
		float distanceX = centerX1 - centerX2;
		float distanceY = centerY1 - centerY2;
		float radiusD = radius1 - radius2;
		return distanceX * distanceX + distanceY * distanceY <= radiusD * radiusD;
	}

	static public float Distance(float fromX, float fromY, float toX, float toY)
	{
		return Mathf.Sqrt((toX - fromX) * (toX - fromX) + (toY - fromY) * (toY - fromY));
	}

	static public float Distance(Vector2 from, Vector2 to)
	{
		return Distance(from.x, from.y, to.x, to.y);
	}

	static public float AngleTo(float fromX, float fromY, float targetX, float targetY)
	{
		return NormalizeAngle(Mathf.Atan2((targetY - (fromY)), (targetX - (fromX))) * Mathf.Rad2Deg);
	}

	static public float AngleTo(Vector3 fromPosition, Vector3 targetPosition)
	{
		return AngleTo(fromPosition.x, fromPosition.y, targetPosition.x, targetPosition.y);
	}

	public static float NormalizeAngle(float angle)
	{
		if (angle > 360)
		{
			angle %= 360;
		}
		else if (angle < 0)
		{
			if (angle < -359)
			{
				angle = (-1) * (Mathf.Abs(angle) % 360);
			}
			angle += 360;
		}
		return angle;
	}

	/*
	public static Point intersectionPoint(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4)
	{
		float d = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1);
		float n_a = (x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3);
		float n_b = (x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3);

		if (Mathf.Approximately(d, 0)) return null;

		float ua = n_a / d;
		float ub = n_b / d;
		if (ua >= 0 && ua <= 1 && ub >= 0 && ub <= 1)
		{
			return Point.recycle(x1 + (ua * (x2 - x1)), y1 + (ua * (y2 - y1)));
		}
		return null;
	}*/

	public static bool isIntersection(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4)
	{
		float d = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1);
		float n_a = (x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3);
		float n_b = (x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3);

		if (Mathf.Approximately(d, 0)) return false;

		float ua = n_a / d;
		float ub = n_b / d;
		if (ua >= 0 && ua <= 1 && ub >= 0 && ub <= 1)
		{
			return true;
		}

		return false;
	}

	public static bool floatsEqual(float a, float b, float precision = 0.001f)
	{
		return Mathf.Abs(a - b) < precision;
	}

	public static float random() { return Random.value; }
	public static float random(float Range) { return Random.Range(0, Range); }
	public static float random(float Min, float Max) { return Random.Range(Min, Max); }
	public static int randomInt(int Range) { return (int)Random.Range(0, Range); }
	public static int randomInt(float Min, float Max) { return (int)Random.Range(Min, Max); }
	public static int randomInt(int Min, int Max) { return (int)Random.Range(Min, Max); }
	public static float randomSpread(float range, bool bothDirections = true) { return bothDirections ? Random.Range(-range, range) : Random.Range(-range / 2, range / 2); }
	public static bool randomBool(float chance = .5f) { return Random.Range(0f, 1f) < chance; }
	public static float randomSign() { return randomSign(.5f); }
	public static float randomSign(float chance = .5f) { return Random.Range(0f, 1f) < chance ? 1 : -1; }

	internal static float randomAngle()
	{
		return randomAngle(false);
	}

	internal static float randomAngle(bool radians = false)
	{
		if (radians)
			return random(PI_2);
		return random(360f);
	}

	internal static Vector2 randomVector2(float range)
	{
		return new Vector2(random(range), random(range));
	}

	internal static Vector2 randomVector2(float min, float max)
	{
		return new Vector2(random(min, max), random(min, max));
	}

	internal static Vector3 randomVector3(float range)
	{
		return new Vector3(random(range), random(range), random(range));
	}

	internal static Vector3 randomVector3(float min, float max)
	{
		return new Vector3(random(min, max), random(min, max), random(min, max));
	}

	public static float Sign(float Num)
	{
		if (Num >= 0) return 1;
		else return -1;
	}

	public static float limit(float value, float AbsMaxAndMin)
	{
		if (value > AbsMaxAndMin) value = AbsMaxAndMin;
		if (value < -AbsMaxAndMin) value = -AbsMaxAndMin;
		return value;
	}

	public static float ScaleClamp(float value, float inMin, float inMax, float outMin, float outMax)
	{
		if (outMax > outMin) return Mathf.Max(Mathf.Min(outMin + ((value - inMin) / (inMax - inMin)) * (outMax - outMin), outMax), outMin);
		else return Mathf.Max(Mathf.Min(outMin + ((value - inMin) / (inMax - inMin)) * (outMax - outMin), outMin), outMax);
	}

	public static float ScaleClamp(float value, float inMin, float inMid, float inMax, float outMin, float outMid, float outMax)
	{
		if (value < inMid) return ScaleClamp(value, inMin, inMid, outMin, outMid);
		else return ScaleClamp(value, inMid, inMax, outMid, outMax);
	}

	public struct InOutRange
	{
		public float inValueMin;
		public float inValueMax;
		public float outValueMin;
		public float outValueMax;

		public InOutRange(float inValueMin, float inValueMax, float outValueMin, float outValueMax)
		{
			this.inValueMin = inValueMin;
			this.inValueMax = inValueMax;
			this.outValueMin = outValueMin;
			this.outValueMax = outValueMax;
		}
	}

	public static float ScaleClamp(float value, List<InOutRange> values)
	{
		float result = value;
		 
		for (int i = 0; i < values.Count; i++)
		{
			InOutRange range = values[i];
			if((i == 0 || value >= range.inValueMin) && (value < range.inValueMax || i == values.Count - 1))
			{
				return Math.ScaleClamp(value, range.inValueMin, range.inValueMax, range.outValueMin, range.outValueMax);
			}
		} 
		return result;
	}

	public static float TowardValue(float currentValue, float targetValue, float speed)
	{
		if (targetValue > currentValue)
		{
			currentValue += Mathf.Abs(speed);
			if (currentValue > targetValue) currentValue = targetValue;
		}
		else if (targetValue < currentValue)
		{
			currentValue -= Mathf.Abs(speed);
			if (currentValue < targetValue) currentValue = targetValue;
		}
		return currentValue;
	}

	static public bool AngleInZone(float angle, float angleMiddle, float angleRange)
	{

		angle = NormalizeAngle(angle);
		angleMiddle = NormalizeAngle(angleMiddle);

		/*			angle = 360 - angle;
			angleMiddle = 360 - angle;*/

		float limitA = angleMiddle - angleRange;
		float limitB = angleMiddle + angleRange;

		if (limitA < 0)
		{
			limitA += 360;
			limitB += 360;
			angle += 360;
		}
		
		if (limitA < limitB)
		{
			if (angle >= limitA && angle <= limitB)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			if (angle >= limitB && angle <= limitA)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	
	public static Vector2 rotatePoint(float x, float y, float pivotX, float pivotY, float angle)
	{
		var point = new Vector2 (); 
		float radians = -angle / 180 * Mathf.PI;
		float dx = x - pivotX;
		float dy = pivotY - y;
		point.x = pivotX + Mathf.Cos(radians) * dx - Mathf.Sin(radians) * dy;
		point.y = pivotY - (Mathf.Sin(radians) * dx + Mathf.Cos(radians) * dy);
		return point;
	}

	static public float LengthDirX(float length, float direction)
	{
		return Mathf.Cos(direction * Mathf.Deg2Rad) * length;
	}

	static public float LengthDirY(float length, float direction)
	{
		return Mathf.Sin(direction * Mathf.Deg2Rad) * length;
	}
	
	public static Vector2 LengthDir(float length, float direction)
	{
		return new Vector2(Mathf.Cos(direction * Mathf.Deg2Rad) * length, Mathf.Sin(direction * Mathf.Deg2Rad) * length);
	}

	static public T choose<T>(params T[] Options)
	{
		int rnd = Mathf.FloorToInt(random() * Options.Length);
		return Options[rnd];
	}

	static public T choose<T>(List<T> Options)
	{
		int rnd = Mathf.FloorToInt(random() * Options.Count);
		return Options[rnd];
	}


	static public T chooseAndRemove<T>(List<T> Options)
	{
		int rnd = Mathf.FloorToInt(random() * Options.Count);
		T res = Options[rnd];
		Options.RemoveAt(rnd);// splice
		return res;
	}


	public static float sumArray(List<float> array)
	{
		float totalSum = 0;
		for (int i = 0; i < array.Count; i++)
		{
			totalSum += array[i];
		}
		return totalSum;
	}


	public static string formatSecondsToHMS(float value)
	{
		System.TimeSpan t = System.TimeSpan.FromSeconds(value);

		string answer = string.Format("{0:D2}h:{1:D2}m:{2:D2}s",
				t.Hours,
				t.Minutes,
				t.Seconds);

		return answer;

	}

	public static float rotateTowardAngle(float curAngle, float wantedAngle, float rotSpeed = 1500)
	{
		curAngle = NormalizeAngle(curAngle);
		wantedAngle = NormalizeAngle(wantedAngle);
		if (floatsEqual(curAngle, wantedAngle)) return curAngle;


		if (Mathf.Abs(curAngle - wantedAngle) >= 180)
		{
			if (curAngle > wantedAngle)
			{
				wantedAngle += 360;
			}
			else
			{
				curAngle += 360;
			}
		}
		bool wasBigger = curAngle > wantedAngle;

		float dif1;
		float dif2;

		if (wantedAngle > curAngle)
		{
			dif1 = wantedAngle - curAngle;
			dif2 = 360 - dif1;
			if (dif1 < dif2)
			{
				curAngle += rotSpeed;
			}
			else
			{
				curAngle -= rotSpeed;
			}
		}
		else
		{
			dif1 = curAngle - wantedAngle;
			dif2 = 360 - dif1;
			if (dif1 < dif2)
			{
				curAngle -= rotSpeed;
			}
			else
			{
				curAngle += rotSpeed;
			}
		}


		if (wasBigger == (curAngle > wantedAngle))
		{

		}
		else
		{
			curAngle = wantedAngle;
		}

		curAngle = NormalizeAngle(curAngle);

		return curAngle;

	}


	static public float SmallestAnglesDifference(float angle1, float angle2)
	{
		angle1 = normalizeAngle(angle1);
		angle2 = normalizeAngle(angle2);

		float diff1 = Mathf.Abs(angle1 - angle2);
		float diff2 = 0;
		if (angle1 <= 180)
		{
			diff2 += angle1;
		}
		else if (angle1 > 180)
		{
			diff2 += (360 - angle1);
		}
		if (angle2 <= 180)
		{
			diff2 += angle2;
		}
		else if (angle2 > 180)
		{
			diff2 += (360 - angle2);
		}

		if (diff1 <= diff2)
		{
			return diff1;
		}
		else
		{
			return diff2;
		} 
	}

	static public float roundDec(float numIn, float decimalPlaces)
	{
		int nExp = (int)(Mathf.Pow(10, decimalPlaces));
		float nRetVal = Mathf.Round(numIn * nExp) / nExp;
		return nRetVal;
	}

	public static float normalizeAngle(float angle)
	{
		if (angle > 360)
		{
			angle %= 360;
		}
		else if (angle < 0)
		{
			if (angle < -359)
			{
				angle = (-1) * (Mathf.Abs(angle) % 360);
			}
			angle += 360;
		}
		return angle;
	} 

	public static Vector2 VectorBetweenPointAndLine(Vector2 point, Vector2 lineStartPoint, float lineDirection)
	{
		return VectorBetweenPointAndLine(point - lineStartPoint, AngleToVector(lineDirection));
	}

	public static Vector2 VectorBetweenPointAndLine(Vector2 point, Vector2 line)
	{
		return point - (Vector2.Dot(point, line) / Vector2.Dot(line, line)) * line;
	}

	public static Vector3 VectorBetweenPointAndLine(Vector3 point, Vector3 line)
	{
		return point - (Vector2.Dot(point, line) / Vector2.Dot(line, line)) * line;
	}

	public static Vector3 VectorBetweenPointAndLine(Vector3 point, Vector3 lineStart, Vector3 lineEnd)
	{
		return point - NearestPointOnLine(point, lineStart, lineEnd);
	}

	public static Vector3 NearestPointOnLine(Vector3 from, Vector3 lineStart, Vector3 lineEnd)
	{
		var line = lineEnd - lineStart;

		var point = (Vector3.Dot(from - lineStart, line) / Vector3.Dot(line, line)) * line;
		
		if (Vector3.Dot(line, point) < 0) return lineStart;
		if (point.sqrMagnitude > line.sqrMagnitude) return lineEnd;

		return (Vector3.Dot(from - lineStart, line) / Vector3.Dot(line, line)) * line + lineStart;
	}

	public static Vector3 NearestPointOnLineUnclamped(Vector3 from, Vector3 lineStart, Vector3 lineEnd)
	{
		var line = lineEnd - lineStart;
		return (Vector3.Dot(from - lineStart, line) / Vector3.Dot(line, line)) * line + lineStart;
	}

	public static float DistanceBetweenPointAndLine(Vector2 point, Vector2 lineStart, Vector2 lineEnd)
	{
		return DistanceBetweenPointAndLine(point - lineStart, lineEnd - lineStart);
	}

	public static float DistanceBetweenPointAndLine(Vector2 point, Vector2 lineStartPoint, float lineDirection)
	{
		return DistanceBetweenPointAndLine(point - lineStartPoint, AngleToVector(lineDirection));
	}

	public static float DistanceBetweenPointAndLine(Vector2 point, Vector2 lineEnd)
	{
		return VectorBetweenPointAndLine(point, lineEnd).magnitude;
	}

	public static float VectorToAngle(Vector2 vector)
	{
		return Mathf.Atan2(vector.y, vector.x) * Mathf.Rad2Deg;
	}

	public static Vector2 AngleToVector(float angle)
	{
		angle = angle * Mathf.Deg2Rad;
		return new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
	}

	public static Vector2 GetClosestPointOnLineSegment(Vector2 lineStart, Vector2 lineEnd, Vector2 point)
	{
		Vector2 AP = point - lineStart;
		Vector2 AB = lineEnd - lineStart;

		float magnitudeAB = AB.sqrMagnitude;
		float ABAPproduct = Vector2.Dot(AP, AB);
		float distance = ABAPproduct / magnitudeAB;

		if (distance < 0)
		{
			return lineStart;
		}
		else if (distance > 1)
		{
			return lineEnd;
		}
		else
		{
			return lineStart + AB * distance;
		}
	}

	public static Vector3 GetClosestPointOnLineSegment(Vector3 lineStart, Vector3 lineEnd, Vector3 point)
	{
		Vector3 AP = point - lineStart;
		Vector3 AB = lineEnd - lineStart;

		float magnitudeAB = AB.sqrMagnitude;
		float ABAPproduct = Vector3.Dot(AP, AB);
		float distance = ABAPproduct / magnitudeAB;

		if (distance < 0)
		{
			return lineStart;
		}
		else if (distance > 1)
		{
			return lineEnd;
		}
		else
		{
			return lineStart + AB * distance;
		}
	}
}