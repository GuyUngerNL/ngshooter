using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class FlxGroup : FlxBasic
{
	#region Parenting

	[SerializeField] List<FlxBasic> _children;

	public List<FlxBasic> children {
		get {
			return _children;
		}
	}

	/// <summary>
	/// How many child objects with FlxBasic component there is. Does not count the 
	/// </summary>
	public bool hasChildren {
		get { 
			return _children != null;
		}
	}

	protected virtual void OnTransformChildrenChanged ()
	{
		_children = null;

		for(int i = 0, l = transform.childCount; i < l; i++) {
			var component = transform.GetChild (i).GetComponent <FlxBasic> ();
			if(component != null) {
				if(_children == null) {
					_children = new List<FlxBasic> ();
				}
				_children.Add (component);
			}
		}
	}

	#endregion


}
