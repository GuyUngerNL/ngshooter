using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class FlxCamera2D : FlxCamera
{

    public override Vector3 topLeftPoint
    {
        get
        {
            return flx.transformer.PositionFromUnity(transform.position + new Vector3(-screenSize.x / 2f, screenSize.y / 2f, -transform.position.z));
        }
    }

    public override Vector3 topRightPoint
    {
        get
        {
            return flx.transformer.PositionFromUnity(transform.position + new Vector3(screenSize.x / 2f, screenSize.y / 2f, -transform.position.z));
        }
    }

    public override Vector3 bottomLeftPoint
    {
        get
        {
            return flx.transformer.PositionFromUnity(transform.position + new Vector3(-screenSize.x / 2f, -screenSize.y / 2f, -transform.position.z));
        }
    }

    public override Vector3 bottomRightPoint
    {
        get
        {
            return flx.transformer.PositionFromUnity(transform.position + new Vector3(screenSize.x / 2f, -screenSize.y / 2f, -transform.position.z));
        }
    }

    public override Vector2 center
    {
        get
        {
            return flx.transformer.PositionFromUnity(transform.position + new Vector3(0, 0, -transform.position.z));
        }
    }

    public override float width
    {
        get { return camera ? (2f * lastUsedAspectRatio * camera.orthographicSize) : 0f; }
    }

    public override float height
    {
        get { return camera ? (2f * camera.orthographicSize) : 0f; }
    }

    public override Vector2 mousePosition
    {
        get
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            // create a plane at 0,0,0 whose normal points to +Y:
            Plane hPlane = new Plane(-Vector3.forward, Vector3.zero);
            // Plane.Raycast stores the distance from ray.origin to the hit point in this variable:
            float distance = 0;
            // if the ray hits the plane...
            if (hPlane.Raycast(ray, out distance))
            {
                // get the hit point:
                return actor.transformer.PositionFromUnity(ray.GetPoint(distance));
            }

            return Vector2.zero;
        }
    }


    protected override void LateUpdate()
    {
        base.LateUpdate();

        float screenAspectRatio = (float)Screen.width / Screen.height;

        if (screenAspectRatio == lastUsedAspectRatio && lastUsedZoom == zoom && lastUsedSize == screenSize)
        {
            return;
        }

        lastUsedAspectRatio = screenAspectRatio;
        lastUsedZoom = zoom;
        lastUsedSize = screenSize;

        float targetAspectRatio = screenSize.x / screenSize.y;

        if (targetAspectRatio < screenAspectRatio)
        {
            camera.orthographicSize = screenSize.y * 0.5f / zoom;
        }
        else
        {
            camera.orthographicSize = screenSize.y * 0.5f * (targetAspectRatio / screenAspectRatio) / zoom;
        }

        TriggerOnResize(this);
    }

    protected override void OnValidate()
    {
        base.OnValidate();

        flx.transformer = FlxTransformer2D.transformer;
    }
}
