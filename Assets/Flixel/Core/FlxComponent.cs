using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class FlxComponent : MonoBehaviour
{
	/// <summary>
	/// Alias for "GetComponent<FlxBasic>()"
	/// </summary>
	public FlxBasic flx { get { return GetComponent<FlxBasic>(); } }

	/// <summary>
	/// Alias for "GetComponent<FlxActor>()"
	/// </summary>
	public FlxActor actor { get { return GetComponent<FlxActor>(); } }

	/// <summary>
	/// Alias for "GetComponent<FlxGroup>()"
	/// </summary>
	public FlxGroup group { get { return GetComponent<FlxGroup>(); } }

	/// <summary>
	/// Shortcut for logging messages
	/// </summary>
	public static void log(params object[] values)
	{
		Log.msg(values);
	}
	public static void logError(params object[] values)
	{
		Log.msgError(values);
	}

	/// <summary>
	/// Alias for Time.smoothDeltaTime
	/// </summary>
	public float elapsed
	{
		get
		{
			return Time.deltaTime;
		}
	}

	public virtual void OnRecycle() { }
	public virtual void OnDecycle() { }

	public virtual void Reset()
	{
		if(gameObject.GetComponent<FlxBasic>() == null)
			gameObject.AddComponent<FlxActor>();
	}

	protected void EnableComponents(bool enable)
	{
		MonoBehaviour[] components = GetComponents<MonoBehaviour>();
		for (int j = 0; j < components.Length; j++)
		{
			MonoBehaviour c = components[j];
			c.enabled = enable;
		}
	}

	public void DisableColliders()
	{
		var colliders = GetComponentsInChildren<Collider>();
		for (int i = 0; i < colliders.Length; i++)
		{
			colliders[i].enabled = false;
		}
	}

	public void EnableColliders()
	{
		var colliders = GetComponentsInChildren<Collider>();
		for (int i = 0; i < colliders.Length; i++)
		{
			colliders[i].enabled = true;
		}
	}
	
}