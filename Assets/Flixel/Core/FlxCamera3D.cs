using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class FlxCamera3D : FlxCamera
{
    public override Vector3 topLeftPoint
    {
        get { return flx.transformer.PositionFromUnity(GetGroundPointFromRay(camera.ViewportPointToRay(new Vector3(0, 1, 0)))); }
    }

    public override Vector3 topRightPoint
    {
        get { return flx.transformer.PositionFromUnity(GetGroundPointFromRay(camera.ViewportPointToRay(new Vector3(1, 1, 0)))); }
    }

    public override Vector3 bottomLeftPoint
    {
        get { return flx.transformer.PositionFromUnity(GetGroundPointFromRay(camera.ViewportPointToRay(new Vector3(0, 0, 0)))); }
    }

    public override Vector3 bottomRightPoint
    {
        get { return flx.transformer.PositionFromUnity(GetGroundPointFromRay(camera.ViewportPointToRay(new Vector3(1, 0, 0)))); }
    }

    public override float width
    {
        get { return camera ? (2f * lastUsedAspectRatio * camera.orthographicSize) : 0f; }
    }

    public override float height
    {
        get { return camera ? (2f * camera.orthographicSize) : 0f; }
    }

    public override Vector2 center
    {
        get
        {
			if (camera.orthographicSize == 0f || float.IsInfinity(camera.orthographicSize)) return targetPosition;
			return flx.transformer.PositionFromUnity(GetGroundPointFromRay(camera.ViewportPointToRay(new Vector3(0.5f, 0.5f))));
		}
	}

    Vector3 GetGroundPointFromRay(Ray ray)
    {
        Vector3 direction = Mathf.Sign(ray.direction.y) == Mathf.Sign(ray.origin.y) ? -ray.direction : ray.direction;

        float scale = Mathf.Abs(ray.origin.y / direction.y);

        return ray.origin + direction * scale;
    }

    void Update()
    {
     
    }

    protected override void LateUpdate()
    {
        base.LateUpdate();

        float screenAspectRatio = (float)Screen.width / Screen.height;

        lastUsedAspectRatio = screenAspectRatio;
        lastUsedZoom = zoom;
        lastUsedSize = screenSize;

        float targetAspectRatio = screenSize.x / screenSize.y;

        float distance;

        if (targetAspectRatio < screenAspectRatio)
        {
            camera.orthographicSize = screenSize.y * 0.5f / zoom;
        }
        else
        {
            camera.orthographicSize = screenSize.y * 0.5f * (targetAspectRatio / screenAspectRatio) / zoom;
        }

        if (camera.orthographic)
        {
            distance = 1000f;
        }
        else
        {
            var angleModifier = Mathf.Tan((camera.fieldOfView / 2f) * Mathf.Deg2Rad);

            if (targetAspectRatio < screenAspectRatio)
            {
                distance = (screenSize.y / 2f) / angleModifier / zoom;
            }
            else
            {
                distance = (screenSize.y / 2f) / angleModifier / zoom * (targetAspectRatio / screenAspectRatio);
            }
        }

        actor.position = actor.transformer.PositionFromUnity(actor.transformer.PositionToUnity(targetPosition) - camera.transform.forward * distance);
    }

    protected override void OnValidate()
    {
        base.OnValidate();

        flx.transformer = FlxTransformer3D.transformer;
    }
}

    
