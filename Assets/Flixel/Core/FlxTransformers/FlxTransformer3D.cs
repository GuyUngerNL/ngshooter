using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class FlxTransformer3D : FlxTransformer
{
	private static FlxTransformer _default;

	public static new FlxTransformer transformer
	{
		get
		{
			if (!_default)
			{
				_default = Resources.Load<FlxTransformer>("Transformers/Flixel 3D");
			}

			return _default;
		}
	}

	public override Vector3 PositionToUnity (Vector3 position)
	{
		return new Vector3 (position.x, position.z, -position.y);
	}

	public override Vector3 PositionFromUnity (Vector3 position)
	{
		return new Vector3 (position.x, -position.z, position.y);
	}

	public override Vector3 RotationToUnity (Vector3 eulerAngles)
	{
		return new Vector3(eulerAngles.z, eulerAngles.x, eulerAngles.y);
	}

	public override Vector3 RotationFromUnity (Vector3 eulerAngles)
	{
		return new Vector3(eulerAngles.y ,eulerAngles.z, eulerAngles.x);
	}

	public override Vector3 ScaleToUnity (Vector3 scale)
	{
		return new Vector3 (scale.x, scale.z, scale.y);
	}

	public override Vector3 ScaleFromUnity (Vector3 scale)
	{
		return new Vector3 (scale.x, scale.z, scale.y);
	}
	
}

