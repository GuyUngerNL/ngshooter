using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class FlxTransformer2D : FlxTransformer 
{
    private static FlxTransformer _default;

    public static new FlxTransformer transformer
    {
        get
        {
            if (!_default)
            {
                _default = Resources.Load<FlxTransformer>("Transformers/Flixel 2D");
            }

            return _default;
        }
    }

    public override Vector3 PositionToUnity (Vector3 position)
	{
		return new Vector3(position.x, -position.y, position.z);
	}

	public override Vector3 PositionFromUnity (Vector3 position)
	{
		return new Vector3(position.x, -position.y, position.z);
	}

	public override Vector3 RotationToUnity (Vector3 eulerAngles)
	{
		return new Vector3 (eulerAngles.z, eulerAngles.y, eulerAngles.x);
	}

	public override Vector3 RotationFromUnity (Vector3 eulerAngles)
	{
		return new Vector3 (eulerAngles.z, eulerAngles.y, eulerAngles.x);
	}

	public override Vector3 ScaleToUnity (Vector3 scale)
	{
		return scale;
	}

	public override Vector3 ScaleFromUnity (Vector3 scale)
	{
		return scale;
	}
}

