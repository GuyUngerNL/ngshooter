﻿using System;

[Flags]
public enum FlxFlag
{
	Something 		= 1 << 0,
	BlaBlaBla 		= 1 << 1,
	Third 			= 1 << 2,
	Fourth	 		= 1 << 3,
}
