using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(FlxMotion), true)]
[CanEditMultipleObjects]
public class FlxMotionEditor : Editor
{
	SerializedProperty spVelocity;
	SerializedProperty spAcceleration;
	SerializedProperty spDeceleration;
	SerializedProperty spAngularVelocity;
	SerializedProperty spAngularAcceleration;
	SerializedProperty spAngularDeceleration;

    AbstractProperty<float, FlxMotion> apDirection;

	bool showBasicInfo = false;

    void OnEnable()
	{
		spVelocity = serializedObject.FindProperty("velocity");
		spAcceleration = serializedObject.FindProperty("acceleration");
		spDeceleration = serializedObject.FindProperty("deceleration");
		spAngularVelocity = serializedObject.FindProperty("angularVelocity");
		spAngularAcceleration = serializedObject.FindProperty("angularAcceleration");
		spAngularDeceleration = serializedObject.FindProperty("angularDeceleration");

        apDirection = new AbstractProperty<float, FlxMotion>(serializedObject.targetObjects);

        apDirection.inspectorGUI = (FlxMotion[] targets, float value) => {
            return EditorGUILayout.FloatField("Direction", value);
        };

        apDirection.updateValue = (FlxMotion motion) => {
            return motion.direction;
        };

        apDirection.changed = (FlxMotion[] targets, float value) => {
            Undo.RecordObjects(targets, "Changed Direction");

            foreach (var o in targets)
            {
                o.direction = value;
                EditorUtility.SetDirty(o.transform);
            }
        };
    }

	public override void OnInspectorGUI()
	{
		serializedObject.Update();
		EditorGUILayout.Space();

		showBasicInfo = EditorGUILayout.Foldout(showBasicInfo, "Motion properties");

		if(showBasicInfo)
		{
			EditorGUI.indentLevel++;

			apDirection.OnInspectorGUI();

			EditorGUILayout.Space();

			EditorGUILayout.PropertyField(spVelocity, true);

			EditorGUILayout.PropertyField(spAcceleration, true);

			EditorGUILayout.PropertyField(spDeceleration, true);

			EditorGUILayout.Space();

			EditorGUILayout.PropertyField(spAngularVelocity, true);

			EditorGUILayout.PropertyField(spAngularAcceleration, true);

			EditorGUILayout.PropertyField(spAngularDeceleration, true);

			EditorGUILayout.Space();

			EditorGUI.indentLevel--;
		}

		SerializedProperty prop = serializedObject.GetIterator();
		prop.NextVisible(true);

		var hasMoreProperties = false;

		do
		{
			hasMoreProperties = prop.NextVisible(false);
		}
		while (hasMoreProperties && prop.name != spAngularDeceleration.name);

		while (hasMoreProperties && prop.NextVisible(false))
		{
			EditorGUILayout.PropertyField(prop, true);
		}

		serializedObject.ApplyModifiedProperties();



	}

}
