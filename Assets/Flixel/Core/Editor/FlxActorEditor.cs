using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Flixel 
{
	[CustomEditor(typeof(FlxActor), true)]
	[CanEditMultipleObjects]
	public class FlxActorEditor : FlxBasicEditor
	{
		AbstractProperty<Vector3, FlxActor> apOffset;
		AbstractProperty<Vector3, FlxActor> apShake;
		AbstractProperty<Vector3, FlxActor> apShakeRotation;

		protected override void OnEnable()
		{
			base.OnEnable ();

			// Offset
			apOffset = new AbstractProperty<Vector3, FlxActor> (serializedObject.targetObjects);

			apOffset.inspectorGUI = (FlxActor[] targets, Vector3 value) => {
				return EditorGUILayout.Vector3Field ("Offset", value);	
			};

			apOffset.updateValue = (FlxActor flx) => {
				return flx.offset;
			};

			apOffset.changed = (FlxActor[] targets, Vector3 value) => {
				Undo.RecordObjects (soTransform.targetObjects, "Changed Offset");

				foreach(var o in targets){
					o.offset = value;
					EditorUtility.SetDirty(o.transform);
				}
			};

			// Shake
			apShake = new AbstractProperty<Vector3, FlxActor>(serializedObject.targetObjects);

			apShake.inspectorGUI = (FlxActor[] targets, Vector3 value) => {
				return EditorGUILayout.Vector3Field("Shake", value);
			};

			apShake.updateValue = (FlxActor flx) => {
				return flx.shake;
			};

			apShake.changed = (FlxActor[] targets, Vector3 value) => {
				Undo.RecordObjects(soTransform.targetObjects, "Changed Shake");

				foreach (var o in targets)
				{
					o.shake = value;
					EditorUtility.SetDirty(o.transform);
				}
			};

			// Shake Rotation
			apShakeRotation = new AbstractProperty<Vector3, FlxActor>(serializedObject.targetObjects);

			apShakeRotation.inspectorGUI = (FlxActor[] targets, Vector3 value) => {
				return EditorGUILayout.Vector3Field("Shake Rotation", value);
			};

			apShakeRotation.updateValue = (FlxActor flx) => {
				return flx.shakeRotation;
			};

			apShakeRotation.changed = (FlxActor[] targets, Vector3 value) => {
				Undo.RecordObjects(soTransform.targetObjects, "Changed Shake Rotation");

				foreach (var o in targets)
				{
					o.shakeRotation = value;
					EditorUtility.SetDirty(o.transform);
				}
			};
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI ();

			EditorGUILayout.Space ();

			var instance = target as FlxActor;

			var shakesDisabled = instance.GetComponent<ShakeManager>();

			EditorGUI.BeginDisabledGroup(shakesDisabled);

			apOffset.OnInspectorGUI ();
			apShake.OnInspectorGUI();
			apShakeRotation.OnInspectorGUI();

			EditorGUI.EndDisabledGroup();

			if(shakesDisabled)
			{
				EditorGUILayout.HelpBox("Shakes controled by the Shake Manager component.", MessageType.Info);
			}
		}


	}
}
