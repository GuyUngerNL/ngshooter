using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Flixel 
{
	[CustomEditor(typeof(FlxBasic), true)]
	[CanEditMultipleObjects]
	public class FlxBasicEditor : Editor
	{
		protected SerializedObject soTransform;

		SerializedProperty spTransformer;
		//SerializedProperty spFlag;

		AbstractProperty<Vector3, FlxBasic> apPosition;
		AbstractProperty<Vector3, FlxBasic> apRotation;
		AbstractProperty<Vector3, FlxBasic> apScale;
		AbstractProperty<bool, FlxBasic> apExist;

		AbstractProperty<bool, FlxBasic> apPhysics;


		FlxTransformer[] transforms;
		GameObject[] gameObjects;

		static bool useWorldPosition
		{
			get
			{
				return EditorPrefs.GetBool("FlxBasicEditor_useWorldPosition", false);
			}
			set
			{
				EditorPrefs.SetBool("FlxBasicEditor_useWorldPosition", value);
			}
		}

		static bool useWorldRotation
		{
			get
			{
				return EditorPrefs.GetBool("FlxBasicEditor_useWorldRotation", false);
			}
			set
			{
				EditorPrefs.SetBool("FlxBasicEditor_useWorldRotation", value);
			}
		}

		string[] transformNames = new string[]{
			"Unity",
			"Flx 2D",
			"Flx 3D",
		};

		protected virtual void OnDestroy()
		{
			foreach(var t in soTransform.targetObjects) {
				if (t && t.hideFlags == HideFlags.HideInInspector && (t as Component).GetComponent <FlxBasic> () == null) {
					t.hideFlags = HideFlags.None;
				}
			}
		}

		protected virtual void OnEnable()
		{
			soTransform = Noopol.EditorGUIAddons.GetSerializedComponent<Transform> (serializedObject);

			foreach(var i in serializedObject.targetObjects) {
				ComponentUtilityAddons.MoveComponentToTop (i as Component);
			}

			foreach(var t in soTransform.targetObjects) {
				if (t.hideFlags != HideFlags.HideInInspector) {
					t.hideFlags = HideFlags.HideInInspector;
					ComponentUtility.MoveComponentUp (t as Component);
				}
			}

			spTransformer = serializedObject.FindProperty("m_transfomer");
			//spFlag = serializedObject.FindProperty("flag");

			transforms = new FlxTransformer[]{
				FlxTransformer.transformer,
				FlxTransformer2D.transformer,
				FlxTransformer3D.transformer,
			};



			
			// EXIST
			apExist = new AbstractProperty<bool, FlxBasic>(serializedObject.targetObjects);

			apExist.inspectorGUI = (FlxBasic[] targets, bool value) => {
				return EditorGUILayout.ToggleLeft("Exists", value, GUILayout.Width(60f));
			};

			apExist.updateValue = (FlxBasic flx) => {
				return flx.exists;
			};

			apExist.changed = (FlxBasic[] targets, bool value) => {
				Undo.RecordObjects(targets, "Changed Exists");

				foreach (var o in targets)
				{
					o.exists = value;
					EditorUtility.SetDirty(o);
					EditorUtility.SetDirty(o.gameObject);
				}

				UnityEditor.SceneManagement.EditorSceneManager.MarkAllScenesDirty();
			};


			// POSITION
			apPosition = new AbstractProperty<Vector3, FlxBasic> (serializedObject.targetObjects);

			apPosition.inspectorGUI = (FlxBasic[] targets, Vector3 value) => {
				return EditorGUILayout.Vector3Field ("Position", value);
			};

			apPosition.updateValue = (FlxBasic flx) => {
				if(useWorldPosition)
				{
					return flx.position;
				}
				else
				{
					return flx.localPosition;
				}
			};

			apPosition.changed = (FlxBasic[] targets, Vector3 value) => {
				Undo.RecordObjects (soTransform.targetObjects, "Changed Position");

				foreach(var o in targets){
					if (useWorldPosition)
					{
						o.position = value;
					}
					else
					{
						o.localPosition = value;
					}
					EditorUtility.SetDirty(o.transform);
				}
			};

		
		
			// ROTATION
			apRotation = new AbstractProperty<Vector3, FlxBasic> (serializedObject.targetObjects);

			apRotation.inspectorGUI = (FlxBasic[] targets, Vector3 value) => {
				return EditorGUILayout.Vector3Field ("Rotation", value);
			};

			apRotation.updateValue = (FlxBasic flx) => {
				if(useWorldRotation)
				{
					return flx.rotation;
				}
				else
				{
					return flx.localRotation;
				}
			};

			apRotation.changed = (FlxBasic[] targets, Vector3 value) => {
				Undo.RecordObjects (soTransform.targetObjects, "Changed Rotation");

				foreach(var o in targets){
					if (useWorldRotation)
					{
						o.rotation = value;
					}
					else
					{
						o.localRotation = value;
					}
					EditorUtility.SetDirty(o.transform);
				}
			};



			// ROTATION
			apScale = new AbstractProperty<Vector3, FlxBasic> (serializedObject.targetObjects);

			apScale.inspectorGUI = (FlxBasic[] targets, Vector3 value) => {
				return EditorGUILayout.Vector3Field ("Scale", value);	
			};

			apScale.updateValue = (FlxBasic flx) => {
				return flx.scale;
			};

			apScale.changed = (FlxBasic[] targets, Vector3 value) => {
				Undo.RecordObjects (soTransform.targetObjects, "Changed Scale");

				foreach(var o in targets){
					o.scale = value;
					EditorUtility.SetDirty(o.transform);
				}
			};


			// PHYSICS
			apPhysics = new AbstractProperty<bool, FlxBasic>(serializedObject.targetObjects);

			apPhysics.inspectorGUI = (FlxBasic[] targets, bool value) => {
				return EditorGUILayout.ToggleLeft("Physics", value, GUILayout.Width(60f));
			};

			apPhysics.updateValue = (FlxBasic flx) => {
				return flx.GetComponent<Rigidbody>() && flx.GetComponent<Collider>();
			};

			apPhysics.changed = (FlxBasic[] targets, bool value) => {
				foreach (var o in targets)
				{
					if(value)
					{
						var rigidbody = Undo.AddComponent<Rigidbody>(o.gameObject);
						rigidbody.isKinematic = true;

						UnityEditorInternal.InternalEditorUtility.SetIsInspectorExpanded(rigidbody, false);
						if (!o.GetComponent<Collider>())
						{
							var collider = Undo.AddComponent<SphereCollider>(o.gameObject);
							collider.isTrigger = true;
						}
					}
					else
					{
						var rigidBody = o.GetComponent<Rigidbody>();

						if (rigidBody)
						{
							Undo.DestroyObjectImmediate(rigidBody);
						}

						var colliders = o.GetComponents<Collider>();

						foreach(var c in colliders)
						{
							Undo.DestroyObjectImmediate(c);
						}
						
					}
				}

				EditorGUIUtility.ExitGUI();
			};

		}

		private int GetSelectedTransformer()
		{
			FlxBasic basic = target as FlxBasic;

			if (basic.transformer.GetType () == typeof(FlxTransformer2D))
				return 1;
			if (basic.transformer.GetType () == typeof(FlxTransformer3D))
				return 2;

			return 0;
		}

		public override void OnInspectorGUI()
		{
			FlxBasic basic = target as FlxBasic;

			var flxBasics = basic.gameObject.GetComponents<FlxBasic>();

			if(flxBasics.Length > 1)
			{
				foreach(var flx in flxBasics)
				{
					if (flx == basic) continue;
					GameObject.DestroyImmediate(flx, true);
					EditorGUIUtility.ExitGUI();
				}
			}

			var targets = serializedObject.targetObjects.Cast<FlxBasic>();

			serializedObject.Update();
			EditorGUILayout.Space();

			EditorGUILayout.BeginHorizontal ();

			var isBasic = !(target is FlxActor);
			var canBeBasic = true;

			foreach (var t in targets)
			{
				if((t as FlxBasic).gameObject.GetComponent<FlxMotion>())
				{
					canBeBasic = false;
					break;
				}
			}

			EditorGUI.BeginDisabledGroup(!canBeBasic);

			if (EditorGUILayout.ToggleLeft("Basic", isBasic, GUILayout.Width(60f)) != isBasic)
			{
				if (isBasic)
				{
					foreach (var t in targets)
					{
						(t as FlxBasic).gameObject.AddComponent<FlxActor>();
					}
				}
				else
				{
					foreach (var t in targets)
					{
						(t as FlxBasic).gameObject.AddComponent<FlxBasic>();
					}
				}

				foreach (var t in targets)
				{
					GameObject.DestroyImmediate(t, true);
				}

				EditorGUIUtility.ExitGUI();
			}

			EditorGUI.EndDisabledGroup();

			apExist.OnInspectorGUI();

			apPhysics.OnInspectorGUI();

			GUILayout.FlexibleSpace ();

			EditorGUI.BeginChangeCheck ();
			EditorGUI.showMixedValue = spTransformer.hasMultipleDifferentValues;
			int selectedTransform = EditorGUILayout.Popup (GetSelectedTransformer(), transformNames, GUILayout.Width (60f));
			EditorGUI.showMixedValue = false;
			if (EditorGUI.EndChangeCheck ()) {
				spTransformer.objectReferenceValue = transforms[selectedTransform];

				foreach(var t in targets)
				{
					t.transformer = transforms[selectedTransform];
				}
			}

			EditorGUILayout.EndHorizontal ();

			EditorGUILayout.Space ();

			apPosition.OnInspectorGUI ();

			var r = GUILayoutUtility.GetLastRect();
			r.width = EditorGUIUtility.labelWidth;
			r.x = r.width - 20f;
			r.width = 20f;
			r.height = EditorGUIUtility.singleLineHeight;

			if(GUI.Button(r, useWorldPosition ? new GUIContent("G", "Global position. Click to switch to local.") : new GUIContent("L", "Local position. Click to switch to global.")))
			{
				useWorldPosition = !useWorldPosition;
			}

			apRotation.OnInspectorGUI ();

			r = GUILayoutUtility.GetLastRect();
			r.width = EditorGUIUtility.labelWidth;
			r.x = r.width - 20f;
			r.width = 20f;
			r.height = EditorGUIUtility.singleLineHeight;

			if (GUI.Button(r, useWorldRotation ? new GUIContent("G", "Global rotation. Click to switch to local.") : new GUIContent("L", "Local rotation. Click to switch to global.")))
			{
				useWorldRotation = !useWorldRotation;
			}


			apScale.OnInspectorGUI ();

			serializedObject.ApplyModifiedProperties();
		}


		public virtual void OnSceneGUI()
		{
			Repaint ();
		}
	}
}
