using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
[DisallowMultipleComponent]
public class FlxBasic : FlxComponent
{
	#region General Properties

	public FlxFlag flag;

	bool _exists;

	public bool exists
	{
		get
		{
#if UNITY_EDITOR
			if (Application.isPlaying)
			{
#endif
				return _exists;
#if UNITY_EDITOR
			}
			else
			{
				return gameObject.activeSelf;
			}
#endif
		}
		set
		{
#if UNITY_EDITOR
			if (Application.isPlaying)
			{
#endif
				if (_exists == value)
				{
					return;
				}

				if (value)
				{
					_exists = true;
					gameObject.SetActive(true);
					if(_exists)
					{
						if (!poolEnabled && pool != null)
						{
							pool.Enable(this);
							poolEnabled = true;
						}
						OnRecycle();
					}
				}
				else
				{
					_exists = false;
					gameObject.SetActive(false);
					if(!_exists)
					{
						if (poolEnabled && pool != null)
						{
							pool.Disable(this);
							poolEnabled = false;
						}
						OnDecycle();
					}
				}
#if UNITY_EDITOR
			}
			else
			{
				gameObject.SetActive(value);
			}
#endif
		}
	}

	bool _visible = true;

	public bool visible
	{
		get
		{
			return _visible;
		}
		set
		{
			if(_visible == value)
			{
				return;
			}

			_visible = value;

			var renderers = GetComponentsInChildren<Renderer>();

			for(int i = 0; i < renderers.Length; i++)
			{
				renderers[i].enabled = visible;
			}
		}
	}

	public enum KILL_TYPE{
		NORMAL,
		FORCE_KILL
	}
	public virtual void Kill(KILL_TYPE killType)
	{
		exists = false;
	}

	[NonSerialized]
	public ObjectPool pool;

	[NonSerialized]
	public int poolIndex = -1;

	[NonSerialized]
	public bool poolEnabled;

	/// <summary>
	/// Coordinate system transformer. It has neccecery functions to convert position/rotation/scale between coordinate systems
	/// </summary>
	/// <value>The transformer.</value>
	[SerializeField]
	private FlxTransformer m_transfomer;

	public FlxTransformer transformer
	{
		get
		{
			return m_transfomer;
		}
		set
		{
			if (value != null && value != m_transfomer)
			{
				Vector3 tmpPosition = localPosition;
				Vector3 tmpRotation = rotation;
				Vector3 tmpScale = scale;

				m_transfomer = value;

				localPosition = tmpPosition;
				rotation = tmpRotation;
				scale = tmpScale;
			}
		}
	}

	#endregion

	#region Position

	/// <summary>
	/// Position in the world
	/// </summary>
	public virtual Vector3 position
	{
		get
		{
			return transformer.PositionFromUnity(transform.position);
		}
		set
		{
			transform.position = transformer.PositionToUnity(value);
		}
	}

	public float x { get { return position.x; } set { position = new Vector3(value, position.y, position.z); } }
	public float y { get { return position.y; } set { position = new Vector3(position.x, value, position.z); } }
	public float z { get { return position.z; } set { position = new Vector3(position.x, position.y, value); } }


	/// <summary>
	/// Position relative to the parent.
	/// </summary>
	public virtual Vector3 localPosition
	{
		get
		{
			return transformer.PositionFromUnity(transform.localPosition);
		}
		set
		{
			transform.localPosition = transformer.PositionToUnity(value);
		}
	}

	public float localX { get { return localPosition.x; } set { localPosition = new Vector3(value, localPosition.y, localPosition.z); } }
	public float localY { get { return localPosition.y; } set { localPosition = new Vector3(localPosition.x, value, localPosition.z); } }
	public float localZ { get { return localPosition.z; } set { localPosition = new Vector3(localPosition.x, localPosition.y, value); } }



	/// <summary>
	/// Position relative to the room. or if the room does not exist use world
	/// </summary>
	
	public FlxBasic SetPosition(Vector3 position)
	{
		this.position = position;
		return this;
	}

	public FlxBasic SetPosition(float x = 0, float y = 0, float z = 0)
	{
		this.position = new Vector3(x, y, z);
		return this;
	}

	public FlxBasic SetLocalPosition(Vector3 position)
	{
		this.localPosition = position;
		return this;
	}

	public FlxBasic SetLocalPosition(float x = 0, float y = 0, float z = 0)
	{
		this.localPosition = new Vector3(x, y, z);
		return this;
	}

	#endregion

	#region Rotation

	/// <summary>
	/// A Vector3 representation of three rotation axis; Angle, Pitch and Roll.
	/// </summary>
	/// <value>The rotation.</value>
	/// 

	public virtual Vector3 rotation
	{
		get
		{
			return transformer.RotationFromUnity(transform.eulerAngles);
		}
		set
		{
			transform.eulerAngles = transformer.RotationToUnity(value);
		}
	}

	public float angle
	{
		get
		{
			return rotation.x;
		}
		set
		{
			Vector3 temp = rotation;
			rotation = new Vector3(value, temp.y, temp.z);
		}
	}

	public float pitch
	{
		get
		{
			return rotation.y;
		}
		set
		{
			Vector3 temp = rotation;
			rotation = new Vector3(temp.x, value, temp.z);
		}
	}

	public float roll
	{
		get
		{
			return rotation.z;
		}
		set
		{
			Vector3 temp = rotation;
			rotation = new Vector3(temp.x, temp.y, value);
		}
	}



	/// <summary>
	/// A Vector3 representation of three rotation axis; Angle, Pitch and Roll.
	/// </summary>
	/// <value>The rotation.</value>

	public virtual Vector3 localRotation
	{
		get
		{
			return transformer.RotationFromUnity(transform.localEulerAngles);
		}
		set
		{
			transform.localEulerAngles = transformer.RotationToUnity(value);
		}
	}

	public float localAngle
	{
		get
		{
			return localRotation.x;
		}
		set
		{
			Vector3 temp = localRotation;
			localRotation = new Vector3(value, temp.y, temp.z);
		}
	}

	public float localPitch
	{
		get
		{
			return localRotation.y;
		}
		set
		{
			Vector3 temp = localRotation;
			localRotation = new Vector3(temp.x, value, temp.z);
		}
	}

	public float localRoll
	{
		get
		{
			return localRotation.z;
		}
		set
		{
			Vector3 temp = localRotation;
			localRotation = new Vector3(temp.x, temp.y, value);
		}
	}


	public FlxBasic SetAngle(float angle)
	{
		this.angle = angle;
		return this;
	}

	public FlxBasic SetRotation(Vector3 rotation)
	{
		this.position = rotation;
		return this;
	}

	public FlxBasic SetRotation(float x = 0, float y = 0, float z = 0)
	{
		this.rotation = new Vector3(x, y, z);
		return this;
	}

	public FlxBasic SetLocalRotation(Vector3 rotation)
	{
		this.localPosition = rotation;
		return this;
	}

	public FlxBasic SetLocalRotation(float x = 0, float y = 0, float z = 0)
	{
		this.localRotation = new Vector3(x, y, z);
		return this;
	}


	public float AngleTo(float towardsX, float towardsY)
	{
		return Math.AngleTo(x, y, towardsX, towardsY);
	}

	public float AngleTo(FlxBasic targetObject)
	{
		return targetObject ? Math.AngleTo(x, y, targetObject.x, targetObject.y) : flx.angle;
	}

	public float AngleTo(FlxComponent targetObject)
	{
		return targetObject ? Math.AngleTo(x, y, targetObject.actor.x, targetObject.actor.y) : flx.angle;
	}

	public float AngleTo(Vector2 targetVector2)
	{
		return Math.AngleTo(x, y, targetVector2.x, targetVector2.y);
	}

	public float DistanceTo(float towardsX, float towardsY)
	{
		return Math.Distance(x, y, towardsX, towardsY);
	}

	public float DistanceTo(FlxBasic targetObject)
	{
		return targetObject ? Math.Distance(x, y, targetObject.x, targetObject.y) : float.MaxValue;
	}

	public float DistanceTo(FlxActor targetObject)
	{
		return targetObject ? Math.Distance(x, y, targetObject.x, targetObject.y) : float.MaxValue;
	}

	public float DistanceTo(FlxComponent targetObject)
	{
		return targetObject ? Math.Distance(x, y, targetObject.actor.x, targetObject.actor.y) : float.MaxValue;
	}

	public float DistanceTo(Vector2 targetVector2)
	{
		return Math.Distance(x, y, targetVector2.x, targetVector2.y);
	}

	public Vector3 normalizedRotation
	{
		get
		{
			Vector3 temp = rotation;

			return new Vector3(
				PH.normalizeAngle(temp.x),
				PH.normalizeAngle(temp.y),
				PH.normalizeAngle(temp.z)
			);
		}
	}

	/// <summary>
	/// Sets the rotation variables between 0 - 360.
	/// </summary>
	public void NormalizeRotation()
	{
		Vector3 temp = rotation;

		rotation = new Vector3(
			PH.normalizeAngle(temp.x),
			PH.normalizeAngle(temp.y),
			PH.normalizeAngle(temp.z)
		);
	}

	#endregion

	#region Scale 

	public virtual Vector3 scale
	{
		get
		{
			return transformer.ScaleFromUnity(transform.localScale);
		}
		set
		{
			transform.localScale = transformer.ScaleToUnity(value);
		}
	}

	public float scaleX { get { return scale.x; } set { scale = new Vector3(value, scale.y, scale.z); } }
	public float scaleY { get { return scale.y; } set { scale = new Vector3(scale.x, value, scale.z); } }
	public float scaleZ { get { return scale.z; } set { scale = new Vector3(scale.x, scale.y, value); } }

	/// <summary>
	/// Takes parent scale into account. Read only.
	/// </summary>
	public virtual Vector3 totalScale
	{
		get
		{
			return transformer.ScaleFromUnity(transform.lossyScale);
		}
	}

	#endregion

	#region Parenting

	public FlxBasic parent
	{
		get
		{
			return transform.parent ? transform.parent.GetComponent<FlxBasic>() : null;
		}
		set
		{
			transform.SetParent(value.transform, false);
		}
	}

	#endregion

	


	#region Public Getters

	public bool isOnScreen
	{
		get
		{
			var pos = position;

			var tl = FlxCamera.main.topLeftPoint;

			if (pos.x < tl.x || pos.y < tl.y) return false;

			var br = FlxCamera.main.bottomRightPoint;

			if (pos.x > br.x || pos.y > br.y) return false;

			return true;
		}
	}

	public Vector2 screenPosition
	{
		get
		{
			return Camera.main.WorldToScreenPoint(transform.position);
		}
	}

	#endregion

	#region Unity Methods Implementation

	protected virtual void OnDestroy()
	{
		if(pool != null && poolIndex >= 0)
		{
			pool.Remove(this);
		}
	}

	override public void Reset()
	{
		base.Reset();
		if (m_transfomer == null)
		{
			m_transfomer = FlxTransformer.@default;
		}
	}

	protected virtual void Awake()
	{
		if(m_transfomer == null)
		{
			m_transfomer = FlxTransformer.@default;
		}
	}

	protected virtual void Start()
	{
		exists = true;
	}

	List<FlxComponent> _components = null;

	public void UpdateComponents()
	{
		if (_components == null)
		{
			_components = new List<FlxComponent>();
		}

		GetComponents<FlxComponent>(_components);
	}

	public override void OnRecycle()
	{
		if (_components == null)
		{
			_components = new List<FlxComponent>();
			GetComponents<FlxComponent>(_components);
		}

		for (int i = 0; i < _components.Count; i++)
		{
			if (_components[i] == this)
			{
				continue;
			}

			_components[i].OnRecycle();
		}
	}

	public override void OnDecycle()
	{
		if (_components == null)
		{
			_components = new List<FlxComponent>();
			GetComponents<FlxComponent>(_components);
		}

		for (int i = 0; i < _components.Count; i++)
		{
			if (_components[i] == this)
			{
				continue;
			}

			_components[i].OnDecycle();
		}

		transform.parent = null;
	}

	#endregion

}

