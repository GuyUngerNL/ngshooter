using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class FlxTransformer : ScriptableObject
{
	private static FlxTransformer _default;


	public static FlxTransformer @default
	{
		get
		{
			if (!_default)
			{
				_default = FlxTransformer3D.transformer;
			}

			return _default;
		}
	}

	private static FlxTransformer _transformer;

	public static FlxTransformer transformer
	{
		get
		{
			if (!_transformer)
			{
				_transformer = Resources.Load<FlxTransformer>("Transformers/Unity");
			}

			return _transformer;
		}
	}

	/// <summary>
	/// Transforms the position from this coordinate system to Unity coordinate system.
	/// </summary>
	public virtual Vector3 PositionToUnity(Vector3 position)
	{
		return position;
	}

	/// <summary>
	/// Transforms the position from Unity coordinate system to this coordinate system.
	/// </summary>
	public virtual Vector3 PositionFromUnity(Vector3 position)
	{
		return position;
	}

	/// <summary>
	/// Transforms the rotation from this coordinate system to Unity coordinate system.
	/// </summary>
	public virtual Vector3 RotationToUnity(Vector3 eulerAngles)
	{
		return eulerAngles;
	}

	/// <summary>
	/// Transforms the rotation from Unity coordinate system to this coordinate system.
	/// </summary>
	public virtual Vector3 RotationFromUnity(Vector3 eulerAngles)
	{
		return eulerAngles;
	}

	/// <summary>
	/// Transforms the scale from this coordinate system to Unity coordinate system.
	/// </summary>
	public virtual Vector3 ScaleToUnity(Vector3 scale)
	{
		return scale;
	}

	/// <summary>
	/// Transforms the s from Unity coordinate system to this coordinate system.
	/// </summary>
	public virtual Vector3 ScaleFromUnity(Vector3 scale)
	{
		return scale;
	}

	/// <summary>
	/// Transforms the position from Unity coordinate system to this coordinate system.
	/// </summary>
	/*
	static public Vector3 PositionToUnity(Vector3 position)
	{
		return new Vector3(position.x, position.z, -position.y);
	}
	static public Vector3 PositionFromUnity(Vector3 position)
	{
		return new Vector3(position.x, position.z, -position.y);
	}*/
}

