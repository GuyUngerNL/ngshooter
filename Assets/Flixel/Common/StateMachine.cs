﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StateEvent
{
    Enter,
    Exit,
    Update
}

public class StateMachine<T> where T : struct, IComparable, IFormattable, IConvertible
{
    public class State
    {
        public List<StateUpdateEvent> updates = new List<StateUpdateEvent>();

		[NonSerialized]
        public List<T> toStates = new List<T>();
        public List<List<StateTransitionCondition>> toConditions = new List<List<StateTransitionCondition>>();
    }

    public delegate void StateUpdateEvent(StateEvent e);
    public delegate void StateChangeEvent(T from, T to);
    public delegate bool StateTransitionCondition();

    public event StateChangeEvent OnStateChange;

    T _activeState;
    T _previousState;

    float _timeOfLastStateChange;

    Dictionary<T, State> _states = new Dictionary<T, State>();


    public T activeState { get { return _activeState; } }
    public T previousState { get { return _previousState; } }

    public float timeSinceLastStateChange { get { return Time.time - _timeOfLastStateChange; } }

    /// <summary>
    /// Helper variables to speed up iteration
    /// </summary>
    State s;
    List<StateTransitionCondition> c;
    int i;
    int j;

    public StateMachine()
    {
        Reset();
    }

    public void Reset()
    {
        _activeState = default(T);
        _previousState = default(T);
        _timeOfLastStateChange = Time.time;
    }

    public void Add(T state, StateUpdateEvent update)
    {
        if (!_states.TryGetValue(state, out s))
        {
            s = new State();
            _states.Add(state, s);
        }

        s.updates.Add(update);
    }

    public void AddTransitionCondition(T from, T to, StateTransitionCondition condition)
    {
        if (!_states.TryGetValue(from, out s))
        {
            s = new State();
            _states.Add(from, s);
        }

        i = s.toStates.IndexOf(to);

        if(i == -1)
        {
            c = new List<StateTransitionCondition>();
            c.Add(condition);
            s.toStates.Add(to);
            s.toConditions.Add(c);
        }
        else
        {
            s.toConditions[i].Add(condition);
        }
    }

    public void Update()
    {
        if(!_states.TryGetValue(_activeState, out s))
        {
            return;
        }

        for(i = 0; i < s.toStates.Count; i++)
        {
            c = s.toConditions[i];

            for(j = 0; j < c.Count; j++)
            {
                if (c[j]())
                {
                    ChangeState(s.toStates[i]);
                    return;
                }
            }
        }

        for(i = 0; i < s.updates.Count; i++)
        {
            s.updates[i](StateEvent.Update);
        }
    }

    public void ChangeState(T to)
    {
        if(_states.TryGetValue(_activeState, out s))
        {
            for(i= 0; i < s.updates.Count; i++)
            {
                s.updates[i](StateEvent.Exit);
            }
        }

        _previousState = _activeState;
        _activeState = to;
        _timeOfLastStateChange = Time.time;

        if(OnStateChange != null)
        {
            OnStateChange(_previousState, _activeState);
        }

        if (_states.TryGetValue(_activeState, out s))
        {
            for (i = 0; i < s.updates.Count; i++)
            {
                s.updates[i](StateEvent.Enter);
            }
            for (i = 0; i < s.updates.Count; i++)
            {
                s.updates[i](StateEvent.Update);
            }
        }
    }

}

