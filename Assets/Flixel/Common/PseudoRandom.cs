﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PseudoRandom<T> : UList<T>
{
	public float chance = 0.5f;

	public PseudoRandom(params T[] items) : base(items.Length)
	{
		var length = items.Length;

		for (int i = 0; i < length; i++)
		{
			Add(items[length - i - 1]);
		}
	}

	public T next
	{
		get
		{
			if (Count == 0) return default(T);
			if (Count == 1) return this[0];

			var itemIndex = Count - 1;
			var item = this[itemIndex];

			float chanceRnd = Random.Range(0f, 0.999f);
			int countRnd = Mathf.RoundToInt((itemIndex - 1) * Mathf.Pow(chanceRnd, 1f / chance));

			RemoveAt(itemIndex);
			Insert(countRnd, item);

			return item;
		}
	}
}

static class PseudoRandom
{
	/// <summary>
	/// Range of numbers calculated like this 'for(to; to < from; step)'
	/// </summary>
	/// <param name="from">inclusive</param>
	/// <param name="to">exclusive</param>
	/// <param name="step">step</param>
	/// <returns></returns>
	public static PseudoRandom<int> Range(int from, int to, int step)
	{
		int[] list = new int[((to - from) / step)];

		for (int i = 0, j = from; j < to; i++, j += step)
		{
			list[i] = j;
		}

		return new PseudoRandom<int>(list);
	}

	public static PseudoRandom<float> RangeByCount(float from, float to, int count)
	{
		float step  = (to - from) / count;
		float[] list = new float[count];

		for (int i = 0; i < count; i++)
		{
			list[i] = from + step * i;
		}

		return new PseudoRandom<float>(list);
	}
}