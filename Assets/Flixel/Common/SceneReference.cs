﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[Serializable]
public class SceneReference
{
	[SerializeField]
	string _sceneName;

	public string sceneName
	{
		get
		{
			return _sceneName;
		}
		set
		{
			_sceneName = value;
		}
	}

	public bool isValid
	{
		get
		{
			return !string.IsNullOrEmpty(_sceneName);
		}
	}
}