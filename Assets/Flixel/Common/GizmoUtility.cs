﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GizmoUtility
{

	public static void DrawGizmoPrefab(GameObject prefab, Transform target, Vector3 positionOffset)
	{
		DrawGizmoPrefab(prefab, target, positionOffset, Color.gray);
	}

	public static void DrawGizmoPrefab(GameObject prefab, Transform target, Vector3 positionOffset, Color color)
	{
		DrawGizmoPrefab(prefab, target, positionOffset, Color.gray, color.a);
	}

	public static void DrawGizmoPrefab(GameObject prefab, Transform target, Vector3 positionOffset, Color color, float transparency)
	{
		if (target == null || prefab == null) return;

		color.a = transparency;

		var c = Gizmos.color;
		var m = Gizmos.matrix;

		Gizmos.color = color;
		Gizmos.matrix = target.localToWorldMatrix;

		foreach (var mr in prefab.GetComponentsInChildren<MeshFilter>())
		{
			if (mr.sharedMesh == null) continue;
			Gizmos.DrawMesh(mr.sharedMesh, mr.transform.position + positionOffset, mr.transform.rotation, mr.transform.lossyScale);
		}

		Gizmos.color = c;
		Gizmos.matrix = m;
	}

	public static void DrawPrefab(GameObject prefab, Transform target, Vector3 positionOffset = default(Vector3), int materialPass = default(int))
	{
		DrawGizmoPrefab(prefab, target, positionOffset, Color.white, 0.1f);

		if (target == null || prefab == null) return;

		foreach (var mf in prefab.GetComponentsInChildren<MeshFilter>())
		{
			if (mf.sharedMesh == null) continue;

			var mr = mf.GetComponent<MeshRenderer>();

			if (mr == null || mr.sharedMaterial == null) continue;

			var matrix = target.localToWorldMatrix * mf.transform.localToWorldMatrix;
			matrix.SetColumn(3, matrix.GetColumn(3) + (Vector4)positionOffset);

			mr.sharedMaterial.SetPass(materialPass);
			Graphics.DrawMeshNow(mf.sharedMesh, matrix);
		}
	}
}
