﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager
{

    public static void Save(string key, object data, bool writeToDisk = false)
    {
        PlayerPrefs.SetString(key, JsonUtility.ToJson(data));

        if (writeToDisk)
        {
            PlayerPrefs.Save();
        }
    }

    public static object Load(string key, Type type)
    {
        object data = Activator.CreateInstance(type);

        var stringData = PlayerPrefs.GetString(key, string.Empty);

        if (!string.IsNullOrEmpty(stringData))
        {
            JsonUtility.FromJsonOverwrite(stringData, data);
        }

        return data;
    }

    public static T Load<T>(string key) where T : class
    {
        T data = (T)Activator.CreateInstance(typeof(T));

        var stringData = PlayerPrefs.GetString(key, string.Empty);

        if(!string.IsNullOrEmpty(stringData))
        {
            JsonUtility.FromJsonOverwrite(stringData, data);
        }

        return data;
    }

    public static void Overwrite(string key, object data)
    {
        var stringData = PlayerPrefs.GetString(key, string.Empty);

        if (!string.IsNullOrEmpty(stringData))
        {
            JsonUtility.FromJsonOverwrite(stringData, data);
        }
    }

    public static void WriteToDisk()
    {
        PlayerPrefs.Save();
    }

}
