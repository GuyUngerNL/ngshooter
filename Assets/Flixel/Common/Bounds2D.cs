﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Bounds2D
{
	public float x;
	public float y;

	public float width;
	public float height;

	public Vector2 center
	{
		get
		{
			return new Vector2(x, y);
		}
	}

	public Vector2 size
	{
		get
		{
			return new Vector2(width, height);
		}
	}

	public float minX
	{
		get
		{
			return x - width / 2f;
		}
		set
		{
			width = maxX - value;
			x = value + width / 2f;
		}
	}

	public float maxX
	{
		get
		{
			return x + width / 2f;
		}
		set
		{
			width = value - minX;
			x = value - width / 2f;
		}
	}

	public float minY
	{
		get
		{
			return y - height / 2f;
		}
		set
		{
			height = maxY - value;
			y = value + height / 2f;
		}
	}

	public float maxY
	{
		get
		{
			return y + height / 2f;
		}
		set
		{
			height = value - minY;
			y = value - height / 2f;
		}
	}


	public Bounds2D(float x = 0f, float y = 0f, float width = 0f, float height = 0f)
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}


	public bool Overlaps(Bounds2D rect)
	{
		return maxX > rect.minX && minX < rect.maxX && maxY > rect.minY && minY < rect.maxY; 
	}

	public bool OverlapsPoint(Vector2 point)
	{
		return point.x > minX && point.x < maxX && point.y > minY && point.y < maxY;
	}

	public void Extend(float size)
	{
		width += size * 2f;
		height += size * 2f;
	}

	public void Union(Bounds2D bounds)
	{
		if (bounds.minX < minX) minX = bounds.minX;
		if (bounds.minY < minY) minY = bounds.minY;
		if (bounds.maxX > maxX) maxX = bounds.maxX;
		if (bounds.maxY > maxY) maxY = bounds.maxY;
	}

	public static Bounds2D operator *(Bounds2D bounds, Vector2 scale)
	{
		return new Bounds2D(bounds.x * scale.x, bounds.y * scale.y, bounds.width * scale.x, bounds.height * scale.y);
	}

	public static Bounds2D operator /(Bounds2D bounds, Vector2 scale)
	{
		return new Bounds2D(bounds.x / scale.x, bounds.y / scale.y, bounds.width / scale.x, bounds.height / scale.y);
	}

	public static Bounds2D operator +(Bounds2D a, Bounds2D b)
	{
		a.Union(b);
		return a;
	}

	public static explicit operator Rect(Bounds2D bounds)
	{
		return new Rect(bounds.x, bounds.y, bounds.width, bounds.height);
	}

	public static explicit operator Bounds2D(Rect rect)
	{
		return new Bounds2D(rect.x, rect.y, rect.width, rect.height);
	}

	public static Bounds2D BetweenPoints(Vector2 left, Vector2 right, Vector2 top, Vector2 bottom)
	{
		float width = right.x - left.x;
		float height = bottom.y - top.y;
		return new Bounds2D(left.x + width / 2, top.y + height / 2, width, height);
	}

	public override string ToString()
	{
		return string.Format("x: {0}, y: {1}, width: {2}, height: {3}", x, y, width, height);
	}

	static public Bounds2D Lerp(Bounds2D from, Bounds2D to, float t)
	{
		return new Bounds2D(Mathf.Lerp(from.x, to.x, t), Mathf.Lerp(from.y, to.y, t), Mathf.Lerp(from.width, to.width, t), Mathf.Lerp(from.height, to.height, t));
	}

	static public Bounds2D Max(Bounds2D value1, Bounds2D value2)
	{
		return new Bounds2D(Mathf.Min(value1.x, value2.x), Mathf.Min(value1.y, value2.y), Mathf.Max(value1.width, value2.width), Mathf.Max(value1.height, value2.height));
	}
}
