﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using System.Reflection;
using System.ComponentModel;
using System.Linq.Expressions;

public static class ExpressionParser
{
	public static BoolExp Compile(Type type, string query)
	{
		var exp = new BoolExp();
		var ands = query.Split(new string[] { "&", "&&", "and", "AND" }, System.StringSplitOptions.RemoveEmptyEntries);

		foreach (var and in ands)
		{
			exp.Push();

			var ors = and.Split(new string[] { "|", "||", "or", "OR" }, System.StringSplitOptions.RemoveEmptyEntries);

			foreach (var or in ors)
			{
				var cond = CompileCondition(or, type);

				if(cond != null)
				{
					exp.Push(cond);
				}
			}
		}

		return exp;
	}
	
	static BoolCondition CompileCondition(string query, Type type)
	{
		query = query.Trim();

		if (string.IsNullOrEmpty(query)) return null;

		var match = Regex.Match(query, "([^<>=]*)(<=|>=|<|>|==|!=)([^<>=]*)");

		if (match.Groups.Count != 4) throw new System.Exception("Invalid expression");

		var rawObjA = match.Groups[1].ToString().Trim();
		var rawObjB = match.Groups[3].ToString().Trim();
		var rawCondition = match.Groups[2].ToString().Trim();

		return new BoolCondition(type, rawObjA, rawObjB, rawCondition);
	}
	
	public class BoolExp
	{
		List<List<BoolCondition>> conditions = new List<List<BoolCondition>>();

		public void Push()
		{
			conditions.Add(new List<BoolCondition>());
		}

		public void Push(BoolCondition cond)
		{
			conditions[conditions.Count - 1].Add(cond);
		}

		public bool Eval(object obj)
		{
			for (int i = 0; i < conditions.Count; i++)
			{
				var ors = conditions[i];
				if (ors.Count == 0) continue;

				bool orResult = false;

				for (int j = 0; j < ors.Count; j++)
				{
					var c = ors[j];
					if (c.Eval(obj)) orResult = true;
				}

				if (orResult == false) return false;
			}

			return true;
		}
	}

	public enum Condition
	{
		Equals,
		NotEquals,
		LessThen,
		GreaterThen,
		LessThenOrEquals,
		GreaterThenOrEquals,
	}

	public class BoolCondition
	{
		public PropertyInfo a;
		public PropertyInfo b;

		public object aVal;
		public object bVal;

		public Condition c;

		public BoolCondition(Type type, string propA, string propB, string condition)
		{
			a = type.GetProperty(propA, BindingFlags.Instance | BindingFlags.FlattenHierarchy | BindingFlags.Public);
			b = type.GetProperty(propB, BindingFlags.Instance | BindingFlags.FlattenHierarchy | BindingFlags.Public);

			if(a == null && b == null) throw new Exception($"Invalid condition properties ({propA}, {propB}).");

			if (a == null)
			{
				var bType = b.PropertyType;
				if(bType == typeof(string))
				{
					aVal = propA;
				}
				else
				{
					var converter = TypeDescriptor.GetConverter(bType);
					if (converter == null) throw new Exception($"Invalid condition property ({propB}).");
					aVal = converter.ConvertFromInvariantString(propA);
				}
			}

			if (b == null)
			{
				var aType = a.PropertyType;
				if (aType == typeof(string))
				{
					bVal = propB;
				}
				else
				{
					var converter = TypeDescriptor.GetConverter(aType);
					if (converter == null) throw new Exception($"Invalid condition property ({propA}).");
					bVal = converter.ConvertFromInvariantString(propB);
				}
			}

			switch (condition)
			{
				case "==": c = Condition.Equals; break;
				case "!=": c = Condition.NotEquals; break;
				case "<": c = Condition.LessThen; break;
				case ">": c = Condition.GreaterThen; break;
				case "<=": c = Condition.LessThenOrEquals; break;
				case ">=": c = Condition.GreaterThenOrEquals; break;
				default: throw new Exception($"Invalid condition operator ({condition}).");
			}
		}

		public bool Eval(object context)
		{
			var valA = (a == null) ? aVal : a.GetValue(context);
			var valB = (b == null) ? bVal : b.GetValue(context);

			return Compare(a == null ? b.PropertyType : a.PropertyType, valA, valB);
		}

		public bool Compare(Type type, object valA, object valB)
		{
			int comparison = 0;

			if (type == typeof(string))
			{
				comparison = ((string)valA).CompareTo(valB);
			}
			else if (type == typeof(float))
			{
				comparison = ((float)valA).CompareTo(valB);
			}
			else if (type == typeof(int))
			{
				comparison = ((int)valA).CompareTo(valB);
			}
			else if (type == typeof(bool))
			{
				comparison = ((bool)valA).CompareTo(valB);
			}
			else
			{
				if (c == Condition.Equals) return valA == valB;
				if (c == Condition.NotEquals) return valA != valB;
			}

			switch (c)
			{
				case Condition.Equals: return comparison == 0;
				case Condition.NotEquals: return comparison != 0;
				case Condition.GreaterThen: return comparison > 0;
				case Condition.GreaterThenOrEquals: return comparison >= 0;
				case Condition.LessThen: return comparison < 0;
				case Condition.LessThenOrEquals: return comparison <= 0;
			}

			return false;
		}
	}
}
