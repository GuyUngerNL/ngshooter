﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorePrefab : MonoBehaviour
{
    static CorePrefab Instance;

    void Awake ()
    {
        if(Instance)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

}


