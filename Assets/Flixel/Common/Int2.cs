﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct Int2 
{
	public int x;
	public int y;

	public Int2(int x = 0, int y = 0)
	{
		this.x = x;
		this.y = y;
	}

	public Int2(Vector2 vector)
	{
		this.x = (int)vector.x;
		this.y = (int)vector.y;
	}

	public Int2 up
	{
		get { return new Int2(x, y - 1); }
		private set {}
	}
		
	public Int2 down
	{
		get { return new Int2(x, y + 1); }
		private set {}
	}
		
	public Int2 left
	{
		get { return new Int2(x - 1, y); }
		private set {}
	}
		
	public Int2 right
	{
		get { return new Int2(x + 1, y); }
		private set {}
	}

	public override bool Equals(System.Object obj)
	{
		// If parameter is null return false.
		if (obj == null || !(obj is Int2))
		{
			return false;
		}

		var p = (Int2)obj;

		// Return true if the fields match:
		return (x == p.x) && (y == p.y);
	}

	public bool Equals(Int2 p)
	{
		// If parameter is null return false:
		if ((object)p == null)
		{
			return false;
		}

		// Return true if the fields match:
		return (x == p.x) && (y == p.y);
	}

	public override int GetHashCode()
	{
		return x ^ y;
	}

	public static bool operator ==(Int2 a, Int2 b)
	{
		// If both are null, or both are same instance, return true.
		if (System.Object.ReferenceEquals(a, b))
		{
			return true;
		}

		// If one is null, but not both, return false.
		if (((object)a == null) || ((object)b == null))
		{
			return false;
		}

		// Return true if the fields match:
		return a.x == b.x && a.y == b.y;
	}

	public static bool operator !=(Int2 a, Int2 b)
	{
		return !(a == b);
	}


	public static Int2 operator +(Int2 a, Int2 b)
	{
		return new Int2(a.x + b.x, a.y + b.y);
	}

	public static Int2 operator -(Int2 a, Int2 b)
	{
		return new Int2(a.x - b.x, a.y - b.y);
	}

	public static Int2 operator *(Int2 a, int b)
	{
		return new Int2(a.x * b, a.y * b);
	}

	public static Int2 operator /(Int2 a, int b)
	{
		return new Int2(a.x / b, a.y / b);
	}

	public override string ToString()
	{
		return string.Format("X: {0}, Y: {1}", x,y);
	}

	public bool IsNextTo(Int2 pos)
	{
		return (Mathf.Abs(pos.x - x) + Mathf.Abs(pos.y - y)) == 1;
	}

	public Vector2 ToVector2()
	{
		return new Vector2(x, y);
	}
}
	