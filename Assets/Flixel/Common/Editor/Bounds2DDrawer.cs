﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(Bounds2D))]
public class Bounds2DDrawer : PropertyDrawer
{
    public override void OnGUI(Rect pos, SerializedProperty property, GUIContent label)
    {
        pos.height = EditorGUIUtility.singleLineHeight;

        property.Next(true);

        EditorGUI.MultiPropertyField(
            pos, 
            new GUIContent[]
            {
                new GUIContent("X"),
                new GUIContent("Y"),
                new GUIContent("W"),
                new GUIContent("H")
            },
            property,
            label
        );
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return Screen.width < 333 ? (16f + 18f) : 16f;
    }

}
