﻿using System;
using System.Linq;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.Text.RegularExpressions;

[CustomPropertyDrawer(typeof(SceneReference))]
public class SceneReferenceDrawer : PropertyDrawer
{
	static string[] sceneNames;
	static GUIContent[] scenes;

	class NestedString
	{
		public int level;
		public string path;
		public string name;
		public string prefix;
		public Dictionary<string, NestedString> children;

		public NestedString(string name, string prefix, string path, int level)
		{
			this.level = level;
			this.name = name;
			this.prefix = prefix;
			this.path = path;
			this.children = new Dictionary<string, NestedString>();
		}

		public void Merge()
		{
			if(children.Count == 1)
			{
				foreach (var pair in children)
				{
					name = name + pair.Key;
					children = pair.Value.children;
					path = pair.Value.path;

					foreach (var subPair in children)
					{
						subPair.Value.level--;
						subPair.Value.prefix = prefix + "/" + name;
					}

					Merge();
					break;
				}
			}
			else
			{
				foreach (var pair in children)
				{
					pair.Value.Merge();
				}
			}
		}

		public List<KeyValuePair<string, NestedString>> Split()
		{
			var newList = new List<KeyValuePair<string, NestedString>>();

			if (level > 0 && !isGrandfather && children.Count > 0 && children.Count < 5)
			{
				foreach (var pair in children)
				{
					newList.Add(new KeyValuePair<string, NestedString>(name + pair.Key, new NestedString(name + pair.Value.name, prefix, pair.Value.path, level)));
				}

				return newList;
			}
			else
			{
				var newChildren = new Dictionary<string, NestedString>();

				foreach (var pair in children)
				{
					var newChild = pair.Value.Split();

					foreach(var pair2 in newChild)
					{
						newChildren.Add(pair2.Key, pair2.Value);
					}
				}

				children = newChildren;
			}

			return new List<KeyValuePair<string, NestedString>>() { new KeyValuePair<string, NestedString>(name, this) };
		}

		public bool isGrandfather
		{
			get
			{
				foreach (var pair in children)
				{
					if (pair.Value.isGrandfather || pair.Value.children.Count > 0) return true;
				}

				return false;
			}
		}

		public bool hasMultipleOffsprings
		{
			get
			{
				if(children.Count == 0)
				{
					return false;
				}
				else if(children.Count > 1)
				{
					return true;
				}
				else
				{
					foreach(var pair in children)
					{
						return pair.Value.hasMultipleOffsprings;
					}
				}

				return false;
			}
		}

		public string displayName
		{
			get
			{
				if(!hasMultipleOffsprings)
				{
					foreach(var pair in children)
					{
						return name + pair.Value.displayName;
					}
				}

				return name;
			}
		}

		public string prefixedName
		{
			get
			{
				return (prefix + "/" + displayName).Substring(1);
			}
		}
	}

	static Dictionary<string, NestedString> sceneMenu = new Dictionary<string, NestedString>();

	public override void OnGUI(Rect pos, SerializedProperty property, GUIContent label)
	{
		if (scenes == null)
		{
			//CreateSceneList();
		}

		label.tooltip = "List of scenes that are added and enabled in editor build settings.";

		//var selectedIndex = Array.IndexOf<string>(sceneNames, spSceneName.stringValue);

		//var selected = scenes.Length > selectedIndex ? Mathf.Clamp(selectedIndex, 0, scenes.Length) : 0;

		//pos = EditorGUI.PrefixLabel(pos, label);
		//if(GUI.Button(pos, sceneNames[selected], EditorStyles.popup))
		//{
		//	var menu = new GenericMenu();

		//	AddToMenu(menu, sceneMenu, sceneNames[selected], spSceneName);

		//	menu.ShowAsContext();
		//}

		//property.serializedObject.Update();

		var spSceneName = property.FindPropertyRelative("_sceneName");

		var scenePath = string.Empty;

		foreach (var s in EditorBuildSettings.scenes)
		{
			if (!s.enabled) continue;
			if (string.IsNullOrEmpty(spSceneName.stringValue)) continue;

			var fileName = "/" + spSceneName.stringValue + ".unity";

			if (s.path.Contains(fileName))
			{
				scenePath = s.path;
				break;
			}
		}

		var oldScene = string.IsNullOrEmpty(scenePath) ? null : AssetDatabase.LoadAssetAtPath<SceneAsset>(scenePath);

		EditorGUI.BeginChangeCheck();
		var newScene = EditorGUI.ObjectField(pos, label, oldScene, typeof(SceneAsset), false) as SceneAsset;

		if (EditorGUI.EndChangeCheck())
		{
			var newPath = AssetDatabase.GetAssetPath(newScene);
			var scenePathProperty = spSceneName;
			scenePathProperty.stringValue = System.IO.Path.GetFileNameWithoutExtension(newPath);
		}

		//property.serializedObject.ApplyModifiedProperties();
	}

	static void AddToMenu(GenericMenu menu, Dictionary<string, NestedString> items, string selected, SerializedProperty property)
	{
		var sorted = items.Keys.ToList();
		sorted.Sort((x, y) => x.CompareTo(y));

		foreach (var item in sorted)
		{
			var value = items[item];

			if(value.hasMultipleOffsprings)
			{
				AddToMenu(menu, value.children, selected, property);
			}
			else
			{
				var name = value.prefixedName;

				menu.AddItem(new GUIContent(name), selected == name, (nameObject) => { property.serializedObject.Update(); property.stringValue = (string)nameObject; property.serializedObject.ApplyModifiedProperties(); }, value.path);
			}
		}
	}

	static void CreateSceneList()
	{
		List<string> sceneNamesList = new List<string>();
		List<GUIContent> sceneList = new List<GUIContent>();

		foreach (var s in EditorBuildSettings.scenes)
		{
			if (!s.enabled) continue;

			var name = System.IO.Path.GetFileNameWithoutExtension(s.path);

			sceneNamesList.Add(name);
			sceneList.Add(new GUIContent(name));

			var scenePath = Regex.Split(name, @"(?<!^)(?=[A-Z][a-z])");

			var curMenuLevel = sceneMenu;
			var curPrefix = string.Empty;

			var level = 0;

			foreach (var p in scenePath)
			{
				if (!curMenuLevel.ContainsKey(p))
				{
					curMenuLevel.Add(p, new NestedString(p, curPrefix, name, level));
				}

				curMenuLevel = curMenuLevel[p].children;
				curPrefix = curPrefix + "/" + p;
				level++;
			}
		}

		foreach (var p in sceneMenu)
		{
			p.Value.Merge();
		}

		var newSceneMenu = new Dictionary<string, NestedString>();

		foreach (var p in sceneMenu)
		{
			var newList = p.Value.Split();

			foreach(var p2 in newList)
			{
				newSceneMenu.Add(p2.Key, p2.Value);
			}
		}

		sceneMenu = newSceneMenu;

		sceneNames = sceneNamesList.ToArray();
		scenes = sceneList.ToArray();
	}

}
