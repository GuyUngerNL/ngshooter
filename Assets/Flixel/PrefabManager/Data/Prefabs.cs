using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// This is auto-generated class with the list of assets in project.
/// DO NOT edit this class.
/// </summary>
public static class Prefabs
{
  static ObjectPool __FlxSoundPlayerPool;
  public static ObjectPool FlxSoundPlayer { get { return __FlxSoundPlayerPool ? __FlxSoundPlayerPool : @get(out __FlxSoundPlayerPool, "audio/FlxSoundPlayer"); } }

  static ObjectPool __LightPool;
  public static ObjectPool Light { get { return __LightPool ? __LightPool : @get(out __LightPool, "managers/Light"); } }

  static ObjectPool __UIEventSystemPool;
  public static ObjectPool UIEventSystem { get { return __UIEventSystemPool ? __UIEventSystemPool : @get(out __UIEventSystemPool, "managers/UIEventSystem"); } }


  static ObjectPool @get(out ObjectPool pool, string resourcePath) { return pool = Pool.GetOrCreatePool(Resources.Load<GameObject>(resourcePath)); }
}

public static class Managers
{
	static FlxAudioManager __AudioPlayerInstance;
	public static FlxAudioManager AudioPlayer { get { return __AudioPlayerInstance ? __AudioPlayerInstance : @get<FlxAudioManager>(out __AudioPlayerInstance, "managers/AudioPlayer"); } }

	static DevTools.DevConsoleManager __DevConsoleInstance;
	public static DevTools.DevConsoleManager DevConsole { get { return __DevConsoleInstance ? __DevConsoleInstance : @get<DevTools.DevConsoleManager>(out __DevConsoleInstance, "managers/DevConsole"); } }

	static CameraManager __CameraInstance;
	public static CameraManager Camera { get { return __CameraInstance ? __CameraInstance : @get<CameraManager>(out __CameraInstance, "managers/Camera"); } }

	static FlxInputManager __InputInstance;
	public static FlxInputManager Input { get { return __InputInstance ? __InputInstance : @get<FlxInputManager>(out __InputInstance, "managers/Input"); } }

	static TimeManager __TimeManagerInstance;
	public static TimeManager TimeManager { get { return __TimeManagerInstance ? __TimeManagerInstance : @get<TimeManager>(out __TimeManagerInstance, "managers/TimeManager"); } }

	static UI __UIInstance;
	public static UI UI { get { return __UIInstance ? __UIInstance : @get<UI>(out __UIInstance, "managers/UI"); } }

	static T @get<T>(out T instance, string resourcePath) where T : FlxManager { instance = GameObject.FindObjectOfType<T>(); if (!instance) { instance = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>(resourcePath)).GetComponent<T>(); if (instance.persistAcrossScenes) { GameObject.DontDestroyOnLoad(instance.gameObject); } } return instance; }
	public static Dictionary<System.Type, object> Instances = new Dictionary<System.Type, object>();

}