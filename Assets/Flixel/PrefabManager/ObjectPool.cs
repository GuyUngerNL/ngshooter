﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : IEnumerable<FlxBasic>, IEnumerable
{
	FlxBasic _prefab = null;

	public FlxBasic prefab {
		get {
			return _prefab;
		}
	}

	int _countExisting;
	int _countDisabled;

	public UList<FlxBasic> existing;
	public UList<FlxBasic> disabled;

	public ObjectPool(GameObject prefab) : this(prefab.GetComponent<FlxBasic>()) {}

	public ObjectPool(FlxBasic prefab)
	{
		_prefab = prefab;

		existing = new UList<FlxBasic>(3);
		disabled = new UList<FlxBasic>(3);
	}

	public T Recycle<T>() where T : Component
	{
		var newObject = Recycle();
		return (newObject != null) ? newObject.GetComponent<T>() : default(T);
	}

	public FlxBasic Recycle()
	{
		if(_prefab == null)
		{
			return null;
		}

		FlxBasic newObject = null;

		if (_countDisabled > 0)
		{
			while(newObject == null && _countDisabled > 0)
			{
				newObject = disabled[0];

				if(newObject == null)
				{
					_countDisabled--;

					disabled.RemoveAt(0, out newObject);

					if(newObject != null)
					{
						newObject.poolIndex = 0;
					}
				}
			}
		}
		else
		{
			newObject = GameObject.Instantiate(_prefab);
		}

		if(newObject != null)
		{
			newObject.pool = this;
			newObject.exists = true;
		}

		return newObject;
	}

	public void Enable(FlxBasic item)
	{
		FlxBasic replacement = null;

		if (item.poolIndex >= 0)
		{
			disabled.RemoveAt(item.poolIndex, out replacement);
			_countDisabled = disabled.Count;
			if (replacement != null) replacement.poolIndex = item.poolIndex;
		}

		existing.Add(item, out item.poolIndex);
		_countExisting = existing.Count;
	}

	public void Disable(FlxBasic item)
	{
		FlxBasic replacement = null;

		if (item.poolIndex >= 0)
		{
			existing.RemoveAt(item.poolIndex, out replacement);
			_countExisting = existing.Count;
			if (replacement != null) replacement.poolIndex = item.poolIndex;
		}

		disabled.Add(item, out item.poolIndex);
		_countDisabled = disabled.Count;
	}

	public void Remove(FlxBasic item)
	{
		FlxBasic replacement = null;

		if (item.exists)
		{
			existing.RemoveAt(item.poolIndex, out replacement);
			_countExisting = existing.Count;
		}
		else
		{
			disabled.RemoveAt(item.poolIndex, out replacement);
			_countDisabled = disabled.Count;
		}

		if (replacement != null) replacement.poolIndex = item.poolIndex;

		item.pool = null;
		item.poolIndex = -1;
	}

	public FlxBasic this[int i]
	{
		get
		{
			return i >= _countExisting ? disabled[i - _countExisting] : existing[i];
		}
	}

	public int Length
	{
		get
		{
			return _countExisting + _countDisabled;
		}
	}

	public int Count
	{
		get
		{
			return _countExisting + _countDisabled;
		}
	}

	public int CountExistings
	{
		get
		{
			return _countExisting;
		}
	}

	public int CountDisabled
	{
		get
		{
			return _countDisabled;
		}
	}

	IEnumerator IEnumerable.GetEnumerator()
	{
		return GetEnumerator();
	}

	public IEnumerator<FlxBasic> GetEnumerator()
	{
		foreach (var item in existing) { yield return item; }
		foreach (var item in disabled) { yield return item; }
	}

	public static implicit operator bool(ObjectPool pool)
	{
		return pool != null;
	}

	#region GetNearest 

	public U GetNearest<U>(float x, float y) where U : FlxComponent
	{
		FlxBasic result = null;
		float closestDistance = float.MaxValue;
		for (int i = 0; i < _countExisting; i++)
		{
			FlxBasic other = existing[i];

			float distance = (other.actor.x - x) * (other.actor.x - x) + (other.actor.y - y) * (other.actor.y - y);
			if (distance < closestDistance)
			{
				result = other;
				closestDistance = distance;
			}
		}
		return result ? result.GetComponent<U>() : default(U);
	}

	public U GetNearest<U>(FlxBasic actor) where U : FlxComponent
	{
		return GetNearest<U>(actor.x, actor.y);
	}

	public U GetNearest<U>(FlxActor actor) where U : FlxComponent
	{
		return GetNearest<U>(actor.x, actor.y);
	}

	public U GetNearest<U>(FlxComponent component) where U : FlxComponent
	{
		return GetNearest<U>(component.actor.x, component.actor.y);
	}

	public U GetNearest<U>(Vector2 vector2) where U : FlxComponent
	{
		return GetNearest<U>(vector2.x, vector2.y);
	}

	#endregion

	#region GetNearest with filter

	public U GetNearest<U>(float x, float y, System.Func<U, bool> predicate) where U : FlxComponent
	{
		FlxBasic result = null;
		float closestDistance = float.MaxValue;
		U component = null;

		for (int i = 0; i < _countExisting; i++)
		{
			FlxBasic other = existing[i];

			component = result.GetComponent<U>();

			if (!component) continue;

			if (!predicate(component)) continue;

			float distance = (other.actor.x - x) * (other.actor.x - x) + (other.actor.y - y) * (other.actor.y - y);
			if (distance < closestDistance)
			{
				result = other;
				closestDistance = distance;
			}
		}

		return component;
	}

	public U GetNearest<U>(FlxBasic actor, System.Func<U, bool> predicate) where U : FlxComponent
	{
		return GetNearest<U>(actor.x, actor.y, predicate);
	}

	public U GetNearest<U>(FlxComponent component, System.Func<U, bool> predicate) where U : FlxComponent
	{
		return GetNearest<U>(component.actor.x, component.actor.y, predicate);
	}

	public U GetNearest<U>(Vector2 vector2, System.Func<U, bool> predicate) where U : FlxComponent
	{
		return GetNearest<U>(vector2.x, vector2.y, predicate);
	}

	#endregion
}
