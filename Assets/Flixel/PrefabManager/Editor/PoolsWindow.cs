﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class PoolsWindow : EditorWindow
{
	Vector2 scrollPosition;
	// Add menu named "My Window" to the Window menu
	//[MenuItem("Window/Pools")]
	static void Init()
	{
		PoolsWindow window = (PoolsWindow)EditorWindow.GetWindow(typeof(PoolsWindow));
		window.titleContent = new GUIContent("Pools");
		window.autoRepaintOnSceneChange = true;
		window.Show();
	}

	void OnGUI()
	{
        EditorGUILayout.Space();

        if (!Application.isPlaying)
		{
			EditorGUILayout.HelpBox ("Pool debug only available during playtime.", MessageType.Info);
			return;
		}

        EditorGUI.indentLevel++;

        EditorGUILayout.LabelField("Pool name: (Total/Active) ", EditorStyles.boldLabel);
        EditorGUILayout.Space();
        EditorGUILayout.Space();

        scrollPosition = EditorGUILayout.BeginScrollView (scrollPosition);

		var pools = Pool.pools;

		foreach(var p in pools)
		{

            EditorGUILayout.LabelField(
                p.Key.name,
                string.Format("{0} / {1}", p.Value.Length, (p.Value.Length - p.Value.CountDisabled))
                );
		}

		EditorGUILayout.EndScrollView ();

        EditorGUI.indentLevel--;
    }

    public void Update()
	{
		Repaint();
	}
}
