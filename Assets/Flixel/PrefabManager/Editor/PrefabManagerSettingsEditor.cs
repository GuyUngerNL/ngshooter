using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System;
using System.Linq;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Noopol;
using System.Text;


[CustomEditor(typeof(PrefabManagerSettings))]
public class PrefabManagerSettingsEditor : Editor
{
    SerializedProperty spFolders;

    ReorderableList folderList;

    void OnEnable()
    {
        spFolders = serializedObject.FindProperty("folders");

        folderList = new ReorderableList(serializedObject, spFolders, true, true, true, true);

        folderList.drawHeaderCallback = (Rect rect) => {
            EditorGUI.LabelField(rect, "Folder List");
        };

        folderList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
            var element = spFolders.GetArrayElementAtIndex(index);
            rect.y += 2f;
            rect.width -= 70f;
            rect.height = EditorGUIUtility.singleLineHeight;

            EditorGUI.BeginDisabledGroup (true);
            EditorGUI.TextField (rect, element.stringValue);
            EditorGUI.EndDisabledGroup ();

            rect.x += rect.width + 10f;
            rect.width = 60f;

            var dirSeparator = System.IO.Path.DirectorySeparatorChar.ToString ();

            if(GUI.Button (rect, "Browse")) {
                var sourceFolder = Application.dataPath + dirSeparator + element.stringValue.Replace ("/", dirSeparator);
                var targetFolder = EditorUtility.OpenFolderPanel(name, sourceFolder, "Folder");

                if(string.IsNullOrEmpty (targetFolder)) {
                    
                } else if(!targetFolder.StartsWith (Application.dataPath)) {
                    EditorUtility.DisplayDialog ("Invalid Folder", "Please select folder inside this project.", "Ok");
                } else if(!targetFolder.Contains ("Resources")) {
                    EditorUtility.DisplayDialog ("Invalid Folder", "Please select a 'Resources' folder.", "Ok");
                } else {
                    targetFolder = targetFolder.Substring(Application.dataPath.Length + 1);
                    targetFolder = targetFolder.Replace (dirSeparator, "/");
                    element.stringValue = targetFolder;
                }
            }
        };
    }

    public override void OnInspectorGUI()
    {

        PrefabManagerSettings instance = target as PrefabManagerSettings;

        serializedObject.Update();
        EditorGUILayout.Space();

        folderList.DoLayoutList ();

        EditorGUILayout.Space ();

        serializedObject.ApplyModifiedProperties();

        EditorGUI.BeginDisabledGroup (EditorApplication.isCompiling);
        if(GUILayout.Button ("Generate Class")) {
            GenerateClass (instance);
            AssetDatabase.Refresh();
        }
        EditorGUI.EndDisabledGroup ();

        if(EditorApplication.isCompiling) {
            EditorGUILayout.HelpBox ("Cannot generate class while scripts are compiling.", MessageType.Warning);
        }
    }

    [MenuItem("Gungrounds/Prefab Settings")]
    public static void EditSettings()
    {
        var assets = AssetDatabase.FindAssets("t:PrefabManagerSettings");

        foreach (var a in assets)
        {
            var asset = AssetDatabase.LoadAssetAtPath<PrefabManagerSettings>(AssetDatabase.GUIDToAssetPath(a));

            if (asset)
            {
                Selection.activeObject = asset;
            }
        }
    }

    [MenuItem("Gungrounds/Generate Prefabs List Class")]
    public static void GenerateDefaultPrefabs()
    {
		EditorUtility.DisplayProgressBar("Generating Prefabs", "Collecting all prefabs", 0.1f);

		var assets = AssetDatabase.FindAssets ("t:PrefabManagerSettings");

        foreach(var a in assets)
        {
            var asset = AssetDatabase.LoadAssetAtPath <PrefabManagerSettings> (AssetDatabase.GUIDToAssetPath (a));

            if(asset) {
                GenerateClass (asset);
            }
        }

		AssetDatabase.Refresh();

		EditorUtility.ClearProgressBar();
    }

    public static void GenerateClass(PrefabManagerSettings settings)
    {
        List<string> assets = new List<string> ();
        List<string> assetLists = new List<string> ();

		foreach (var folder in settings.folders) {
            var folderPath = Application.dataPath + System.IO.Path.DirectorySeparatorChar + folder;

            if(Directory.Exists (folderPath)) {
                AddAssetsFromFolder (settings, assets, assetLists, folderPath);
            }
        }

        string classContents = GetClassContents (settings, assets, assetLists);

        string settingsPath = AssetDatabase.GetAssetPath (settings);
        string settingsDir = System.IO.Path.GetDirectoryName (settingsPath);
        string classPath = settingsDir + System.IO.Path.DirectorySeparatorChar + GetVariableName(settings.name) + ".cs";

        System.IO.File.WriteAllText(classPath, classContents);
        AssetImporter.GetAtPath(classPath);
    }

    static void AddAssetsFromFolder(PrefabManagerSettings settings, List<string> assets, List<string> assetLists, string directory)
    {
        foreach (string f in Directory.GetFiles(directory))
        {
            var resourcesIndex = f.IndexOf ("Resources");

            if(resourcesIndex < 0) {
                continue;
            }

            var filePath = f.Substring (resourcesIndex + 10);
            if(f.EndsWith (".prefab")) {
                filePath = filePath.Substring (0, filePath.Length - 7);

                if(!assets.Contains (filePath)) {
                    assets.Add (filePath);
                }
            }

            if(f.EndsWith (".asset")) {
                if(!assets.Contains (filePath)) {
                    //assets.Add (filePath);
                }
            }
        }

        foreach (string d in Directory.GetDirectories(directory))
        {
            AddAssetsFromFolder (settings, assets, assetLists, d);
        }
    }

    static string GetClassContents(PrefabManagerSettings settings, List<string> assets, List<string> assetLists)
    {
        StringBuilder text = new StringBuilder();

		text.AppendLine("using UnityEngine;");
		text.AppendLine("using System.Collections.Generic;");
		text.AppendLine ();
        text.AppendLine ("/// <summary>");
        text.AppendLine ("/// This is auto-generated class with the list of assets in project.");
        text.AppendLine ("/// DO NOT edit this class.");
        text.AppendLine ("/// </summary>");

        text.AppendLine("public static class Prefabs");
        text.AppendLine("{");

        foreach (var a in assets)
        {
            var resourcePath = a.Replace(System.IO.Path.DirectorySeparatorChar.ToString(), "/");

            var asset = Resources.Load<GameObject>(a);
            if (!(asset))
            {
                continue;
            }

            if ((asset).GetComponent<FlxManager>())
            {
                continue;
            }

            var varName = GetVariableName(asset.name);

            text.AppendLine(string.Format("  static ObjectPool __{0}Pool;", varName));
            text.AppendLine(string.Format("  public static ObjectPool {0} {{ get {{ return __{0}Pool ? __{0}Pool : @get(out __{0}Pool, \"{1}\"); }} }}", varName, resourcePath));
            //text.AppendLine(string.Format("  public static ObjectPool {0} {  }", varName, a));
            text.AppendLine();
        }

        text.AppendLine();
        text.AppendLine("  static ObjectPool @get(out ObjectPool pool, string resourcePath) { return pool = Pool.GetOrCreatePool(Resources.Load<GameObject>(resourcePath)); }");
        text.AppendLine("}");
        text.AppendLine();




        text.AppendLine("public static class Managers");
        text.AppendLine("{");

        foreach (var a in assets)
        {
            var resourcePath = a.Replace(System.IO.Path.DirectorySeparatorChar.ToString(), "/");

            var asset = Resources.Load<GameObject>(a);
            if (!(asset))
            {
                continue;
            }

            var managerComponent = asset.GetComponent<FlxManager>();

            if(!managerComponent)
            {
                continue;
            }

            var varName = GetVariableName(asset.name);

            text.AppendLine(string.Format("  static {1} __{0}Instance;", varName, managerComponent.GetType().FullName));
            text.AppendLine(string.Format("  public static {2} {0} {{ get {{ return __{0}Instance ? __{0}Instance : @get<{2}>(out __{0}Instance, \"{1}\"); }} }}", varName, resourcePath, managerComponent.GetType().FullName));
            text.AppendLine();
        }

        text.AppendLine();
		text.AppendLine("  static T @get<T>(out T instance, string resourcePath) where T : FlxManager { instance = GameObject.FindObjectOfType<T>(); if(!instance) { instance = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>(resourcePath)).GetComponent<T>(); if(instance.persistAcrossScenes) { GameObject.DontDestroyOnLoad(instance.gameObject); } } return instance; }");
		text.AppendLine("  public static Dictionary<System.Type, object> Instances = new Dictionary<System.Type, object>();");
		text.AppendLine("}");
        text.AppendLine();

        return text.ToString ();
    }

    static string GetVariableName(string name)
    {
        var index = name.LastIndexOf (System.IO.Path.DirectorySeparatorChar);
        name = index < 0 ? name : name.Substring (index);
        char[] arr = name.Where(c => (char.IsLetterOrDigit(c))).ToArray();
        return new string(arr);
    }

}
