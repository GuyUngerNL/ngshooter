﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public sealed class Pool : Singleton<Pool>
{
	static ObjectPool _pool;
	static Dictionary<GameObject, ObjectPool> _pools = new Dictionary<GameObject, ObjectPool>();

	public static Dictionary<GameObject, ObjectPool> pools
	{
		get 
		{
			return _pools;
		}
	}

	public static T Recycle<T>(FlxBasic flx) where T : Component
	{
		return Recycle<T>(flx.gameObject);
	}

	public static FlxBasic Recycle(FlxBasic flx)
	{
		return Recycle(flx.gameObject);
	}

	public static T Recycle<T>(GameObject gameObject) where T : Component
	{
		_pool = GetOrCreatePool(gameObject);

		return (_pool == null) ? null : _pool.Recycle<T>();
	}

	public static FlxBasic Recycle(GameObject gameObject)
	{
		_pool = GetOrCreatePool(gameObject);

		return (_pool == null) ? null : _pool.Recycle();
	}

	public static ObjectPool GetPool(FlxBasic flx)
	{
		return GetPool(flx.gameObject);
	}

	public static ObjectPool GetPool(GameObject gameObject)
	{
		if (gameObject == null)
		{
			return null;
		}

		FlxBasic flx = gameObject.GetComponent<FlxBasic>();

		if (flx == null)
		{
			return null;
		}

		_pool = null;

		if (_pools.TryGetValue(gameObject, out _pool))
		{
			return _pool;
		}

		return null;
	}

	public static ObjectPool GetOrCreatePool(FlxBasic flx)
	{
		return GetOrCreatePool(flx.gameObject);
	}

	public static ObjectPool GetOrCreatePool(GameObject gameObject)
	{
		if (gameObject == null)
		{
			return null;
		}

		FlxBasic flx = gameObject.GetComponent<FlxBasic>();

		if (flx == null)
		{
			Debug.LogError(string.Format("FlxActor component not found on prefab ({0}). Pool will not be created.", gameObject.name));
			return null;
		}

		_pool = null;

		if (!_pools.TryGetValue(gameObject, out _pool))
		{
			_pool = new ObjectPool(gameObject);
			_pools.Add(gameObject, _pool);
		}

		return _pool;
	}
	
}
