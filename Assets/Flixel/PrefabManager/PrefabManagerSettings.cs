﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Noopol;

public class PrefabManagerSettings : ScriptableObject
{
	public List<string> folders;
}
