﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenGlow : MonoBehaviour
{
    Material material;
    RenderTexture texture;
    Camera targetCamera;
    GameObject targetCameraObject;

    [Range(0, 5)]
    public int downscale;
    public LayerMask layerMask;


    private void Awake()
    {
        texture = new RenderTexture(
            Mathf.CeilToInt(Screen.width / Mathf.Pow(2, downscale)),
            Mathf.CeilToInt(Screen.height / Mathf.Pow(2, downscale)),
            16
        );

        targetCameraObject = new GameObject("Glow Camera Effect");
        targetCameraObject.transform.SetParent(Camera.main.transform);

        targetCamera = targetCameraObject.AddComponent<Camera>();
        targetCamera.CopyFrom(Camera.main);
        targetCamera.cullingMask = layerMask.value;
        targetCamera.clearFlags = CameraClearFlags.Color;
        targetCamera.backgroundColor = Color.black;

        targetCamera.targetTexture = texture;

        material = new Material(Shader.Find("Hidden/ScreenGlow"));
        material.SetTexture("_GlowTex", texture);
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if(material)
        {
            Graphics.Blit(source, destination, material);
        }
    }
}
