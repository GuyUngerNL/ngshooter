﻿using Malee;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameVersion : ScriptableObject
{
#if UNITY_EDITOR
	[UnityEditor.MenuItem("Gungrounds/Versioning")]
	public static void SelectGameVersions()
	{
		UnityEditor.Selection.activeObject = UnityEditor.AssetDatabase.LoadAssetAtPath<GameVersion>("Assets/Temp/Versioning/Versions.asset");
	}
#endif

	[Serializable]
	public struct Version
	{
		public string version;
		public string title;
		public string date;

		[Reorderable]
		public Patchnotes changelog;
	}

	[Serializable]
	public struct Patchnote { [Multiline] public string note; }

	[Serializable]
	public class Patchnotes : Malee.ReorderableArray<Patchnote> { }

	[Serializable]
	public class Versions : Malee.ReorderableArray<Version> { }

	[Reorderable]
	public Versions versions;

	public Version latest => versions[versions.Length - 1];
}
