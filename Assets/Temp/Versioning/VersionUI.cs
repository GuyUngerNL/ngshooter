﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class VersionUI : MonoBehaviour
{
	public TextMeshProUGUI text;

	void Start()
	{
		var latest = GameSettings.main.version.latest;
		text.text = $"Tormental - {latest.title} \n [{latest.date} version {latest.version}]";
	}
}
