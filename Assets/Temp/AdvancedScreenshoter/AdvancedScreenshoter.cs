﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdvancedScreenshoter : MonoBehaviour
{
	public new Camera camera;
	public int width;
	public int height;

	[HideInInspector]
	public string path;

	private void Reset()
	{
		camera = GetComponent<Camera>();
	}
}
