using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

[CustomEditor(typeof(AdvancedScreenshoter))]
public class AdvancedScreenshoterEditor : Editor
{

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		DestinationFolder();

		var instance = target as AdvancedScreenshoter;
		var camera = instance.camera;

		var disabled = instance.camera == null || string.IsNullOrWhiteSpace(instance.path) || instance.width <= 0 || instance.height <= 0;

		EditorGUI.BeginDisabledGroup(disabled);

		if (GUILayout.Button("Shoot"))
		{
			var rt = new RenderTexture(instance.width, instance.height, 24);
			var tex = new Texture2D(instance.width, instance.height, TextureFormat.ARGB32, false);

			var alphaPixels = tex.GetPixels();

			for (int i = 0; i < alphaPixels.Length; i++)
			{
				alphaPixels[i] = new Color(0, 0, 0, 0);
			}

			tex.SetPixels(alphaPixels);

			camera.targetTexture = rt;
			camera.Render();

			RenderTexture.active = rt;
			tex.ReadPixels(new Rect(0, 0, instance.width, instance.height), 0, 0);
			RenderTexture.active = null;
			camera.targetTexture = null;

			File.WriteAllBytes(instance.path, tex.EncodeToPNG());
		}

		EditorGUI.EndDisabledGroup();
	}

	public void DestinationFolder()
	{
		var instance = target as AdvancedScreenshoter;

		string path = instance.path;
		string directory = string.IsNullOrEmpty(path) ? Directory.GetParent(Application.dataPath).FullName : System.IO.Path.GetDirectoryName(path);
		string name = "Output filename";

		EditorGUILayout.BeginHorizontal();
		EditorGUI.BeginDisabledGroup(true);
		EditorGUILayout.TextField(name, path);
		EditorGUI.EndDisabledGroup();
		if (GUILayout.Button("Browse", GUILayout.MaxWidth(60f)))
		{
			EditorGUI.BeginChangeCheck();
			instance.path = EditorUtility.SaveFilePanel(name, directory, "", "png");
			if (EditorGUI.EndChangeCheck())
			{
				Repaint();
				EditorUtility.SetDirty(instance);
			}
		}
		if (GUILayout.Button("Clear", GUILayout.MaxWidth(60f)))
		{
			instance.path = string.Empty;
		}
		EditorGUILayout.EndHorizontal();
	}
}
