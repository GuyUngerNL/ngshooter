#pragma shader_feature FLX_EMISSION
#pragma shader_feature FLX_SPECULAR
#pragma shader_feature FLX_RIM

struct Input
{
	float2 uv_MainTex;
	float3 viewDir;
	INTERNAL_DATA
};

struct v2f {
	float4 pos : SV_POSITION;
};


sampler2D _MainTex;

#if defined(FLX_SPECULAR)
sampler2D _SpecularTex;
sampler2D _GlossTex;
#endif

float _Specular;
float _Gloss;

sampler2D _RampTex;

#if defined(FLX_EMISSION)
sampler2D _EmissionTex;
#endif

#if defined(FLX_RIM)
sampler2D _RimTex;
#endif

float4 _Color;
fixed4 _EmissionColor;

float4 _RimColor;
float _RimPower;
float _RimCutoff;
float _RimCamera;

float4 _Pivot;
float _PerspectiveCorrection;
float _Flatten;

uniform float _Outline;
uniform float4 _OutlineColor;



inline fixed4 LightingFlxShade(SurfaceOutput s, fixed3 lightDir, fixed3 viewDir, fixed atten)
{
	fixed3 h = normalize(lightDir + viewDir);

	fixed NdotL = dot(s.Normal, lightDir) * 0.5 + 0.5;
	fixed3 ramp = tex2D(_RampTex, float2(NdotL * atten, 0)).rgb;

	float nh = max(0, dot(s.Normal, h));
	float spec = pow(nh, s.Gloss * 128) * s.Specular;

	fixed4 c;
	c.rgb = ((s.Albedo * ramp * _LightColor0.rgb + _LightColor0.rgb * spec) * (atten * 2));
	c.a = s.Alpha;
	return c;
}



void FlxVertStandard(inout appdata_full IN)
{
	float4 v = mul(unity_ObjectToWorld, IN.vertex);

	v -= _Pivot;
	v.y += _PerspectiveCorrection * v.z * v.y * .7;
	v.z -= _PerspectiveCorrection * v.y * 3;
	v.y *= _Flatten;
	//v.x /= 1 + (v.z / 3) * -_PerspectiveCorrection;
	v += _Pivot;
	IN.vertex = mul(unity_WorldToObject, v);
}



float4 FlxVertStandardWithOutline(appdata_full IN) : SV_POSITION
{
	v2f o;
	float4 v = mul(unity_ObjectToWorld, IN.vertex);
	float4 inNormal = float4(IN.normal.x, IN.normal.y, IN.normal.z, 0);
	float4 normal = mul(unity_ObjectToWorld, inNormal);

	v -= _Pivot;
	v.y += _PerspectiveCorrection * v.z * v.y * .7;
	v.z -= _PerspectiveCorrection * v.y * 3;
	v.y *= _Flatten;
	v += _Pivot;

	v.xyz += normalize(normal) * (_Outline * 10);

	o.pos = v;

	o.pos = mul(unity_WorldToObject, o.pos);
	o.pos = UnityObjectToClipPos(o.pos);

	return o.pos;
}



void FlxSurfStandard(Input IN, inout SurfaceOutput o)
{
	float4 albedo = tex2D(_MainTex, IN.uv_MainTex) * _Color;

	o.Albedo = albedo.rgb;
	o.Alpha = albedo.a;

	float3 rimNormal = -IN.viewDir;
	rimNormal.y *= _RimCamera;

	#if defined(FLX_SPECULAR)
	float spec = tex2D(_SpecularTex, IN.uv_MainTex).a * _Specular;
	float gloss = tex2D(_GlossTex, IN.uv_MainTex).a * _Gloss;
	#else
	float spec = _Specular;
	float gloss = _Gloss;
	#endif
	
	o.Specular = spec;
	o.Gloss = gloss;

	float VdotN = dot(rimNormal, WorldNormalVector(IN, o.Normal));
	float VdotNn = (VdotN + 1.0) / 2.0;

	VdotNn = saturate((VdotNn - _RimCutoff) / (1.0 - _RimCutoff));

	half3 rim = pow(VdotNn, _RimPower) * _RimColor.rgb * _RimColor.a * 0.5;

	#if defined(FLX_EMISSION)
	o.Emission = rim + _EmissionColor * tex2D(_EmissionTex, IN.uv_MainTex);
	#else
	o.Emission = rim + float4(1,1,1,1);
	o.Emission = rim + _EmissionColor;
	#endif
}