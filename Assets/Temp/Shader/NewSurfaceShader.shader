﻿Shader "Custom/NewSurfaceShader" {
	Properties{
		_MainTex("Texture", 2D) = "white" {}
	_Amount("Extrusion Amount", Range(-1,12)) = 0.5

	_Size("Size", Float) = 0
		_Pivot("_Pivot", Vector) = (0,0,0,0)
		_Flatten("Flatten", Float) = 1.0

		_RimColor("Rim Color", Color) = (0.97,0.88,1,0.75)
		_RimPower("Rim Power", Float) = 2.5
		_BumpMap("Normal (Normal)", 2D) = "bump" {}
	_SpecularTex("Specular Level (R) Gloss (G) Rim Mask (B)", 2D) = "gray" {}
	_RampTex("Toon Ramp (RGB)", 2D) = "white" {}
	_Cutoff("Alphatest Cutoff", Range(0, 1)) = 0.5

	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		CGPROGRAM
#pragma surface surf TF2 vertex:vert addshadow
		struct Input {
		float2 uv_MainTex;
		INTERNAL_DATA
	};

	sampler2D _RampTex;


	inline fixed4 LightingTF2(SurfaceOutput s, fixed3 lightDir, fixed3 viewDir, fixed atten)
	{
		fixed3 h = normalize(lightDir + viewDir);

		fixed NdotL = dot(s.Normal, lightDir) * 0.5 + 0.5;
		fixed3 ramp = tex2D(_RampTex, float2(NdotL * atten, 0)).rgb;

		float nh = max(0, dot(s.Normal, h));
		float spec = pow(nh, s.Gloss * 128) * s.Specular;

		fixed4 c;
		c.rgb = ((s.Albedo * ramp * _LightColor0.rgb + _LightColor0.rgb * spec) * (atten * 2));
		c.a = s.Alpha;
		return c;
	}




	float _Size;
	float _Flatten;
	float4 _Pivot;

	sampler2D _SpecularTex, _BumpMap;
	float4 _RimColor;
	float _RimPower;

	float _Amount;
	void vert(inout appdata_full IN)
	{

		float4 v = mul(unity_ObjectToWorld, IN.vertex);

		v -= _Pivot;

		v.y += _Size * v.z * v.y;
		v += _Pivot;
		v.y *= _Flatten;
		IN.vertex = mul(unity_WorldToObject, v);

	}
	sampler2D _MainTex;
	void surf(Input IN, inout SurfaceOutput o)
	{
		o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
		o.Alpha = tex2D(_MainTex, IN.uv_MainTex).a;
		o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));
		float3 specGloss = tex2D(_SpecularTex, IN.uv_MainTex).rgb;
		o.Specular = specGloss.r;
		o.Gloss = specGloss.g;

		half3 rim = pow(max(0, dot(float3(0, 1, 0), WorldNormalVector(IN, o.Normal))), _RimPower) * _RimColor.rgb * _RimColor.a * specGloss.b;
		o.Emission = rim;
	}
	ENDCG
	}
	FallBack "Diffuse"
}
