﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Flx/Outline" 
{
	Properties 
	{
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}

		[HDR]
		_EmissionColor("EmissionColor", Color) = (1, 1, 1, 0)
		_RimColor("Rim Color", Color) = (1,1,1,1)
		_RimPower("Rim Power", Float) = 1
		_RimCutoff("Rim Cutoff", Range(0.0, 1.0)) = 0
		_RimCamera("Rim Camera", Range(0.0, 1.0)) = 0

		_SpecularTex("Specular Level (R) Gloss (G) Rim Mask (B)", 2D) = "gray" {}
		_RampTex("Toon Ramp (RGB)", 2D) = "white" {}

		_Pivot("_Pivot", Vector) = (0,0,0,0)

		_PerspectiveCorrection("Perspective Correction", Float) = 0
		_Flatten("Flatten", Float) = 1.0

		_OutlineColor("Outline Color", Color) = (0,0,0,1)
		_Outline("Outline width", Range(0.0, 0.1)) = .0
	}


	CGINCLUDE
	#include "UnityCG.cginc"


	
	float4 _Color;
	fixed4 _EmissionColor;

	sampler2D _MainTex;
	sampler2D _SpecularTex;
	sampler2D _RampTex;

	float4 _RimColor;
	float _RimPower;
	float _RimCutoff;
	float _RimCamera;

	float4 _Pivot;
	float _PerspectiveCorrection;
	float _Flatten;

	uniform float _Outline;
	uniform float4 _OutlineColor;

	ENDCG

		SubShader 
		{

		Pass{
			Tags{ "RenderType" = "Opaque" }
			Cull Front

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag

			struct v2f {
				float4 pos : SV_POSITION;
			};

			float4 vert(appdata_full IN) : SV_POSITION
			{
				v2f o;
				float4 v = mul(unity_ObjectToWorld, IN.vertex);

				float4 inNormal = float4(IN.normal.x, IN.normal.y, IN.normal.z, 0);

				float4 normal = mul(unity_ObjectToWorld, inNormal);

				v -= _Pivot;
				v.y += _PerspectiveCorrection * v.z * v.y * .7;
				v.z -= _PerspectiveCorrection * v.y * 3;
				v.y *= _Flatten;
				v += _Pivot;

				v.xyz += normalize(normal) * (_Outline * 10);

				o.pos = v;

				o.pos = mul(unity_WorldToObject, o.pos);
				o.pos = UnityObjectToClipPos(o.pos);

				return o.pos;
			}

			half4 frag(v2f v) : COLOR
			{
				return _OutlineColor;
			}

			ENDCG
		}




		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM

		#pragma surface surf FlxShade vertex:vert addshadow
		#pragma target 3.0

		struct Input
		{
			float2 uv_MainTex;
			float3 viewDir;
			INTERNAL_DATA
		};

		inline fixed4 LightingFlxShade(SurfaceOutput s, fixed3 lightDir, fixed3 viewDir, fixed atten)
		{
			fixed3 h = normalize(lightDir + viewDir);

			fixed NdotL = dot(s.Normal, lightDir) * 0.5 + 0.5;
			fixed3 ramp = tex2D(_RampTex, float2(NdotL * atten, 0)).rgb;

			float nh = max(0, dot(s.Normal, h));
			float spec = pow(nh, s.Gloss * 128) * s.Specular;

			fixed4 c;
			c.rgb = ((s.Albedo * ramp * _LightColor0.rgb + _LightColor0.rgb * spec) * (atten * 2));
			c.a = s.Alpha;
			return c;
		}




		void vert(inout appdata_full IN)
		{
			float4 v = mul(unity_ObjectToWorld, IN.vertex);

			v -= _Pivot;
			v.y += _PerspectiveCorrection * v.z * v.y * .7;
			v.z -= _PerspectiveCorrection * v.y * 3;
			v.y *= _Flatten;
			v += _Pivot;

			IN.vertex = mul(unity_WorldToObject, v);
		}




		void surf (Input IN, inout SurfaceOutput o) 
		{
			float4 albedo = tex2D(_MainTex, IN.uv_MainTex) * _Color;

			o.Albedo = albedo.rgb;
			o.Alpha = albedo.a;

			float3 rimNormal = -IN.viewDir;
			rimNormal.y *= _RimCamera;

			float3 specGloss = tex2D(_SpecularTex, IN.uv_MainTex).rgb;
			o.Specular = specGloss.r;
			o.Gloss = specGloss.g;

			float VdotN = dot(rimNormal, WorldNormalVector(IN, o.Normal));
			float VdotNn = (VdotN + 1.0) / 2.0;
			
			VdotNn = saturate((VdotNn - _RimCutoff) / (1.0 - _RimCutoff));

			half3 rim = pow(VdotNn, _RimPower) * _RimColor.rgb * _RimColor.a * specGloss.b;
			o.Emission = rim + _EmissionColor;
		}

		ENDCG

	}

	FallBack "Diffuse"
}
