﻿Shader "Flx/Transparent" 
{
	Properties 
	{
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Color (RGB) Alpha (A)", 2D) = "white" {}
		_RampTex("Toon Ramp (RGB)", 2D) = "white" {}

		[Space(30)]
		[HDR]
		_EmissionColor("EmissionColor", Color) = (1, 1, 1, 0)
		_EmissionTex("Emission", 2D) = "white" {}

		[Space(30)]
		_RimColor("Rim Color", Color) = (1,1,1,1)
		_RimTex("Rim Tex", 2D) = "gray" {}
		_RimPower("Rim Power", Float) = 0.2
		_RimCutoff("Rim Cutoff", Range(0.0, 1.0)) = 0.5
		_RimCamera("Rim Camera", Range(0.0, 1.0)) = 0

		[Space(30)]
		_Specular("Specular", Range(0.0, 1.0)) = 0
		_SpecularTex("Specular Tex", 2D) = "gray" {}

		[Space(30)]
		_Gloss("Gloss", Range(0.001, 1.0)) = 0.05
		_GlossTex("Gloss Tex", 2D) = "gray" {}

		[Space(30)]
		_Pivot("Pivot", Vector) = (0,0,0,0)
		_PerspectiveCorrection("Perspective Correction", Float) = 0
		_Flatten("Flatten", Float) = 1.0
	}


	CGINCLUDE
	#include "UnityCG.cginc"
	#include "Flx.cginc"

	#pragma shader_feature FLX_EMISSION
	#pragma shader_feature FLX_SPECULAR
	#pragma shader_feature FLX_RIM

	ENDCG

	SubShader 
	{
	
		Tags { "Queue"="Transparent" "RenderType"="Transparent" }
		LOD 200

		CGPROGRAM

		#pragma surface FlxSurfStandard FlxShade vertex:FlxVertStandard addshadow alpha
		// #pragma surface surf Lambert 
		#pragma target 3.0

		ENDCG

	}

	FallBack "Diffuse"
	CustomEditor "FlxStandardEditor"
}
