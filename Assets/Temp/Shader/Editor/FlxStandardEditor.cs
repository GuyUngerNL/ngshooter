﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class FlxStandardEditor : MaterialEditor
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		if (!isVisible)
			return;

		// get the current keywords from the material
		Material targetMat = target as Material;
		string[] keyWords = targetMat.shaderKeywords;

		// Emission
		/*
		#pragma shader_feature FLX_EMISSION
		#pragma shader_feature FLX_SPECULAR
		#pragma shader_feature FLX_RIM
		*/

		var keywordList = new List<string>();

		var emissionKeyword = keyWords.Contains("FLX_EMISSION");
		var emissionTexture = targetMat.GetTexture("_EmissionTex") != null;
		if (emissionTexture) keywordList.Add("FLX_EMISSION");

		var specularKeyword = keyWords.Contains("FLX_SPECULAR");
		var specularTexture = targetMat.GetTexture("_SpecularTex") != null || targetMat.GetTexture("_EmissionTex") != null;
		if (specularTexture) keywordList.Add("FLX_SPECULAR");

		var rimKeyword = keyWords.Contains("FLX_RIM");
		var rimTexture = targetMat.GetTexture("_RimTex") != null;
		if (rimTexture) keywordList.Add("FLX_RIM");

		targetMat.shaderKeywords = keywordList.ToArray();

		if (emissionKeyword != emissionTexture ||
			specularKeyword != specularTexture ||
			rimKeyword != rimTexture
			)
		{
			EditorUtility.SetDirty(targetMat);

		}
	}
}
