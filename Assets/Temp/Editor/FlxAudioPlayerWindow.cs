﻿using UnityEngine;
using UnityEditor;
public class FlxAudioPlayerWindow : EditorWindow
{
	FlxAudioAsset audio;

	float fadeIn;
	float fadeOut;
	float delay;
	bool loop;

	// Add menu named "My Window" to the Window menu
	[MenuItem("Window/Flx Audio Player Tester")]
	static void Init()
	{
		FlxAudioPlayerWindow window = (FlxAudioPlayerWindow)EditorWindow.GetWindow(typeof(FlxAudioPlayerWindow));
		window.Show();
	}

	void OnGUI()
	{
		if(!Application.isPlaying)
		{
			EditorGUILayout.HelpBox("Only works in playmode.", MessageType.Info);
			return;
		}

		GUILayout.Label("Audio", EditorStyles.boldLabel);
		audio = (FlxAudioAsset)EditorGUILayout.ObjectField(new GUIContent("Audio Asset"), audio, typeof(FlxAudioAsset), false);


		fadeIn = EditorGUILayout.FloatField("Fade In", fadeIn);
		fadeOut = EditorGUILayout.FloatField("Fade Out", fadeOut);
		delay = EditorGUILayout.FloatField("Delay", delay);
		loop = EditorGUILayout.Toggle("Loop", loop);

		EditorGUI.BeginDisabledGroup(audio == null);

		EditorGUILayout.BeginHorizontal();

		if (GUILayout.Button("Play"))
		{
			audio.Play
			(
				fadeIn: fadeIn,
				delay: delay,
				loop: loop
			);
		}

		if (GUILayout.Button("Pause"))
		{
			audio.Pause
			(
				fadeOut: fadeOut,
				delay: delay
			);
		}

		if (GUILayout.Button("Resume"))
		{
			audio.Resume
			(
				fadeIn: fadeIn,
				delay: delay
			);
		}

		if (GUILayout.Button("Stop"))
		{
			audio.Stop
			(
				fadeOut: fadeOut,
				delay: delay
			);
		}

		EditorGUILayout.EndHorizontal();

		EditorGUI.EndDisabledGroup();

		if(audio != null)
		{
			EditorGUILayout.LabelField("Is Playing:", $"{audio.isPlaying}");
		}
	}
}