﻿using System.Collections; 
using System.Collections.Generic;
using UnityEngine; 

public class TestStuff : MonoBehaviour
{
	public Camera targetCamera;

	public bool applyCustomProjection = true;

	float left;
	float right;
	float top;
	float bottom;

	public float scale = 36f;
	public Vector2 offset;

	[Range(0,1)]
	public float force = 1f;

	Vector3 fixedPosition;

	void LateUpdate()
	{
		if (applyCustomProjection)
		{
			transform.position += (transform.position - fixedPosition) * force;

			left = targetCamera.aspect * -scale + offset.x * force;
			right = targetCamera.aspect * scale + offset.x * force;
			top = scale + offset.y * force;
			bottom = -scale + offset.y * force;

			Matrix4x4 m = PerspectiveOffCenter(left, right, bottom, top, targetCamera.nearClipPlane, targetCamera.farClipPlane);
			targetCamera.projectionMatrix = m;
		}
		else
		{
			fixedPosition = transform.position;
			targetCamera.ResetProjectionMatrix();
		}
	}

	static Matrix4x4 PerspectiveOffCenter(float left, float right, float bottom, float top, float near, float far)
	{
		float x = 2.0F * near / (right - left);
		float y = 2.0F * near / (top - bottom);
		float a = (right + left) / (right - left);
		float b = (top + bottom) / (top - bottom);
		float c = -(far + near) / (far - near);
		float d = -(2.0F * far * near) / (far - near);
		float e = -1.0F;
		Matrix4x4 m = new Matrix4x4();
		m[0, 0] = x;
		m[0, 1] = 0;
		m[0, 2] = a;
		m[0, 3] = 0;
		m[1, 0] = 0;
		m[1, 1] = y;
		m[1, 2] = b;
		m[1, 3] = 0;
		m[2, 0] = 0;
		m[2, 1] = 0;
		m[2, 2] = c;
		m[2, 3] = d;
		m[3, 0] = 0;
		m[3, 1] = 0;
		m[3, 2] = e;
		m[3, 3] = 0;
		return m;
	}
}
