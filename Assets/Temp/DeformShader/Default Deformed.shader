﻿Shader "Custom/Default Deformed" {
	Properties 
	{
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0

		_Axis("Axis", Vector) = (0,0,0)
		_Center("Center", Vector) = (0,0,0)
		_Length("Length", Float) = 1.0
		_Size("Size", Float) = 1.0

		_Squash("Squash", Float) = 1.0
		_Stretch("Stretch", Float) = 1.0

		_Pinch("Pinch", Range(-1, 2)) = 0

	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows vertex:vert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input 
		{
			float2 uv_MainTex;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		float4 _Axis;
		float4 _Center;
		float _Length;
		float _Size;
		float _Squash;
		float _Stretch;

		float _Pinch;

		void vert(inout appdata_full IN)
		{
			float4 v = IN.vertex - _Center;
			float4 a = normalize(_Axis);

			float4 ca = (dot(v, a) / dot(a, a)) * a;

			float4 av = v - ca;

			float ca_l = length(ca) / _Length;
			float av_l = length(av) / _Size;

			float pinchMod = cos(ca_l) * _Pinch;

			IN.vertex.xyz = ca * _Squash + av * _Stretch + av * pinchMod;

			IN.vertex.xyz += _Center;
		}

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}


