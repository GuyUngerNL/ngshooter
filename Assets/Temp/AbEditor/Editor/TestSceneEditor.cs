﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class TestSceneEditor : EditorWindow
{
	[MenuItem("Window/Test Scene Editor")]
	static void Create()
	{
		var window = GetWindow<TestSceneEditor>(false, "Tile Editor", true);
		window.autoRepaintOnSceneChange = true;
		window.Show();
	}

	void OnFocus()
	{
		SceneView.onSceneGUIDelegate -= this.OnSceneGUI;
		SceneView.onSceneGUIDelegate += this.OnSceneGUI;
	}

	void OnDestroy()
	{
		SceneView.onSceneGUIDelegate -= this.OnSceneGUI;
	}

	public Vector3 pos;

	void OnSceneGUI(SceneView sceneView)
	{
		//int controlID = GUIUtility.GetControlID(FocusType.Passive);

		//HandleUtility.AddDefaultControl(controlID);

		//Tools.current = Tool.None;

		//Handles.BeginGUI();

		
		//Handles.EndGUI();


		//pos = Handles.PositionHandle(pos, Quaternion.identity);
		//pos.y = 0f;

		//int x = Mathf.FloorToInt(pos.x / 10f) * 10;
		//int y = Mathf.FloorToInt(pos.z / 10f) * 10;

		//pos.x = x;
		//pos.z = y;

		//Handles.DrawSolidRectangleWithOutline(new Rect(x, y, 10f, 10f), Color.blue, Color.red);
	}


}
