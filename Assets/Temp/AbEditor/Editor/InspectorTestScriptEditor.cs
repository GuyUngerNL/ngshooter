using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using Rotorz.ReorderableList;

[CustomEditor(typeof(InspectorTestScript))]
public class InspectorTestScriptEditor : Editor
{

	SerializedProperty spListA;
	SerializedProperty spListB;

	void OnEnable()
	{
		spListA = serializedObject.FindProperty("listA");
		spListB = serializedObject.FindProperty("listB");
	}

	public override void OnInspectorGUI()
	{

		InspectorTestScript instance = target as InspectorTestScript;

		serializedObject.Update();
		EditorGUILayout.Space();

		ReorderableListGUI.Title("List A");
		ReorderableListGUI.ListField(spListA);

		ReorderableListGUI.Title("List b");
		ReorderableListGUI.ListField(spListB, ReorderableListFlags.ShowIndices);

		serializedObject.ApplyModifiedProperties();
	}


}
