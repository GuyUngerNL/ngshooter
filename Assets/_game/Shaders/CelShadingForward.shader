﻿Shader "Custom/CelShadingForward" {
	Properties{
		_Color("Color", Color) = (1, 1, 1, 1)
		_EmissionColor("EmissionColor", Color) = (1, 1, 1, 0)
		_MainTex("Albedo (RGB)", 2D) = "white" {}

		_Size("Size", Float) = 0
		_Pivot("_Pivot", Vector) = (0,0,0,0)
		_Flatten("Flatten", Float) = 1.0
	}
		SubShader{
		Tags{
		"RenderType" = "Opaque"
	}
		LOD 200

		CGPROGRAM
		#pragma surface surf CelShadingForward vertex:vert
		#pragma target 3.0

		half4 LightingCelShadingForward(SurfaceOutput s, half3 lightDir, half atten) {
		half NdotL = dot(s.Normal, lightDir);
		if (NdotL <= 0.0) NdotL = 0;
		else NdotL = 1;
		half4 c;
		c.rgb = s.Albedo * _LightColor0.rgb * (NdotL * atten * 2);
		c.a = s.Alpha;
		return c;
	}

	sampler2D _MainTex;
	fixed4 _Color;
	fixed4 _EmissionColor;

	float _Size;
	float _Flatten;
	float4 _Pivot;

	void vert(inout appdata_full IN)
	{

		float4 v = mul(unity_ObjectToWorld, IN.vertex);

		v -= _Pivot;

		v.y += _Size * v.z * v.y;
		v += _Pivot;
		v.y *= _Flatten;
		IN.vertex = mul(unity_WorldToObject, v);

	}

	struct Input {
		float2 uv_MainTex;
	};

	void surf(Input IN, inout SurfaceOutput o) {
		// Albedo comes from a texture tinted by color
		fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;

		o.Albedo = c.rgb;
		o.Emission = _EmissionColor;
		o.Alpha = c.a;
	}
	ENDCG
	}

	FallBack "Diffuse"
}