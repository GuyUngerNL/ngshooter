using UnityEngine; /*author Kristian Macanga, Gungrounds*/
using System.Collections;
using System.Collections.Generic;

public class ChanceStruct<T> where T:struct
{
	private List<T> chances;
	private List<T> currentChances;
	private bool allowSameInRow = true;
	private T last = default(T);
	public ChanceStruct( List<T> arr, bool allowSameInRow = true ) 
	{
		chances = arr;
		this.allowSameInRow = allowSameInRow;
		setChances( chances );
	}

	public void addChance( T element )
	{
		currentChances.Add( element );
		chances.Add( element );
	}

	public void setChances( List<T> chances )
	{
		this.chances = chances;

		this.currentChances =  copyArray( this.chances );
	}

	public void resetChances()
	{
		setChances( chances );
	}

	public int getLength()
	{
		return currentChances.Count;
	}

	public int getMaxLength()
	{
		return chances.Count;
	}

	public T getRndElement(bool resetOnEmpty = true, bool searchTillElementIsNotChance = false) 
	{
		if ( resetOnEmpty && currentChances.Count < 1 )
		{
			setChances( chances );
		}

		T element =  chooseAndRemove( currentChances );
		if (allowSameInRow == false) {
			while (last.Equals (element)) {
				element = default(T);
				if ( resetOnEmpty && currentChances.Count < 1 ) setChances( chances );
				element =  chooseAndRemove( currentChances );
			}
			last = default(T);
			last = element;
		}
			
		return element;
		 
	}
 
	static public List<T> copyArray( List<T> array )
	{

		List<T> resArray = new List<T>();
		for (int i=0; i < array.Count; i++ )
		{ 
			resArray.Add(   array[i]   );
		}

		return resArray;
	}

	static public T chooseAndRemove(List<T> Options)
	{
		int rnd = Mathf.FloorToInt((Math.random() * 0.999f) * Options.Count);
		try
		{
			T res = Options[rnd];
			Options.Remove(res);
			return res;
		}
		catch
		{
			Log.msgError("chance struct index bug", rnd, Options.Count);
			return default(T);
		}
		
	}

}

