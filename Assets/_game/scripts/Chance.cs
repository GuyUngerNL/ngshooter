using UnityEngine; /*author Kristian Macanga, Gungrounds*/
using System.Collections;
using System.Collections.Generic;

public class Chance<T> where T:class
{
	public List<T> chances;
	private List<T> currentChances;
	private bool allowSameInRow = true;
	private T last = null;
	public Chance ( List<T> arr, bool allowSameInRow = true ) 
	{
		chances = arr;
		this.allowSameInRow = allowSameInRow;
		setChances( chances );
	}

	public void addChance( T element )
	{
		currentChances.Add( element );
		chances.Add( element );
	}

	public void removeChance( T element )
	{
		currentChances.Remove (element);
		chances.Remove( element );
	}

	public void setChances( List<T> chances )
	{
		this.chances = chances;
		this.currentChances =  copyArray( this.chances );
	}

	public void resetChances()
	{
		setChances( chances );
	}

	public int getLength()
	{
		return currentChances.Count;
	}

	public int getMaxLength()
	{
		return chances.Count;
	}

	public T getRndElement(bool resetOnEmpty = true, bool searchTillElementIsNotChance = false) 
	{
		if ( resetOnEmpty && currentChances.Count == 0 )
		{
			setChances( chances );
		}

		if(currentChances.Count == 0)
		{
			throw new System.Exception("CHANCES ARRAY IS EMPTY! IT SHOULD NEVER BE EMPTY!");
		}

		T element =  chooseAndRemove( currentChances );
		if (allowSameInRow == false) {
			while (last == element) {
				element = null;
				if ( resetOnEmpty && currentChances.Count == 0 ) setChances( chances );
				element =  chooseAndRemove( currentChances );
			}
			last = null;
			last = element;
		}

		if ( searchTillElementIsNotChance && element is Chance<T>)
		{
			Chance<T> el = element as Chance<T>;
			return el.getRndElement();
		}
		else
		{
			return element;
		}
	}

	public T getRndElementDontRemove(bool resetOnEmpty = true, bool searchTillElementIsNotChance = false)
	{
		if (resetOnEmpty && currentChances.Count == 0)
		{
			setChances(chances);
		}

		T element = chooseAndRemove(currentChances);
		if (allowSameInRow == false)
		{
			while (last == element)
			{
				element = null;
				if (resetOnEmpty && currentChances.Count == 0) setChances(chances);
				element = choose(currentChances);
			}
			last = null;
			last = element;
		}

		if (searchTillElementIsNotChance && element is Chance<T>)
		{
			Chance<T> el = element as Chance<T>;
			return el.getRndElement();
		}
		else
		{
			return element;
		}
	}


	public T getElementAtIndex(int index, bool resetOnEmpty = true, bool searchTillElementIsNotChance = false)
	{
		if (resetOnEmpty && currentChances.Count == 0)
		{
			setChances(chances);
		}

		T element = chooseAndRemoveByIndex(currentChances, index);
		if (searchTillElementIsNotChance && element is Chance<T>)
		{
			Chance<T> el = element as Chance<T>;
			return el.getRndElement();
		}
		else
		{
			return element;
		}
	}

	static public List<T> copyArray( List<T> array )
	{

		List<T> resArray = new List<T>();
		for (int i=0; i < array.Count; i++ )
		{ 
			resArray.Add(   array[i]   );
		}

		return resArray;
	}

	static public T choose(List<T> Options)
	{
		int rnd = Mathf.FloorToInt((Math.random() * 0.999f) * Options.Count);
		T res = Options[rnd];
		return res;
	}

	static public T chooseAndRemove(List<T> Options)
	{
		int rnd = Mathf.FloorToInt( (Math.random()*0.999f) * Options.Count );
		T res = Options[rnd];
		Options.Remove(res);
		return res;
	}


	static public T chooseAndRemoveByIndex(List<T> Options, int index)
	{
		int rnd = index;
		T res = Options[rnd];
		Options.Remove(res);
		return res;
	}

}

