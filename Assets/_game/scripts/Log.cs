﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;

public class Log {
	public static void msg(params object[] values)
	{
		string result = "";
		foreach (object value in values)
		{
			if (value == null)
				result += "null, ";
			else if(value is IList)
			{
				result += "[";
				foreach (var item in (IList)value)
				{
					result += item.ToString() + ", ";
				}
				result += "], ";
			}
			else
				result += value.ToString() + ", ";
		}
		if (result.Length > 0) result.Substring(0, result.Length - 2);
		UnityEngine.Debug.Log(result);
	}

	public static void msgError(params object[] values)
	{
		string result = "";
		foreach (object value in values)
		{
			if (value == null)
				result += "null, ";
			else
				result += value.ToString() + ", ";
		}
		if (result.Length > 0) result.Substring(0, result.Length - 2);
		UnityEngine.Debug.LogError(result);
	}

	public static void msgWarning(params object[] values)
	{
		string result = "";
		foreach (object value in values)
		{
			if (value == null)
				result += "null, ";
			else
				result += value.ToString() + ", ";
		}
		if (result.Length > 0) result.Substring(0, result.Length - 2);
		UnityEngine.Debug.LogWarning(result);
	}

	public static void StressTest(string name, Action callback, int times)
	{
		var sw = new Stopwatch();
		sw.Start();

		for (int i = 0; i < times; i++)
		{
			callback();
		}

		sw.Stop();

		UnityEngine.Debug.Log(string.Format("{0} = {1}ms", name, sw.ElapsedMilliseconds));
	}
}