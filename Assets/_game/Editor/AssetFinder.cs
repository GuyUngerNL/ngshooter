﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using Noopol;

public class AssetFinder : EditorWindow
{
	[MenuItem("Asset Finder", menuItem = "Window/Asset Finder")]
	static void Create()
	{
		var window = GetWindow<AssetFinder>("Asset Finder", true);
		window.titleContent = new GUIContent("Asset Finder");
		window.autoRepaintOnSceneChange = true;
		window.Show();
	}

	GameObject[] results = new GameObject[0];
	bool[] toggles;
	int toggleCount;
	bool includeInherited = true;
	bool includeChildrenComponents = true;

	string searchType;

	string[] typeNames;
	Type[] types;
	int selectedType;

	private void OnEnable()
	{
		UpdateResults();
	}

	Vector2 scrollPos;

	void UpdateResults()
	{
		var coms = from assembly in AppDomain.CurrentDomain.GetAssemblies()
					 from type in assembly.GetTypes()
					 where type.IsSubclassOf(typeof(Component))
					 select type;

		if(!string.IsNullOrEmpty(searchType))
		{
			coms = coms.Where(x => x.FullName.ToLower().Contains(searchType.ToLower()));
		}

		types = coms.ToArray();
		typeNames = coms.Select(x => x.Name).ToArray();
		selectedType = 0;

		UpdateAssets();
	}

	void UpdateAssets()
	{
		results = new GameObject[0];

		if (selectedType >= types.Length) return;

		var prefabs = AssetDatabase.FindAssets("t:Prefab").Select(x => AssetDatabase.GUIDToAssetPath(x)).Select(x => AssetDatabase.LoadAssetAtPath<GameObject>(x));

		var query = includeChildrenComponents ?
			prefabs.Where(x => x.GetComponentInChildren(types[selectedType])) :
			prefabs.Where(x => x.GetComponent(types[selectedType]));

		if (!includeInherited)
		{
			query = query.Where(x => x.GetComponentInChildren(types[selectedType]).GetType() == types[selectedType]);
		}

		results = query.ToArray();

		toggles = new bool[results.Length];
		for (int i = 0; i < toggles.Length; i++) toggles[i] = true;
		toggleCount = toggles.Length;
	}

	void OnGUI()
	{
		EditorGUILayout.LabelField("Search components", EditorStyles.boldLabel);

		EditorGUILayout.BeginHorizontal();

		EditorGUI.BeginChangeCheck();
		searchType = EditorGUILayout.TextField(searchType);
		if(EditorGUI.EndChangeCheck())
		{
			UpdateResults();
		}

		EditorGUI.BeginChangeCheck();
		selectedType = EditorGUILayout.Popup(selectedType, typeNames);
		if (EditorGUI.EndChangeCheck())
		{
			UpdateAssets();
		}

		GUILayout.Label(string.Format("({0})", typeNames.Length), GUILayout.Width(40f));

		EditorGUILayout.EndHorizontal();

		EditorGUILayout.Space();

		EditorGUILayout.BeginHorizontal();


		EditorGUILayout.EndHorizontal();

		EditorGUI.BeginChangeCheck();
		includeInherited = EditorGUILayout.ToggleLeft($"Include classes inherited from ({typeNames[selectedType]})", includeInherited);
		if (EditorGUI.EndChangeCheck())
		{
			UpdateAssets();
		}

		EditorGUI.BeginChangeCheck();
		includeChildrenComponents = EditorGUILayout.ToggleLeft($"Include components on prefab children objects", includeChildrenComponents);
		if (EditorGUI.EndChangeCheck())
		{
			UpdateAssets();
		}

		EditorGUILayout.Space();

		EditorGUILayout.BeginHorizontal();

		if (GUILayout.Button("Select All", GUILayout.Width(80f)))
		{
			for (int i = 0; i < toggles.Length; i++)
			{
				toggles[i] = true;
			}

			toggleCount = toggles.Length;
		}

		if (GUILayout.Button("Select None", GUILayout.Width(80f)))
		{
			for (int i = 0; i < toggles.Length; i++)
			{
				toggles[i] = false;
			}

			toggleCount = 0;
		}

		if (GUILayout.Button($"Select in Project ({toggleCount}/{results.Length} prefabs)"))
		{
			Selection.objects = results.Where(x => toggles[Array.IndexOf(results, x)]).ToArray();
		}

		EditorGUILayout.EndHorizontal();


		scrollPos = EditorGUILayout.BeginScrollView(scrollPos);

		for (int i = 0; i < results.Length; i++)
		{
			EditorGUILayout.BeginHorizontal();

			EditorGUI.BeginChangeCheck();
			toggles[i] = GUILayout.Toggle(toggles[i], GUIContent.none, GUILayout.Width(20f));
			if (EditorGUI.EndChangeCheck())
			{
				toggleCount = toggles.Where(x => x).Count();
			}


			GUI.enabled = false;
			EditorGUILayout.ObjectField(results[i], typeof(GameObject), false);
			GUI.enabled = true;

			EditorGUILayout.EndHorizontal();
		}


		EditorGUILayout.EndScrollView();

		

	}

}