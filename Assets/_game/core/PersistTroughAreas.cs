﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistTroughAreas : MonoBehaviour
{
	private void Awake()
	{
		DontDestroyOnLoad(gameObject);
		Events.OnGameUnload += OnGameUnload;
	}

	private void OnDestroy()
	{
		Events.OnGameUnload -= OnGameUnload;
	}

	private void OnGameUnload()
	{
		Events.OnGameUnload -= OnGameUnload;
		Destroy(gameObject);
	}
}
