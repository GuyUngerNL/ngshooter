﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : FlxManager {

	bool _paused = false;

	public float freeze = 0f;

	public List<TimeScaler> scales;
	internal bool devSlowMotion;

	protected override void Awake()
	{
		base.Awake();
		scales = new List<TimeScaler>();
	}

	public enum FreezeMode
	{
		mix = 0,
		max = 1,
		add = 2
	}

	public void Freeze(float duration, FreezeMode mode = FreezeMode.mix, float max = .2f)
	{
		float _freeze = 0;
		switch (mode)
		{
			case FreezeMode.mix:
				_freeze = Mathf.Max(freeze, duration * .5f) + duration * .5f;
				break;
			case FreezeMode.max:
				_freeze = Mathf.Max(freeze, duration);
				break;
			case FreezeMode.add:
				_freeze = freeze + duration;
				break;
		}
		
		_freeze = Math.min(_freeze, max);
		freeze = Math.max(_freeze, freeze);
	}

	public void Pause()
	{
		if (_paused) return;

		_paused = true;
		Time.timeScale = 0f;
	}

	public void Resume()
	{
		if (!_paused) return;

		_paused = false;
		Update();
	}

	public void Scale(float strength, float duration)
	{
		scales.push(new TimeScaler(duration, strength));
	}

	void Update ()
	{
		if (_paused)
		{
			Time.timeScale = 0f;
		}
		else
		{
			freeze -= Time.deltaTime / Time.timeScale;
			if (freeze > 0) {
				Time.timeScale = .05f;
			} else {
				float timeScale = 1;
				for (int i = 0; i < scales.Count; i++)
				{
					timeScale = Mathf.Min(scales[i].strength, timeScale);
					scales[i] = new TimeScaler(scales[i].time - Time.unscaledDeltaTime, scales[i].strength);
					if(scales[i].time <= 0)
					{
						scales.Remove(scales[i]);
					}
				}
				Time.timeScale = timeScale * (devSlowMotion ? .1f : 1f);
			}
		}
	}

	public struct TimeScaler
	{
		public float time;
		public float strength;

		public TimeScaler(float time, float strength)
		{
			this.time = time;
			this.strength = strength;
		}
	}
}
