﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class FlxMenuControls : FlxControls
{
	public float deadZone = 0.3f;
	public float repeatDelay;
	public float actionsPerSecond;

	public bool isMoveUpPressed
	{
		get
		{
			return (rewiredPlayer?.GetAxis("UIVertical") ?? 0f) > deadZone;
		}
	}

	public bool isMoveDownPressed
	{
		get
		{
			return (rewiredPlayer?.GetAxis("UIVertical") ?? 0f) < -deadZone;
		}
	}

	public bool isMoveRightPressed
	{
		get
		{
			return (rewiredPlayer?.GetAxis("UIHorizontal") ?? 0f) > deadZone;
		}
	}

	public bool isMoveLeftPressed
	{
		get
		{
			return (rewiredPlayer?.GetAxis("UIHorizontal") ?? 0f) < -deadZone;
		}
	}

	public bool isPrimaryPressed
	{
		get
		{
			return rewiredPlayer?.GetButtonDown("UIPrimary") ?? true;
		}
	}

	public bool isSecondaryPressed
	{
		get
		{

			return rewiredPlayer?.GetButtonDown("UISecondary") ?? true;
		}
	}

	public bool isCancelPressed
	{
		get
		{

			return rewiredPlayer?.GetButtonDown("UICancel") ?? true;
		}
	}

	public bool isMenuPressed
	{
		get
		{

			return rewiredPlayer?.GetButtonDown("UIMenu") ?? true;
		}
	}
}
