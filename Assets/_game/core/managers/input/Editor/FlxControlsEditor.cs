using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(FlxControls), true)]
public class ControlsEditor : Editor
{

    SerializedProperty spRewiredPlayerid;

    string[] playerNames = new string[] {
        "Player 1",
        "Player 2",
        "Player 3",
        "Player 4",
    };

    protected virtual void OnEnable()
    {
        spRewiredPlayerid = serializedObject.FindProperty("rewiredPlayerId");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.Space();

        if(spRewiredPlayerid.intValue >= playerNames.Length)
        {
            spRewiredPlayerid.intValue = 0;
        }

        spRewiredPlayerid.intValue = EditorGUILayout.Popup("Rewired Player", spRewiredPlayerid.intValue, playerNames);

        serializedObject.ApplyModifiedProperties();
    }

}
