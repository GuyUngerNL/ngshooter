using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(FlxShooterControls))]
public class FlxShooterControlsEditor : ControlsEditor
{
	bool debugOpen;

	protected override void OnEnable()
	{
		base.OnEnable ();

		debugOpen = false;
	}

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI ();

		debugOpen = EditorGUILayout.Foldout (debugOpen, "Debug Information");

		if(debugOpen)
		{
			EditorGUI.indentLevel++;

			var input = target as FlxShooterControls;

			EditorGUILayout.LabelField ("Move Up", input.isMoveUpPressed ? "Pressed" : "");
			EditorGUILayout.LabelField ("Move Down", input.isMoveDownPressed ? "Pressed" : "");
			EditorGUILayout.LabelField ("Move Left", input.isMoveLeftPressed ? "Pressed" : "");
			EditorGUILayout.LabelField ("Move Right", input.isMoveRightPressed ? "Pressed" : "");

			EditorGUILayout.LabelField ("Shoot", input.isShootPressed ? "Pressed" : "");
			EditorGUILayout.LabelField ("Special", input.isSpecialPressed ? "Pressed" : "");

			EditorGUILayout.LabelField ("Aim angle", Math.NormalizeAngle(((int)input.getAimAngle ())).ToString ());

			EditorGUI.indentLevel--;
		}
	}

}
