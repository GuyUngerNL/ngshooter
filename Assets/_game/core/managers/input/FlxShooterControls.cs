﻿using UnityEngine; /*author Kristian Macanga, Gungrounds*/
using System.Collections;
using System.Collections.Generic;
using System;
using Rewired;

public class FlxShooterControls : FlxControls
{
	public FlxActor centerActor;

	public float gamepadStickDeadzone = 0.2f;

	public bool isAiming
	{
		get
		{
			if (rewiredPlayer == null)
			{
				return false;
			}
			else if (activeControllerType == GameInputType.Joystick)
			{
				return rewiredPlayer.GetAxis2D(ControlsDefinitions.Aim_Horizontal, ControlsDefinitions.Aim_Vertical).sqrMagnitude > 0;
			}

			return true;
		}
	}

	private float lastAimingAngle = 0;
	private Vector2 lastPos;

	public float getAimAngle()
	{
		Vector2 pos = Vector2.left;


		if (rewiredPlayer == null || !isAiming)
		{
			return lastAimingAngle;
		}

		var aimAxis = rewiredPlayer.GetAxis2D(ControlsDefinitions.Aim_Horizontal, ControlsDefinitions.Aim_Vertical);

		if (m_activeControllerType == GameInputType.Joystick)
		{
			pos = -aimAxis;
		}
		else if(m_activeControllerType == GameInputType.Keyboard && aimAxis.sqrMagnitude > 0f)
		{
			pos = -aimAxis;
		}
		else if (HasInput(GameInputType.Mouse))
		{
			if(DevConsole.nexMachinaMouseAim)
			{
				return nexMachinaAimingAngle;
			}
			else
			{
				pos = mousePosition - (Vector2)(centerActor?.position ?? Vector3.zero);
				pos.x *= -1f;
			}
		}

		float angle = Vector2.Angle(Vector2.left, pos);
		Vector3 cross = Vector3.Cross(Vector2.left, pos);
		lastAimingAngle = (cross.z > 0) ? -angle : angle;
		return lastAimingAngle;
	}

	
	float nexMachinaAimingAngle = 0f;
	float nexMachinaMouseSensitivity = 0.1f; // input delta will be multiplied by this
	float nexMachinaDeadZone = 0.01f; // if delta is less than this this it wont change angle
	float nexMachinaMaxDelta = 0.2f; // delta will never go higher than this

	protected override void Update()
	{
		base.Update();

		var mDelta = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

		var pos = mDelta * nexMachinaMouseSensitivity * -1f;

		var strenght = Mathf.Clamp(pos.magnitude, 0f, nexMachinaMaxDelta);

		if(strenght > nexMachinaDeadZone)
		{
			float angle2 = Vector2.Angle(Vector2.left, pos.normalized);
			Vector3 cross2 = Vector3.Cross(Vector2.left, pos.normalized);
			var newAngle = (cross2.z > 0) ? -angle2 : angle2;

			nexMachinaAimingAngle = Mathf.LerpAngle(nexMachinaAimingAngle, newAngle, strenght);
		}
	}

	public bool isMoving
	{
		get
		{
			if (rewiredPlayer == null)
			{
				return false;
			}
			else if (HasInput(GameInputType.Joystick))
			{
				return rewiredPlayer.GetAxis2D(ControlsDefinitions.Move_Horizontal, ControlsDefinitions.Move_Vertical).sqrMagnitude > 0;
			}
			else if (HasInput(GameInputType.Keyboard) || HasInput(GameInputType.Mouse))
			{
				return true;
			}
			return false;


			//old version : doesnt check has input keyboard / mouse - instead returns true

		}
	}

	public float getMovementAngle(Vector2 relativePosition)
	{
		Vector2 pos = Vector2.left;

		if (rewiredPlayer == null || !isMoving) //|| inputType == GameInputType.KeyboardAndMouse -- check hasInput (?)
		{
			return 0;
		}

		pos = -rewiredPlayer.GetAxis2D(ControlsDefinitions.Move_Horizontal, ControlsDefinitions.Move_Vertical);

		float deadzone = 0.7f;
		Vector2 stickInput = new Vector2(pos.x, pos.y);
		if (stickInput.magnitude < deadzone) stickInput = Vector2.zero;
		pos.x = stickInput.x;
		pos.y = stickInput.y;

		float angle = Vector2.Angle(Vector2.left, pos);
		Vector3 cross = Vector3.Cross(Vector2.left, pos);
		return (cross.z > 0) ? -angle : angle;
	}


	public Vector2 movementVector
	{
		get
		{
			return rewiredPlayer == null ? default(Vector2) : rewiredPlayer.GetAxis2D(RewiredConsts.Action.Move_Horizontal, RewiredConsts.Action.Move_Vertical);
		}
	}

	public bool isMoveUpPressed
	{
		get
		{

			return rewiredPlayer == null ? false : rewiredPlayer.GetAxis(ControlsDefinitions.Move_Vertical) > gamepadStickDeadzone;
		}
	}

	public bool isMoveDownPressed
	{
		get
		{
			return rewiredPlayer == null ? false : (rewiredPlayer.GetAxis(ControlsDefinitions.Move_Vertical) < -gamepadStickDeadzone);
		}
	}

	public bool isMoveRightPressed
	{
		get
		{
			return rewiredPlayer == null ? false : rewiredPlayer.GetAxis(ControlsDefinitions.Move_Horizontal) > gamepadStickDeadzone;
		}
	}

	public bool isMoveLeftPressed
	{
		get
		{
			return rewiredPlayer == null ? false : rewiredPlayer.GetAxis(ControlsDefinitions.Move_Horizontal) < -gamepadStickDeadzone;
		}
	}

	public bool isShootPressed
	{
		get
		{
			if (rewiredPlayer == null) { return false; }

			if (activeControllerType != GameInputType.Mouse && rewiredPlayer.GetAxis2D(ControlsDefinitions.Aim_Horizontal, ControlsDefinitions.Aim_Vertical).sqrMagnitude > 0f)
			{
				return true;
			}

			return rewiredPlayer.GetButton(ControlsDefinitions.Shoot);
		}
	}

	public bool isShootJustPressed
	{
		get
		{
			return rewiredPlayer == null ? false : rewiredPlayer.GetButtonDown(ControlsDefinitions.Shoot);
		}
	}

	public bool isSpecialPressed
	{
		get
		{

			return rewiredPlayer == null ? false : rewiredPlayer.GetButton(ControlsDefinitions.Special);
		}
	}

	public bool isSpecialJustPressed
	{
		get
		{

			return rewiredPlayer == null ? false : rewiredPlayer.GetButtonDown(ControlsDefinitions.Special);
		}
	}

	public float moveVerticalForce
	{
		get
		{
			return rewiredPlayer == null ? 0 : rewiredPlayer.GetAxis(ControlsDefinitions.Move_Vertical);
		}
	}

	public float moveHorizontalForce
	{
		get
		{
			return rewiredPlayer == null ? 0 : rewiredPlayer.GetAxis(ControlsDefinitions.Move_Horizontal);
		}
	}

	public bool isPickItemJustPressed
	{
		get
		{
			return rewiredPlayer == null ? false : rewiredPlayer.GetButtonDown(ControlsDefinitions.Pick_item);
		}
	}
}
