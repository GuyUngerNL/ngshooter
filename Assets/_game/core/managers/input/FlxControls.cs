﻿using UnityEngine; /*author Kristian Macanga, Gungrounds*/
using System.Collections;
using System.Collections.Generic;
using System;
using Rewired;

[Flags]
public enum GameInputType
{
	None =      0,
	Keyboard =  1 << 0,
	Mouse =     1 << 1,
	Joystick =  1 << 2,
	Touch =     1 << 3,
}

public abstract class FlxControls : FlxComponent
{
	public delegate void ActiveControllerChangedHandler();

	public event ActiveControllerChangedHandler ActiveControllerChanged;

	[SerializeField]
	protected int rewiredPlayerId;

	protected GameInputType inputType = GameInputType.None;

	public Rewired.Player rewiredPlayer;

	protected int rewiredControllerId;

	protected GameInputType m_activeControllerType = GameInputType.None;

	public GameInputType activeControllerType
	{
		get 
		{ 
			return m_activeControllerType; 
		}
		private set 
		{
			if(m_activeControllerType == value) {
				return;
			}

			m_activeControllerType = value;

			if(ActiveControllerChanged != null) {
				ActiveControllerChanged ();
			}
		}
	}

	public GameInputType type 
	{
		get { return inputType; }
	}

	protected virtual void Update()
	{
		UpdateActiveController();
	}

	public void UpdateActiveController()
	{
		if(rewiredPlayer == null) {
			activeControllerType = GameInputType.None;
		} else {

			Controller controller = rewiredPlayer.controllers.GetLastActiveController ();

			if(controller == null) {
				activeControllerType = GameInputType.None;
				return;
			}

			switch(controller.type)
			{
				case ControllerType.Joystick:
					activeControllerType = GameInputType.Joystick;
					break;
				case ControllerType.Keyboard:
					activeControllerType = GameInputType.Keyboard;
					break;
				case ControllerType.Mouse:
					activeControllerType = GameInputType.Mouse;
					break;
			}
		}
		 
	}

	public bool HasInput(GameInputType type)
	{
		return (inputType & type) > 0;
	}

	protected virtual void Awake()
	{
		// just assert that the input manager exists in the scene
		var input = Managers.Input;

		SetRewiredPlayer(rewiredPlayerId);

		ReInput.ControllerDisconnectedEvent += OnControllerDisconnected;
	}

	public void SetRewiredPlayer(int id)
	{
		rewiredPlayer = ReInput.players.GetPlayer(id);
		rewiredPlayerId = rewiredPlayer.id;

		if (rewiredPlayer == null)
		{
			return;
		}

		UpdateGameInputType();
	}

	protected void UpdateGameInputType()
	{
		inputType = GameInputType.None;

		if(rewiredPlayer == null) {
			return;
		}

		if(rewiredPlayer.controllers.ContainsController<Mouse> (0)) {
			inputType |= GameInputType.Mouse;
		}

		if(rewiredPlayer.controllers.ContainsController<Keyboard> (0)) {
			inputType |= GameInputType.Keyboard;
		}

		if(rewiredPlayer.controllers.joystickCount > 0) {
			inputType |= GameInputType.Joystick;
		}
	}

	protected void OnControllerDisconnected(ControllerStatusChangedEventArgs args) 
	{
		UpdateGameInputType ();
	}

	public bool isAnythingPressed
	{
		get
		{
			return rewiredPlayer == null ? false : (rewiredPlayer.GetAnyButton());
		}
	}

	Vector2 lastMousePosition;

	public Vector2 mousePosition
	{
		get
		{
			lastMousePosition = FlxCamera.main ? FlxCamera.main.mousePosition : lastMousePosition;
			return lastMousePosition;
		}
	}
}
