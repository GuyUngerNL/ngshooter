﻿public static class ControlsDefinitions
{
	// Default

	// Menu
	[Rewired.Dev.ActionIdFieldInfo(categoryName = "Menu", friendlyName = "UISubmit")]
	public const int UISubmit = 8;
	[Rewired.Dev.ActionIdFieldInfo(categoryName = "Menu", friendlyName = "UICancel")]
	public const int UICancel = 9;
	[Rewired.Dev.ActionIdFieldInfo(categoryName = "Menu", friendlyName = "UIHorizontal")]
	public const int UIHorizontal = 31;
	[Rewired.Dev.ActionIdFieldInfo(categoryName = "Menu", friendlyName = "UIVertical")]
	public const int UIVertical = 32;
	// Game
	[Rewired.Dev.ActionIdFieldInfo(categoryName = "Game", friendlyName = "Shoot")]
	public const int Shoot = 0;
	[Rewired.Dev.ActionIdFieldInfo(categoryName = "Game", friendlyName = "Special")]
	public const int Special = 1;
	[Rewired.Dev.ActionIdFieldInfo(categoryName = "Game", friendlyName = "Move Vertical")]
	public const int Move_Vertical = 2;
	[Rewired.Dev.ActionIdFieldInfo(categoryName = "Game", friendlyName = "Move Horizontal")]
	public const int Move_Horizontal = 3;
	[Rewired.Dev.ActionIdFieldInfo(categoryName = "Game", friendlyName = "Aim Vertical")]
	public const int Aim_Vertical = 4;
	[Rewired.Dev.ActionIdFieldInfo(categoryName = "Game", friendlyName = "Aim Horizontal")]
	public const int Aim_Horizontal = 5;
	[Rewired.Dev.ActionIdFieldInfo(categoryName = "Game", friendlyName = "Buying and equipping items.")]
	public const int Pick_item = 17;

}