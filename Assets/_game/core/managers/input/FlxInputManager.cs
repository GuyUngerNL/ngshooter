﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class FlxInputManager : FlxManager
{
	public Rewired.Player system;

	public Rewired.Player player1;
	public Rewired.Player player2;

	public Rewired.Player[] players;

	bool sessionInitialized;
	List<int> assignedJoysticks;

	public bool hasStarted { get; private set; }

	IEnumerator Start()
	{
		while (!ReInput.isReady)
		{
			yield return null;
		}

		hasStarted = true;

		system = ReInput.players.GetPlayer(RewiredConsts.Player.System);

		player1 = ReInput.players.GetPlayer(RewiredConsts.Player.Player1);
		player2 = ReInput.players.GetPlayer(RewiredConsts.Player.Player2);

		players = new Rewired.Player[] { player1, player2 };

		assignedJoysticks = new List<int>();

		ReInput.ControllerConnectedEvent += OnControllerConnected;

		foreach (var player in players)
		{
			player.controllers.hasKeyboard = false;
			player.controllers.hasMouse = false;
		}

		AssignAllJoysticksToSystemPlayer(true);
	}

	void AssignAllJoysticksToSystemPlayer(bool removeFromOtherPlayers)
	{
		foreach (var j in ReInput.controllers.Joysticks)
		{
			system.controllers.AddController<Joystick>(j.id, removeFromOtherPlayers);
		}
	}

	void AssignUnassignedJoysticksToSystemPlayer()
	{
		foreach (var j in ReInput.controllers.Joysticks)
		{
			if (assignedJoysticks.Contains(j.id)) continue;
			system.controllers.AddController<Joystick>(j.id, true);
		}
	}

	void OnControllerConnected(ControllerStatusChangedEventArgs args)
	{
		if (args.controllerType != ControllerType.Joystick) return;

		if (assignedJoysticks.Contains(args.controllerId)) return;

		system.controllers.AddController<Joystick>(args.controllerId, true);
	}

	public void InitSession()
	{
		if (sessionInitialized) return;

		sessionInitialized = true;

		StartCoroutine(Session());
	}

	public void HideCursor()
	{
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}

	public void ShowCursor()
	{
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
	}

	IEnumerator Session()
	{
		while (!hasStarted)
		{
			yield return null;
		}

		Controller controller = ReInput.controllers.GetLastActiveController();

		AssignAllJoysticksToSystemPlayer(true);

		// If its keyboard or mouse assign both to player 1
		if (controller == null || controller.type == ControllerType.Keyboard || controller.type == ControllerType.Mouse)
		{
			player1.controllers.hasKeyboard = true;
			player1.controllers.hasMouse = true;
		}
		else if (controller.type == ControllerType.Joystick)
		{
			player1.controllers.AddController(controller, true);
			assignedJoysticks.Add(controller.id);
		}
	}

	public void AddControllerToPlayer(Controller controller, Rewired.Player player)
	{
		if (controller.type == ControllerType.Keyboard || controller.type == Rewired.ControllerType.Mouse)
		{
			player.controllers.hasKeyboard = true;
			player.controllers.hasMouse = true;
		}
		else
		{
			player.controllers.AddController(controller, false);
			assignedJoysticks.Add(controller.id);
		}
	}

	public void ClearControllersFromPlayer(Rewired.Player player)
	{
		foreach(var j in player.controllers.Joysticks)
		{
			assignedJoysticks.Remove(j.id);
		}

		player.controllers.ClearAllControllers();

		AssignUnassignedJoysticksToSystemPlayer();
	}

	public bool ControllerIsAssigned(Controller controller)
	{
		return
			player1.controllers.ContainsController(controller) ||
			player2.controllers.ContainsController(controller);
	}

	public bool PlayerHasAssignedController(Rewired.Player player)
	{
		return player.controllers.hasKeyboard || player.controllers.joystickCount > 0;
	}
}
