﻿using System;

public enum Platform
{
	None          = 0,
	Any			  = 0,
	All			  = 0xffffff,

	PC            = 1 << 0,
	Mac           = 1 << 1,
	Linux         = 1 << 2,
	Steam         = 1 << 3,
	Handheld      = 1 << 4,
	Ios           = 1 << 5,
	Android       = 1 << 6,
	PS3           = 1 << 7,
	PS4           = 1 << 8,
	XBoxOne       = 1 << 9,
	XBox360       = 1 << 10,
	Web           = 1 << 11,
	WinPhone81	  = 1 << 12,
}
