﻿using UnityEngine;
using System.Collections;
using System;
using Steamworks;

public class ServerManager : FlxManager {

	public static string SERVER_URL = "http://104.131.43.136/api/";
	public static string GAME_ID = "7e77b59e-d123-4c38-84e7-fe64f27e477f";
	public static Platform PLATFORM = Platform.Steam | Platform.PC;
	public static bool isConnectedToSteam = false;

	 void Update(){
		if(ServerManager.isConnectedToSteam == false){
			connectSteam ();
		}
	}

	public static void connectSteam(){
		if(SteamManager.Initialized) {
			ServerManager.isConnectedToSteam = true;
			//var id = SteamUser.GetSteamID ();
			//var appID = SteamUtils.GetAppID().ToString ();
			//var accountId = id.GetAccountID ();
			//string name = SteamFriends.GetPersonaName();
			//string userID = accountId.m_AccountID.ToString (); 
			Debug.Log ("Steam connected");
		}
	}
}