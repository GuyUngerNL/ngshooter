﻿using System.Collections.Generic;

public class AudioList
{
	public static string PlaylistGameplay = "mario";
	public static string PlaylistGameplayStatic = "Gameplay Static";

	public static string soundCoin1 = "coin1";
	public static string soundGameplayStatic = "Gameplay Static";
	public static string soundCoinAdded = "coin added";

	public static string SndMineExplosion = "MineExplode";

	public static string SndEnemyImpact1 = "enemy_impact1";
	public static string SndEnemyImpact2 = "enemy_impact2";
	public static string SndEnemyImpact3 = "enemy_impact4";
	public static Chance<string> SndEnemyImpacts = new Chance<string>(new List<string>() { SndEnemyImpact1, SndEnemyImpact2, SndEnemyImpact3 }, false);

	public static string SndChestNear1 = "ChestNear1";
	public static string SndChestNear2 = "ChestNear2";
	public static string SndChestNear3 = "ChestNear3";
	public static Chance<string> SndChestNear = new Chance<string>(new List<string>() { SndChestNear1, SndChestNear2, SndChestNear3 }, false);

	public static string SndEnemy1Kill1 = "monster-10";
	public static string SndEnemy1Kill2 = "monster-11";
	public static string SndEnemy1Kill3 = "monster-12";
	public static string SndEnemy1Kill4 = "monster-13";
	public static string SndEnemy1Kill5 = "monster-14";
	public static string SndEnemy1Kill6 = "monster-15";
	public static string SndEnemy1Kill7 = "monster-16";
	public static string SndEnemy1Kill8 = "monster-17";
	public static string SndEnemy1Kill9 = "monster-18";
	public static Chance<string> SndEnemy1Killed = new Chance<string>(new List<string>() { SndEnemy1Kill1, SndEnemy1Kill2, SndEnemy1Kill3, SndEnemy1Kill4, SndEnemy1Kill5, SndEnemy1Kill6, SndEnemy1Kill7, SndEnemy1Kill8, SndEnemy1Kill9 }, false);

	public static string SndReadyGo = "ReadyGo";



	public static string SndGameOver = "GameOver";


	public static string SndPunch1 = "punch";
	public static string SndPunch2 = "punch2";
	public static string SndPunch3 = "punch3";
	public static string SndPunch4 = "punch4";
	//public static Chance<string> SndPunchs:Chance<string> = new Chance<string>([Data.SndPunch1, Data.SndPunch2, Data.SndPunch3, Data.SndPunch4]);

	public static string SndCoinAdd = "kill add";
	public static string SndKillAdd = "kill add2";
	public static string SndScoreAdd = "score add";
	public static string SndBouncersCollide = "bouncerscollide";
	public static string SndKeyAppears = "key appears";
	public static string SndKeyGrabbed = "key grabbed";

	public static string SndDoorOpen1 = "doorOpen_1";
	public static string SndDoorOpen2 = "doorOpen_2";
	public static Chance<string> SndDoors = new Chance<string>(new List<string>() { SndDoorOpen1, SndDoorOpen2 });


	public static string SndMineTouched = "mine activated";

	public static string SndTrapNukeBombTicking = "sndNukeBombTicking";
	public static string SndTrapNukeBombExplosion = "sndNukeBombExplosion";
	public static string SndNukeExplosion = "sndNukeExplosion";
	public static string SndLaserCharging = "LASER_TANK_CHARGE_3";
	public static string SndLaserShot = "LASER_TANK_SHOOT_11";
	//interface
	public static string SndOpenShop = "open shop";
	public static string SndCloseShop = "back to game";
	public static string SndBuyItem = "buy item";
	public static string SndGetStartingCoins = "get starting coins";
	public static string SndLifeLost = "life lost";
	public static string SndRollover = "rollover";
	//player hit
	public static string SndPlayerDamagedByEnemy = "bite-small";

	//footsteps
	// public static string SndFootSteps:Array<String> = [SndFootStep1, SndFootStep2, SndFootStep3, SndFootStep4, SndFootStep5, SndFootStep6, SndFootStep7, SndFootStep8, SndFootStep9, SndFootStep10];
	public static string SndFootStep1 = "data/sounds/footstep00";
	public static string SndFootStep2 = "data/sounds/footstep01";
	public static string SndFootStep3 = "data/sounds/footstep02";
	public static string SndFootStep4 = "data/sounds/footstep03";
	public static string SndFootStep5 = "data/sounds/footstep04";
	public static string SndFootStep6 = "data/sounds/footstep05";
	public static string SndFootStep7 = "data/sounds/footstep06";
	public static string SndFootStep8 = "data/sounds/footstep07";
	public static string SndFootStep9 = "data/sounds/footstep08";
	public static string SndFootStep10 = "data/sounds/footstep09";
	// paper
   public static string SndPaper1 = "paper1";
	public static string SndPaper2 = "paper2";
	public static string SndPaper3 = "paper3";
	public static string SndPaper4 = "paper4";
	public static string SndPaper5 = "paper5";
	public static string SndPaper6 = "paper6";
	public static Chance<string> SndPapers = new Chance<string>( new List<string>() {SndPaper1, SndPaper2, SndPaper3, SndPaper4, SndPaper5, SndPaper6});
	

	//enemy spawn
   // public static string SndEnemySpawns:Chance<string> = new Chance<string>([SndEnemySpawn1, SndEnemySpawn2, SndEnemySpawn3, SndEnemySpawn4, SndEnemySpawn5, SndEnemySpawn6, SndEnemySpawn7, SndEnemySpawn8, SndEnemySpawn9, SndEnemySpawn10],false);
	public static string SndEnemySpawn1 = "data/sounds/INGAME/ENEMIES/SPAWN/slime1";
	public static string SndEnemySpawn2 = "data/sounds/INGAME/ENEMIES/SPAWN/slime2";
	public static string SndEnemySpawn3 = "data/sounds/INGAME/ENEMIES/SPAWN/slime3";
	public static string SndEnemySpawn4 = "data/sounds/INGAME/ENEMIES/SPAWN/slime4";
	public static string SndEnemySpawn5 = "data/sounds/INGAME/ENEMIES/SPAWN/slime5";
	public static string SndEnemySpawn6 = "data/sounds/INGAME/ENEMIES/SPAWN/slime6";
	public static string SndEnemySpawn7 = "data/sounds/INGAME/ENEMIES/SPAWN/slime7";
	public static string SndEnemySpawn8 = "data/sounds/INGAME/ENEMIES/SPAWN/slime8";
	public static string SndEnemySpawn9 = "data/sounds/INGAME/ENEMIES/SPAWN/slime9";
	public static string SndEnemySpawn10 = "data/sounds/INGAME/ENEMIES/SPAWN/slime10";

	//coins
	public static string SndPickUp = "pick up";
	public static string SndPickUpCoinStart = "pickupCoinStart";
	public static string SndCoinCollected = "pickupCoin";

	public static string SndDeathMenu = "magical choir";
	
	public static string SndLaserGun1 = "lasergun1";
	public static string SndPistolShot = "sndPistolShot";
	public static string SndPowerUpRandom = "sndPowerUpRandom";
	
	public static string SndButtonFall = "data/sounds/sndButtonFall";
	public static string SndProgressNotify1 = "data/sounds/progressNotify1";
	public static string SndProgressNotify2 = "data/sounds/progressNotify2";
	public static string SndProgressNotify3 = "data/sounds/progressNotify3";
	public static string SndProgressNotify4 = "data/sounds/progressNotify4";
	public static string SndProgressNotify5 = "data/sounds/progressNotify5";
	public static string SndProgressNotify6 = "data/sounds/progressNotify6";
	public static string SndProgressNotify7 = "data/sounds/progressNotify7";
	public static string SndProgressNotify8 = "data/sounds/progressNotify8";
	public static string SndProgressNotify9 = "data/sounds/progressNotify9";
	public static string SndProgressNotify10 = "data/sounds/progressNotify10";
	public static string SndProgressNotify11 = "data/sounds/progressNotify11";
	public static string SndProgressNotify12 = "data/sounds/progressNotify12";
	public static string SndCoin1 = "data/sounds/coin1";
	public static string SndCoin2 = "data/sounds/coin2";
	public static string SndCoin3 = "data/sounds/coin3";
	public static string SndCoin4 = "data/sounds/coin4";
	public static string SndCoin5 = "data/sounds/coin5";
	public static string SndCoin6 = "data/sounds/coin6";
	public static string SndCoin7 = "data/sounds/coin7";
	public static string SndCoin8 = "data/sounds/coin8";
	public static string SndCoinHit1 = "sndCoinHit1";
	public static string SndCoinHit2 = "data/sounds/sndCoinHit2";
	public static string SndCoinHit3 = "data/sounds/sndCoinHit3";

	//public static List<string> SndNewCoins:Array<String> = [Data.SndNewCoin1, Data.SndNewCoin2, Data.SndNewCoin3, Data.SndNewCoin4, Data.SndNewCoin5, Data.SndNewCoin6, Data.SndNewCoin7, Data.SndNewCoin8, Data.SndNewCoin9, Data.SndNewCoin10];
	public static string SndNewCoin1 = "data/sounds/INGAME/PICKUPS/coin1";
	public static string SndNewCoin2 = "data/sounds/INGAME/PICKUPS/coin2";
	public static string SndNewCoin3 = "data/sounds/INGAME/PICKUPS/coin3";
	public static string SndNewCoin4 = "data/sounds/INGAME/PICKUPS/coin4";
	public static string SndNewCoin5 = "data/sounds/INGAME/PICKUPS/coin5";
	public static string SndNewCoin6 = "data/sounds/INGAME/PICKUPS/coin6";
	public static string SndNewCoin7 = "data/sounds/INGAME/PICKUPS/coin7";
	public static string SndNewCoin8 = "data/sounds/INGAME/PICKUPS/coin8";
	public static string SndNewCoin9 = "data/sounds/INGAME/PICKUPS/coin9";
	public static string SndNewCoin10 = "data/sounds/INGAME/PICKUPS/coin10";

	public static string SndChestOpen = "chest open";
	public static string SndChestGrab = "chest grab";


	public static string SndHit = "hit";
	public static string SndHitSoft = "hit soft";


}