﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AudioManager : FlxManager
{
	public void PlayMultiple(FlxAudioAsset[] group, float volume = 1, float pitch = 1)
	{
		if (group == null) return;

		for (int i = 0; i < group.Length; i++)
		{
			group[i].Play(volume: volume, pitch: pitch);
		}
	}
}

