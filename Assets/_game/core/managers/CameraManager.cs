﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : FlxManager
{
	public FlxCamera flxCamera { get; private set; }

	public enum STATES
	{
		combat = 0,
	}

	public STATES state;

	public Camera camera;
	public Camera cameraUI;
	
}
