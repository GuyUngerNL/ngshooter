﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeManager : FlxComponent {
	
	public List<IShake> shakes = new List<IShake>();

	public override void OnRecycle()
	{
		base.OnRecycle();
		shakes.Clear();
		actor.shake = Vector3.zero;
		actor.shakeRotation = Vector3.zero;
	}

	void Update ()
	{
		Vector3 position = default(Vector3);
		Vector3 rotation = default(Vector3);

		for (int i = 0; i < shakes.Count; i++)
		{
			IShake shake = shakes[i];

			shake.Update();

			float shakeScale = 1;//? Main.user.screenshakeScale;
			
			position += shake.position * shakeScale;
			rotation += shake.rotation * shakeScale;

			if (shake.isDone)
			{
				shakes.Remove(shake);
				i--;
			}
		}

		actor.shake = position;
		actor.shakeRotation = rotation;
	}

	public void AddShake(IShake shake)
	{
		shakes.push(shake);
	}
}
