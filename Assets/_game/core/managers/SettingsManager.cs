﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsManager : FlxManager
{

	#region Game Global Settings (resolution, quality, screen mode)

	public struct GameResolution : IEquatable<GameResolution>
	{
		public readonly int width;
		public readonly int height;

		public GameResolution(int width, int height)
		{
			this.width = width;
			this.height = height;
		}

		public override bool Equals(object obj)
		{
			return (obj is GameResolution) && Equals((GameResolution)obj);
		}

		public bool Equals(GameResolution that)
		{
			return this.width == that.width && this.height == that.height;
		}

		public static bool operator ==(GameResolution x, GameResolution y)
		{
			return x.Equals(y);
		}

		public static bool operator !=(GameResolution x, GameResolution y)
		{
			return !x.Equals(y);
		}

		public override int GetHashCode()
		{
			return width ^ height;
		}
	}


	GameResolution[] m_resolutions;
	string[] m_qualityLevels;

	public GameResolution[] resolutions
	{
		get
		{
			if (m_resolutions == null)
			{
				List<GameResolution> tempList = new List<GameResolution>();
				var screenResolutions = Screen.resolutions;

				foreach (var screenResolution in screenResolutions)
				{
					GameResolution tempResolution = new GameResolution(screenResolution.width, screenResolution.height);

					if (!tempList.Contains(tempResolution))
					{
						tempList.Add(tempResolution);
					}
				}

				m_resolutions = tempList.ToArray();
			}

			return m_resolutions;
		}
	}


	public string[] qualityLevels
	{
		get
		{
			if (m_qualityLevels == null)
			{
				m_qualityLevels = QualitySettings.names;
			}

			return m_qualityLevels;
		}
	}


	int currentWidth, currentHeight, targetWidth, targetHeight;
	bool currentFullscreen, targetFullscreen;

	int currentQualityLevel, targetQualityLevel;

	int currentvSyncCount, targetvSyncCount;

	public void Start()
	{
		currentWidth = targetWidth = Screen.width;
		currentHeight = targetHeight = Screen.height;
		currentFullscreen = targetFullscreen = Screen.fullScreen;
		currentQualityLevel = targetQualityLevel = QualitySettings.GetQualityLevel();
		targetvSyncCount = currentvSyncCount = QualitySettings.vSyncCount;
	}

	public void SetResolution(int width, int height, bool fullscreen)
	{
		targetWidth = width;
		targetHeight = height;
		targetFullscreen = fullscreen;
	}

	public void SetQualityLevel(int level)
	{
		targetQualityLevel = level;
	}


	void Update()
	{
		if (currentHeight != targetHeight || currentWidth != targetWidth || currentFullscreen != targetFullscreen)
		{
			currentWidth = targetWidth;
			currentHeight = targetHeight;
			currentFullscreen = targetFullscreen;
			Screen.SetResolution(currentWidth, currentHeight, currentFullscreen);
		}


		if (currentQualityLevel != targetQualityLevel)
		{
			currentQualityLevel = targetQualityLevel;
			QualitySettings.SetQualityLevel(currentQualityLevel, true);
		}

		if (currentvSyncCount != targetvSyncCount)
		{
			QualitySettings.vSyncCount = currentvSyncCount = targetvSyncCount;
		}
	}

	#endregion


	#region User settings (audio, controls)

	


	#endregion

}
