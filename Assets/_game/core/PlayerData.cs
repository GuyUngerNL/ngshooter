﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
	// Each player in co-op mode will have different index, main-player will have playerIndex = 0
	public int playerIndex; 
	
	public Player player;

	public PlayerData(int playerIndex)
	{
		this.playerIndex = playerIndex;

		Init();
	}
	
	//vars affected by upgrades
	public float movementSpeed;
	
	public void Init()
	{
		
	}

}
