﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Events  {

	//
	public static void DestroyGameRelatedEvents()
	{
		
	}

	// Called when the game is ended to clean up all game related objects that stay between scenes like Player, HUD, ...
	public delegate void EventOnGameUnload();
	public static event EventOnGameUnload OnGameUnload;
	public static void TriggerOnGameUnload()
	{
		if (OnGameUnload != null)
		{
			OnGameUnload.Invoke();
		}
	}


}

