﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelContentData : MonoBehaviour
{

	public void ActivateChildren(bool enableAlways)
	{
		LevelContentData[] allChildrentContentList = GetComponentsInChildren<LevelContentData>(true);
		List<LevelContentData> contentList = new List<LevelContentData>();
		for (int i = 0; i < allChildrentContentList.Length; i++)
		{
			LevelContentData contentData = allChildrentContentList[i];
			if (contentData.gameObject.transform.parent == gameObject.transform)
			{
				if (enableAlways) contentData.gameObject.SetActive(true); 
				contentList.Add(contentData);
			}
		}

		if (contentList.Count > 0)
		{
			LevelContentData selectedContent = contentList.GetRandom<LevelContentData>();
			for (int i = 0; i < contentList.Count; i++)
			{
				LevelContentData content = contentList[i];
				if (content == selectedContent)
				{
					content.gameObject.SetActive(true); 
					content.ActivateChildren(false);
				}
				else
				{
					content.gameObject.SetActive(false); 
				}
			}
		}

	}

	private void OnDrawGizmos()
	{
		LevelContentData[] allChildrentContentList = GetComponentsInChildren<LevelContentData>(true);
		for (int i = 0; i < allChildrentContentList.Length; i++)
		{
			LevelContentData contentData = allChildrentContentList[i];
			if (contentData.gameObject.transform.parent == gameObject.transform)
			{
				if (contentData.name.charAt(0) == '$' == false)
				{
					contentData.name = "$" + contentData.name;
				}
			}
		}
	}
}