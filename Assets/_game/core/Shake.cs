﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IShake
{
	Vector3 rotation { get; }
	Vector3 position { get; }
	bool isDone { get; }
	void Update();
}
