﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : FlxMotion
{

	protected float movementScale {get{ return GetComponent<CombatEntity>().movementScale ; } } //* GetComponent<CombatEntity>().movementRebalanceScale
	protected float attackScale { get{ return GetComponent<CombatEntity>().attackScale; }}
	protected float resistanceScale { get{ return GetComponent<CombatEntity>().resistanceScale; } }

 
	protected virtual float movementElapsed
	{
		get
		{
			return elapsed * movementScale;
		}
	}
	/*
	protected CombatEntity entity;
	
	private void OnEnable()
	{
		entity = GetComponent<CombatEntity>();
	}
	*/
	protected virtual void Update()
	{
	 
	}
	
}
