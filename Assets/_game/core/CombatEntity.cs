﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[SelectionBase]
public class CombatEntity : FlxComponent
{
	//[Header("Combat Entity")]
	[HideInInspector]
	public float movementScale; 
	[HideInInspector]
	public float attackScale;
	[HideInInspector]
	public float resistanceScale;
	[HideInInspector]
	public float sizeScale;

	public bool hasShadow = true;
	internal bool isShadowVisible = true;

	FlxBasic shadow;
	
	protected virtual void OnDisable()
	{
		if (shadow != null)
		{
			shadow.exists = false;
			shadow = null;
		}
	}

	protected virtual void Update()
	{
		// Update Shadow
		if(hasShadow)
		{
			if (shadow == null)
			{
				//shadow = Prefabs.Shadow.Recycle();
			}

			var shadowPos = actor.position + actor.offset;
			shadowPos.z = .5f;
			shadow.position = shadowPos;
			shadow.visible = actor.visible && isShadowVisible;
		}
		
	}

	public virtual void Hurt(FlxActor hurter, float damage = 1)
	{
	}
 
	public virtual void OnKill()
	{
	
	}
}