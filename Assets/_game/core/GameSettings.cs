﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; 

public class GameSettings : ScriptableObject
{
#if UNITY_EDITOR
	const string DEFAULT_ASSET_PATH = "Assets/_game/Resources/Game Settings.asset";
#endif

	static GameSettings m_main;

	public static GameSettings main
	{
		get
		{
			if (m_main == null)
			{
				m_main = Resources.Load<GameSettings>("Game Settings");

#if UNITY_EDITOR
				if (m_main == null)
				{
					m_main = UnityEditor.AssetDatabase.LoadAssetAtPath<GameSettings>(DEFAULT_ASSET_PATH);
				}

				if (m_main == null)
				{
					m_main = ScriptableObject.CreateInstance<GameSettings>();
					UnityEditor.AssetDatabase.CreateAsset(m_main, DEFAULT_ASSET_PATH);
					UnityEditor.AssetDatabase.SaveAssets();
					UnityEditor.AssetDatabase.Refresh();
				}
#endif

				m_main = Instantiate(m_main);
			}

			return m_main;
		}
	}


#if UNITY_EDITOR
	[UnityEditor.MenuItem("Gungrounds/Game Settings")]
	public static void SelectGameSettings()
	{
		if (m_main == null)
		{
			m_main = UnityEditor.AssetDatabase.LoadAssetAtPath<GameSettings>(DEFAULT_ASSET_PATH);
		}

		if (m_main == null)
		{
			m_main = ScriptableObject.CreateInstance<GameSettings>();
			UnityEditor.AssetDatabase.CreateAsset(m_main, DEFAULT_ASSET_PATH);
			UnityEditor.AssetDatabase.SaveAssets();
			UnityEditor.AssetDatabase.Refresh();
		}

		UnityEditor.Selection.activeObject = m_main;
	}
#endif

	[Header("Game Settings")]
	public GameObject overrideGhostTypeSpawnedByEnemies;
	
	public float movementEnemySolidScale = 1f;
	public float movementEnemyGhostsScale = 1f;
	public enum WEAPON_SWITCH_CONTROL_TYPE
	{
		SWITCH = 0,
		SHOOT = 1
	}
	public WEAPON_SWITCH_CONTROL_TYPE weaponSwitchControlsType = WEAPON_SWITCH_CONTROL_TYPE.SHOOT;
	 
	public int addObjectToEveryXthEnemy = 0;
	public GameObject objectToAdd;
	
	public GameVersion version;
	
	public int aimPrecision = 2;

}
