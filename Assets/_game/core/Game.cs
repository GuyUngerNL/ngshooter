﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class Game : MonoBehaviour
{
	public bool combatMode = false;
	 
	public List<Player> players { get; private set; }
	public bool started { get; private set; }
	public bool isLoading { get; private set; }
	public bool isPaused { get; private set; }
	
	public void Resume()
	{
		if (!isPaused) return;

		isPaused = false;
		
	}
	
	public void InitManagers()
	{

	}
}
 