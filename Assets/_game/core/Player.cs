﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Player : CombatEntity
{
	// Each player in co-op mode will have different index, main-player will have playerIndex = 0
	public int playerIndex { get; private set; }
	public int rewiredPlayerIndex { get; private set; }

	PlayerData _data;
	public PlayerData data { get { return _data; } set { _data = value; } }
	
	public void Init(int playerIndex, int rewiredPlayerIndex)
	{
		this.playerIndex = playerIndex;
		this.rewiredPlayerIndex = rewiredPlayerIndex;
		
	}
		 
}