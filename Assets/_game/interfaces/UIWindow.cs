﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIWindow : MonoBehaviour
{
	public virtual void Show()
	{

	}

	public virtual void Hide()
	{

	}

	protected virtual IEnumerator ShowCoroutine()
	{
		yield break;
	}

	protected virtual IEnumerator HideCoroutine()
	{
		yield break;
	}
}
