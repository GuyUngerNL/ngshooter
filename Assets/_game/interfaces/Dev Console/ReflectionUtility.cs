﻿using System;
using System.Reflection;

using UnityEngine;

public static class ReflectionUtility
{
	public static T GetObject<T>(string path)
	{
		return GetObject<T>(path, string.Empty);
	}

	public static T GetObject<T>(string path, string @namespace)
	{
		var pathParts = path.Split('.');

		if (pathParts.Length <= 1) return default(T);

		if(!string.IsNullOrEmpty(@namespace))
		{
			pathParts[0] = string.Format("{0}.{1}", @namespace, pathParts[0]);
		}

		var baseType = Type.GetType(pathParts[0]);

		if (baseType == null) return default(T);

		object currentObject = null;
		Type currentType = baseType;

		var flags = BindingFlags.Static | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy;

		for (int i = 1; i < pathParts.Length; i++)
		{
			var prop = currentType.GetProperty(pathParts[i], flags);
			var field = currentType.GetField(pathParts[i], flags);

			if (prop != null)
			{
				currentType = prop.PropertyType;
				currentObject = prop.GetValue(currentObject, null);
			}
			else if(field != null)
			{
				currentType = field.FieldType;
				currentObject = field.GetValue(currentObject);
			}
			else
			{
				return default(T);
			}

			if (currentObject == null) return default(T);
		}

		return (T)currentObject;
	}
	
}
