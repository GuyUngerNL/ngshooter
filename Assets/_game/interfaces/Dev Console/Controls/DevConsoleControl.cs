﻿using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DevConsoleControl : MonoBehaviour
{
	protected PropertyInfo prop;

	public void SetProp(PropertyInfo prop)
	{
		this.prop = prop;
	}

	public abstract void UpdateValue();
}
