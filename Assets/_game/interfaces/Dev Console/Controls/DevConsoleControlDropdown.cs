﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class DevConsoleControlDropdown : DevConsoleControl
{
	public Action<int> OnChange;

	public Dropdown dropdown;
	public TMPro.TextMeshProUGUI label;

	public Type enumType;

	public void Init(string name, Type enumType)
	{
		label.text = name;

		this.enumType = enumType;
		var values = Enum.GetNames(enumType);
		dropdown.AddOptions(values.OfType<string>().ToList());

		dropdown.onValueChanged.AddListener
		((x) => {
			if (OnChange != null) OnChange.Invoke(Enum.GetValues(enumType).Cast<int>().ElementAt(x));
		});
	}

	public override void UpdateValue()
	{
		var value = (int)prop.GetValue(null, null);
		dropdown.value = Enum.GetValues(enumType).Cast<int>().ToList().IndexOf(value);
	}
}
