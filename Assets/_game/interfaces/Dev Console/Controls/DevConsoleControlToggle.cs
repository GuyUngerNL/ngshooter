﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class DevConsoleControlToggle : DevConsoleControl
{
	public Action<bool> OnChange;

	public Toggle control;
	public TMPro.TextMeshProUGUI label;

	public void Init(string name)
	{
		label.text = name;

		control.onValueChanged.AddListener
		((x) => {
			if (OnChange != null) OnChange.Invoke(x);
		});
	}

	public override void UpdateValue()
	{
		control.isOn = (bool)prop.GetValue(null, null);
	}
}
