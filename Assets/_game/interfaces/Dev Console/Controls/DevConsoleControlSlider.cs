﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Text.RegularExpressions;
using DevTools;

public class DevConsoleControlSlider : DevConsoleControl
{
	public Action<float> OnChange;

	public Slider input;
	public TMPro.TextMeshProUGUI label;
	public TMPro.TextMeshProUGUI valueLabel;

	public void Init(string name, SliderAttribute attr)
	{
		label.text = name;

		input.minValue = attr.min;
		input.maxValue = attr.max;

		input.onValueChanged.AddListener
		((x) => {
			if (OnChange != null) OnChange.Invoke(x);
			UpdateValueLabel();
		});
	}

	public override void UpdateValue()
	{
		input.wholeNumbers = prop.PropertyType == typeof(int);

		if (prop.PropertyType == typeof(int))
		{
			var value = (int)prop.GetValue(null, null);
			input.value = value;
		}
		else
		{
			var value = (float)prop.GetValue(null, null);
			input.value = value;
		}

		UpdateValueLabel();
	}

	void UpdateValueLabel()
	{
		if (prop.PropertyType == typeof(int))
		{
			valueLabel.text = ((int)input.value).ToString();
		}
		else
		{
			valueLabel.text = input.value.ToString("F2");
		}
	}
}
