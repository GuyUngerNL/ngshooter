﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class DevConsoleControlObject : DevConsoleControl
{
	public Action<object> OnChange;

	public Dropdown dropdown;
	public TMPro.TextMeshProUGUI label;

	public List<string> names;
	public List<object> values;

	public void Init(string name, List<string> names, List<object> values)
	{
		label.text = name;

		names.Insert(0, "None");
		values.Insert(0, null);

		this.names = names;
		this.values = values;

		dropdown.AddOptions(names);

		dropdown.onValueChanged.AddListener
		((x) => {
			if (OnChange != null) OnChange.Invoke(values.ElementAt(x));
		});
	}

	public override void UpdateValue()
	{
		var value = prop.GetValue(null, null);
		dropdown.value = values.IndexOf(value);
	}
}
