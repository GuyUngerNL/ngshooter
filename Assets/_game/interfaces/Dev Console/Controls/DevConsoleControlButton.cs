﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DevConsoleControlButton : DevConsoleControl
{
	public Action OnTrigger;

	public Button btn;
	public Text label;

	public void Init(string name)
	{
		label.text = name;
		btn.onClick.AddListener(() => { if (OnTrigger != null) OnTrigger.Invoke(); });
	}

	public override void UpdateValue()
	{
	}
}
