﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Text.RegularExpressions;

public class DevConsoleControlInput : DevConsoleControl
{
	public Action<float> OnChange;

	public InputField input;
	public TMPro.TextMeshProUGUI label;

	public void Init(string name)
	{
		label.text = name;

		var regex = new Regex(@"^-*[0-9,\.]+$");

		input.onValidateInput = (string text, int charIndex, char addedChar) =>
		{
			return (string.IsNullOrEmpty(text) || regex.IsMatch(text)) && regex.IsMatch(addedChar.ToString()) ? addedChar : '\0';
		};

		input.onValueChanged.AddListener
		((x) => {
			float val = default(float);
			if(float.TryParse(x, out val))
			{
				if (OnChange != null) OnChange.Invoke(val);
			}
		});
	}

	public override void UpdateValue()
	{
		if(prop.PropertyType == typeof(int))
		{
			var value = (int)prop.GetValue(null, null);
			input.text = value.ToString();
		}
		else
		{
			var value = (float)prop.GetValue(null, null);
			input.text = value.ToString("F2");
		}
	}
}
