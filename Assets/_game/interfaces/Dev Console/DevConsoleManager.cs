﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

namespace DevTools
{

	public class DevConsoleManager : FlxManager
	{
		[Header("Elements")]
		public GameObject console;
		public RectTransform controls;

		[Header("Prefabs")]
		public GameObject headerControl;
		public GameObject buttonControl;
		public GameObject dropdownControl;
		public GameObject toggleControl;
		public GameObject inputControl;
		public GameObject slideControl;
		public GameObject objectControl;
		public GameObject objectMultipleControl;

		[Header("Settings")]
		public KeyCode openConsoleKey = KeyCode.Tab;

		public bool opened
		{
			get
			{
				return console.activeSelf;
			}
			set
			{
				if (opened == value) return;

				UpdateAllValues();
				console.SetActive(value);

				if (value)
				{
					Managers.TimeManager.Pause();
					if (DevConsole.nexMachinaMouseAim) Managers.Input.ShowCursor();
				}
				else
				{
					Managers.TimeManager.Resume();
					if (DevConsole.nexMachinaMouseAim) Managers.Input.HideCursor();
				}
			}
		}

		private Dictionary<string, DevConsoleControl> controlObjects = new Dictionary<string, DevConsoleControl>();
		private Dictionary<KeyCode[], MethodInfo> methodShortcuts = new Dictionary<KeyCode[], MethodInfo>();
		private Dictionary<KeyCode[], PropertyInfo> propShortcuts = new Dictionary<KeyCode[], PropertyInfo>();

		protected override void Awake()
		{
			base.Awake();
			console.SetActive(false);
			Init();

			globalUpdateMethod = typeof(DevConsole).GetMethod("Update", BindingFlags.Static | BindingFlags.NonPublic);
		}

		MethodInfo globalUpdateMethod;

		public void Show() { opened = true; }
		public void Hide() { opened = false; }

		private void Update()
		{
			if (Main.game.isPaused)
			{
				opened = false;
				return;
			}

			if (Input.GetKeyDown(openConsoleKey))
			{
				opened = !opened;
			}

			if(DevConsole.enableShortcuts)
			{
				foreach (var method in methodShortcuts)
				{
					var allPressed = true;
					var pressedNow = false;

					for (int i = 0; i < method.Key.Length; i++)
					{
						if (!Input.GetKey(method.Key[i])) allPressed = false;
						if (Input.GetKeyDown(method.Key[i])) pressedNow = true;
					}

					if (allPressed && pressedNow)
					{
						method.Value.Invoke(null, null);
					}
				}

				foreach (var prop in propShortcuts)
				{
					var allPressed = true;
					var pressedNow = false;

					for (int i = 0; i < prop.Key.Length; i++)
					{
						if (!Input.GetKey(prop.Key[i])) allPressed = false;
						if (Input.GetKeyDown(prop.Key[i])) pressedNow = true;
					}

					if (allPressed && pressedNow)
					{
						var propType = prop.Value.PropertyType;

						if (propType == typeof(bool))
						{
							prop.Value.SetValue(null, !((bool)prop.Value.GetValue(null, null)), null);

						}
					}
				}
			}

			globalUpdateMethod.Invoke(null, null);
		}

		void Init()
		{
			Type type = typeof(DevConsole);

			foreach (var method in type.GetMethods(BindingFlags.Static | BindingFlags.Public))
			{
				foreach (var attr in method.GetCustomAttributes(typeof(SectionAttribute), true)) Section((attr as SectionAttribute).name);

				if (method.IsSpecialName)
				{
					var propName = method.Name.Substring(4);

					if (method.Name.StartsWith("get_"))
					{
						var prop = type.GetProperty(propName);

						var shortcutText = string.Empty;

						foreach (var attr in prop.GetCustomAttributes(typeof(ShortcutAttribute), true))
						{
							var keyCode = (attr as ShortcutAttribute).keys;
							propShortcuts.Add(keyCode, prop);
							shortcutText = " (";
							for (int i = 0; i < keyCode.Length; i++)
							{
								if (i != 0) shortcutText += " + ";
								shortcutText += KeyToCaption(keyCode[i]);
							}
							shortcutText += ")";
						}

						foreach (var attr in prop.GetCustomAttributes(typeof(SectionAttribute), true)) Section((attr as SectionAttribute).name);

						if (prop != null && !controlObjects.ContainsKey(propName))
						{
							var propType = prop.PropertyType;

							DevConsoleControl ctrl = null;

							if (propType.IsEnum)
							{
								// Dropdown
								var ddCtrl = Instantiate(dropdownControl).GetComponent<DevConsoleControlDropdown>();
								ddCtrl.Init(GetDisplayName(prop.Name) + shortcutText, propType);
								ddCtrl.OnChange += (x) => { prop.SetValue(null, x, null); };
								ctrl = ddCtrl;
							}
							else if (propType == typeof(bool))
							{
								// Toggle
								var tglCtrl = Instantiate(toggleControl).GetComponent<DevConsoleControlToggle>();
								tglCtrl.Init(GetDisplayName(prop.Name) + shortcutText);
								tglCtrl.OnChange += (x) => {prop.SetValue(null, x, null);};
								ctrl = tglCtrl;
							}
							else if (propType == typeof(int) || propType == typeof(float))
							{
								SliderAttribute sliderAttr = null;
								foreach (var attr in prop.GetCustomAttributes(typeof(SliderAttribute), true))
								{
									sliderAttr = (attr as SliderAttribute);
								}

								if(sliderAttr != null)
								{
									// Slider
									var inpCtrl = Instantiate(slideControl).GetComponent<DevConsoleControlSlider>();
									inpCtrl.Init(GetDisplayName(prop.Name) + shortcutText, sliderAttr);
									inpCtrl.OnChange += (x) => {
										if(propType == typeof(int))
										{
											prop.SetValue(null,(int)x, null);
										}
										else
										{
											prop.SetValue(null, x, null);
										}
									};
									ctrl = inpCtrl;
								}
								else
								{
									// Input
									var inpCtrl = Instantiate(inputControl).GetComponent<DevConsoleControlInput>();
									inpCtrl.Init(GetDisplayName(prop.Name) + shortcutText);
									inpCtrl.OnChange += (x) => {
										if (propType == typeof(int))
										{
											prop.SetValue(null, (int)x, null);
										}
										else
										{
											prop.SetValue(null, x, null);
										}
									};
									ctrl = inpCtrl;
								}
							}
							else if(propType.IsSubclassOf(typeof(UnityEngine.Object)))
							{
								ListAttribute listAttr = null;
								foreach (var attr in prop.GetCustomAttributes(typeof(ListAttribute), true))
								{
									listAttr = (attr as ListAttribute);
								}

								if(listAttr != null)
								{
									var list = ReflectionUtility.GetObject<IEnumerable>(listAttr.variable);

									if (list != null)
									{
										var objects = list.OfType<UnityEngine.Object>().ToList();
										var values = objects.Cast<object>().ToList();
										var names = objects.Select(x => x.name).ToList();

										// Dropdown
										var ddCtrl = Instantiate(objectControl).GetComponent<DevConsoleControlObject>();
										ddCtrl.Init(GetDisplayName(prop.Name) + shortcutText, names, values);
										ddCtrl.OnChange += (x) => { prop.SetValue(null, x, null); };
										ctrl = ddCtrl;
									}
								}
							}
							else if(propType.IsGenericType && propType.GetGenericTypeDefinition() == typeof(List<>))
							{
								var genericArguments = propType.GetGenericArguments();
								if(genericArguments.Length == 1 && genericArguments[0].IsSubclassOf(typeof(UnityEngine.Object)))
								{
									var listType = genericArguments[0];

									ListAttribute listAttr = null;
									foreach (var attr in prop.GetCustomAttributes(typeof(ListAttribute), true))
									{
										listAttr = (attr as ListAttribute);
									}

									if (listAttr != null)
									{
										var list = ReflectionUtility.GetObject<IEnumerable>(listAttr.variable);

										if (list != null)
										{
											var objects = list.OfType<UnityEngine.Object>().ToList();
											var values = objects.Cast<object>().ToList();
											var names = objects.Select(x => x.name).ToList();

											// Dropdown
											var ddCtrl = Instantiate(objectControl).GetComponent<DevConsoleControlObjectMultiple>();
											ddCtrl.Init(GetDisplayName(prop.Name) + shortcutText, names, values);
											ddCtrl.OnChange += (x) => { prop.SetValue(null, x, null); };
											ctrl = ddCtrl;
										}
									}
								}
							}

							if (ctrl != null)
							{
								controlObjects.Add(propName, ctrl);

								ctrl.transform.SetParent(controls, false);
								ctrl.SetProp(prop);
								ctrl.UpdateValue();
							}
						}
					}
				}
				else
				{
					var shortcutText = string.Empty;

					foreach (var attr in method.GetCustomAttributes(typeof(ShortcutAttribute), true))
					{
						var keyCode = (attr as ShortcutAttribute).keys;
						methodShortcuts.Add(keyCode, method);
						shortcutText = " (";
						for (int i = 0; i < keyCode.Length; i++)
						{
							if (i != 0) shortcutText += " + ";
							shortcutText += KeyToCaption(keyCode[i]);
						}
						shortcutText += ")";
					}

					// Button
					var btnCtrl = Instantiate(buttonControl).GetComponent<DevConsoleControlButton>();
					btnCtrl.Init(GetDisplayName(method.Name) + shortcutText);
					btnCtrl.transform.SetParent(controls, false);
					btnCtrl.OnTrigger += () => { method.Invoke(null, null); };
				}
			}
		}

		void Space()
		{
			for (int i = 0; i < 2 + controls.childCount % 2; i++)
			{
				new GameObject().GetOrAddComponent<RectTransform>().SetParent(controls, false);
			}
		}

		void Section(string title)
		{
			if(controls.childCount > 0)
			{
				Space();
			}

			var headerCtrl = Instantiate(headerControl);
			headerCtrl.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = title;
			headerCtrl.transform.SetParent(controls, false);

			new GameObject().GetOrAddComponent<RectTransform>().SetParent(controls, false);
		}

		public void UpdateAllValues()
		{
			foreach(var ctrl in controlObjects.Values)
			{
				ctrl.UpdateValue();
			}
		}

		string GetDisplayName(string name)
		{
			name = name.First().ToString().ToUpper() + name.Substring(1);
			return Regex.Replace(name, "(?!^)([A-Z])", " $1");
		}

		static public string KeyToCaption(KeyCode key)
		{
			switch (key)
			{
				case KeyCode.None: return null;
				case KeyCode.Backspace: return "BS";
				case KeyCode.Tab: return "Tab";
				case KeyCode.Clear: return "Clr";
				case KeyCode.Return: return "NT";
				case KeyCode.Pause: return "PS";
				case KeyCode.Escape: return "Esc";
				case KeyCode.Space: return "SP";
				case KeyCode.Exclaim: return "!";
				case KeyCode.DoubleQuote: return "\"";
				case KeyCode.Hash: return "#";
				case KeyCode.Dollar: return "$";
				case KeyCode.Ampersand: return "&";
				case KeyCode.Quote: return "'";
				case KeyCode.LeftParen: return "(";
				case KeyCode.RightParen: return ")";
				case KeyCode.Asterisk: return "*";
				case KeyCode.Plus: return "+";
				case KeyCode.Comma: return ",";
				case KeyCode.Minus: return "-";
				case KeyCode.Period: return ".";
				case KeyCode.Slash: return "/";
				case KeyCode.Alpha0: return "0";
				case KeyCode.Alpha1: return "1";
				case KeyCode.Alpha2: return "2";
				case KeyCode.Alpha3: return "3";
				case KeyCode.Alpha4: return "4";
				case KeyCode.Alpha5: return "5";
				case KeyCode.Alpha6: return "6";
				case KeyCode.Alpha7: return "7";
				case KeyCode.Alpha8: return "8";
				case KeyCode.Alpha9: return "9";
				case KeyCode.Colon: return ":";
				case KeyCode.Semicolon: return ";";
				case KeyCode.Less: return "<";
				case KeyCode.Equals: return "=";
				case KeyCode.Greater: return ">";
				case KeyCode.Question: return "?";
				case KeyCode.At: return "@";
				case KeyCode.LeftBracket: return "[";
				case KeyCode.Backslash: return "\\";
				case KeyCode.RightBracket: return "]";
				case KeyCode.Caret: return "^";
				case KeyCode.Underscore: return "_";
				case KeyCode.BackQuote: return "`";
				case KeyCode.A: return "A";
				case KeyCode.B: return "B";
				case KeyCode.C: return "C";
				case KeyCode.D: return "D";
				case KeyCode.E: return "E";
				case KeyCode.F: return "F";
				case KeyCode.G: return "G";
				case KeyCode.H: return "H";
				case KeyCode.I: return "I";
				case KeyCode.J: return "J";
				case KeyCode.K: return "K";
				case KeyCode.L: return "L";
				case KeyCode.M: return "M";
				case KeyCode.N: return "N0";
				case KeyCode.O: return "O";
				case KeyCode.P: return "P";
				case KeyCode.Q: return "Q";
				case KeyCode.R: return "R";
				case KeyCode.S: return "S";
				case KeyCode.T: return "T";
				case KeyCode.U: return "U";
				case KeyCode.V: return "V";
				case KeyCode.W: return "W";
				case KeyCode.X: return "X";
				case KeyCode.Y: return "Y";
				case KeyCode.Z: return "Z";
				case KeyCode.Delete: return "Del";
				case KeyCode.Keypad0: return "K0";
				case KeyCode.Keypad1: return "K1";
				case KeyCode.Keypad2: return "K2";
				case KeyCode.Keypad3: return "K3";
				case KeyCode.Keypad4: return "K4";
				case KeyCode.Keypad5: return "K5";
				case KeyCode.Keypad6: return "K6";
				case KeyCode.Keypad7: return "K7";
				case KeyCode.Keypad8: return "K8";
				case KeyCode.Keypad9: return "K9";
				case KeyCode.KeypadPeriod: return ".";
				case KeyCode.KeypadDivide: return "/";
				case KeyCode.KeypadMultiply: return "*";
				case KeyCode.KeypadMinus: return "-";
				case KeyCode.KeypadPlus: return "+";
				case KeyCode.KeypadEnter: return "NT";
				case KeyCode.KeypadEquals: return "=";
				case KeyCode.UpArrow: return "UP";
				case KeyCode.DownArrow: return "DN";
				case KeyCode.RightArrow: return "LT";
				case KeyCode.LeftArrow: return "RT";
				case KeyCode.Insert: return "Ins";
				case KeyCode.Home: return "Home";
				case KeyCode.End: return "End";
				case KeyCode.PageUp: return "PU";
				case KeyCode.PageDown: return "PD";
				case KeyCode.F1: return "F1";
				case KeyCode.F2: return "F2";
				case KeyCode.F3: return "F3";
				case KeyCode.F4: return "F4";
				case KeyCode.F5: return "F5";
				case KeyCode.F6: return "F6";
				case KeyCode.F7: return "F7";
				case KeyCode.F8: return "F8";
				case KeyCode.F9: return "F9";
				case KeyCode.F10: return "F10";
				case KeyCode.F11: return "F11";
				case KeyCode.F12: return "F12";
				case KeyCode.F13: return "F13";
				case KeyCode.F14: return "F14";
				case KeyCode.F15: return "F15";
				case KeyCode.Numlock: return "Num";
				case KeyCode.CapsLock: return "Cap";
				case KeyCode.ScrollLock: return "Scr";
				case KeyCode.RightShift: return "RS";
				case KeyCode.LeftShift: return "LS";
				case KeyCode.RightControl: return "RC";
				case KeyCode.LeftControl: return "LC";
				case KeyCode.RightAlt: return "RA";
				case KeyCode.LeftAlt: return "LA";
				case KeyCode.Mouse0: return "M0";
				case KeyCode.Mouse1: return "M1";
				case KeyCode.Mouse2: return "M2";
				case KeyCode.Mouse3: return "M3";
				case KeyCode.Mouse4: return "M4";
				case KeyCode.Mouse5: return "M5";
				case KeyCode.Mouse6: return "M6";
				case KeyCode.JoystickButton0: return "(A)";
				case KeyCode.JoystickButton1: return "(B)";
				case KeyCode.JoystickButton2: return "(X)";
				case KeyCode.JoystickButton3: return "(Y)";
				case KeyCode.JoystickButton4: return "(RB)";
				case KeyCode.JoystickButton5: return "(LB)";
				case KeyCode.JoystickButton6: return "(Back)";
				case KeyCode.JoystickButton7: return "(Start)";
				case KeyCode.JoystickButton8: return "(LS)";
				case KeyCode.JoystickButton9: return "(RS)";
				case KeyCode.JoystickButton10: return "J10";
				case KeyCode.JoystickButton11: return "J11";
				case KeyCode.JoystickButton12: return "J12";
				case KeyCode.JoystickButton13: return "J13";
				case KeyCode.JoystickButton14: return "J14";
				case KeyCode.JoystickButton15: return "J15";
				case KeyCode.JoystickButton16: return "J16";
				case KeyCode.JoystickButton17: return "J17";
				case KeyCode.JoystickButton18: return "J18";
				case KeyCode.JoystickButton19: return "J19";
			}
			return null;
		}

	}





	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Method, AllowMultiple = false)]
	public class SectionAttribute : Attribute
	{
		public string name;

		public SectionAttribute(string name)
		{
			this.name = name;
		}
	}


	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Property)]
	public class ShortcutAttribute : Attribute
	{
		public KeyCode[] keys;

		public ShortcutAttribute(params KeyCode[] keys)
		{
			this.keys = keys;
		}
	}

	[AttributeUsage(AttributeTargets.Property)]
	public class ListAttribute : Attribute
	{
		public string variable;

		public ListAttribute(string variable)
		{
			this.variable = variable;
		}
	}

	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Method, AllowMultiple = false)]
	public class SliderAttribute : Attribute
	{
		public float min;
		public float max;

		public bool isInt;

		public SliderAttribute(int min, int max)
		{
			this.min = min;
			this.max = max;
			isInt = true;
		}

		public SliderAttribute(float min, float max)
		{
			this.min = min;
			this.max = max;
			isInt = false;
		}
	}





}
