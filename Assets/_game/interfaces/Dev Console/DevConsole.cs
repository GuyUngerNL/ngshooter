﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DevTools;
using UnityEngine.SceneManagement;
using static GameSettings;

/*
 * Use PROPERTIES (variables with custom GET; SET; functions) to create contorls
 * Use METHODS to create buttons
 * METHODS and PROPERTIES must be PUBLIC and STATIC, otherwise they will not be visible
 * 
 * Use [Section("Name")] attribute to split different sections
 * Use [Slider(min, max)] attribute to display numbers as sliders
 * Use [Shortcut(KeyCode)] attribute to assign keyboard shortcut to method, can be multiple codes, eg. [Shortcut(KeyCode.LeftControl, KeyCode.C)]
 * Use [List(string)] attribute to create a dropdown for the UnityObjects, parametar should point to a List<T>, eg. [List("Managers.Content.weapons")]
 * */
public static class DevConsole
{

	[Section("Game")]

	public static bool enableShortcuts
	{
		get { return PlayerPrefs.GetInt("cheat_enableShortcuts", 0) == 1; }
		set { PlayerPrefs.SetInt("cheat_enableShortcuts", value ? 1 : 0); }
	}

	public static bool nexMachinaMouseAim
	{
		get { return PlayerPrefs.GetInt("dev_nexMachinaMouseAim", 1) == 1; }
		set { PlayerPrefs.SetInt("dev_nexMachinaMouseAim", value ? 1 : 0); }
	}

}
