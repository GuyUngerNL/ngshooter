﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI : FlxManager {
	public List<GameObject> UIElementsToInstantiate;

	override protected void Awake()
	{
		for (int i = 0; i < UIElementsToInstantiate.Count; i++)
		{
			GameObject UIPrefab = UIElementsToInstantiate[i];
			Instantiate(UIPrefab);
		}
	}

	public void Init()
	{
	}
}
