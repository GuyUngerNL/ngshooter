﻿using UnityEngine; /*author Kristian Macanga, Gungrounds*/
using System.Collections;

public static class StringExtension
{

    public static int Count(this string ts)
    {
        return ts.Length;
    }

    public static char charAt(this string ts, int index)
    {
        return ts[index];
    }


}