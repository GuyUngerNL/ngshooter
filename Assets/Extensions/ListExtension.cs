﻿using UnityEngine; /*author Kristian Macanga, Gungrounds*/
using System.Collections;
using System.Collections.Generic;

public static class ListExtension
{
	public static void fillWithObjects(this IList list, int count, Object obj)
	{
		for (int i = 0; i < count; i++)
		{
			list.Add(obj);
		}
	}

	public static void fillWithRangeOfNumbers(this IList list, int count, float min, float max)
	{
		for (int i = 0; i < count; i++)
		{
			list.Add(Math.ScaleClamp(i,0,count-1,min,max));
		} 
	}

	public static void Add(this IList list, params object[] values)
	{
		foreach (object value in values)
		{
			list.Add(value);
		}
	}

	public static void swap<T>(this IList<T> list, int index1, int index2)
	{
		T item1 = list[index1];
		T item2 = list[index2];
		list[index1] = item2;
		list[index2] = item1;
	}

	public static T shift<T>(this IList<T> list)
	{
		T item = list[0];
		list.RemoveAt(0);
		return item;
	}

	public static void unshift(this IList list, object element)
	{
		list.Insert(0, element);
	}

	public static void push(this IList list, object element)
	{
		list.Insert(list.Count, element);
	}

	public static T pop<T>(this IList<T> list)
	{
		T result = default(T);
		if (list.Count > 0)
		{
			result = list[list.Count - 1];
			list.RemoveAt(list.Count - 1);
		}
		return result;
	}

	public static List<T> slice<T>(this List<T> list, int startIndex, int endIndex)
	{
		List<T> result = new List<T>();
		if (startIndex < 0)
		{
			endIndex += startIndex * -1;
			startIndex = 0;
		}
		if (endIndex > list.Count - 1)
		{
			endIndex = list.Count - 1;
		}

		for (int i = startIndex; i < endIndex; i++)
		{
			result.Add(list[i]);
		}
		return result;
	}

	/// <summary>
	/// Shuffles the element order of the specified list.
	/// </summary>
	public static void Shuffle<T>(this IList<T> list, int cycles = -1)
	{
		var count = list.Count;
		var last = count - 1;
		if (cycles == -1) cycles = 1;
		for (var j = 0; j < cycles; j++)
		{
			for (var i = 0; i < last; ++i)
			{
				var r = UnityEngine.Random.Range(i, count);
				var tmp = list[i];
				list[i] = list[r];
				list[r] = tmp;
			}
		}
	}

	public static T GetRandom<T>(this IList<T> list, bool remove = false)
	{
		if (list.Count == 0) return default(T);

		int index = Random.Range(0, list.Count);

		T r = list[index];
		if (remove)
		{
			list.RemoveAt(index);
		}
		
		return r;
	}

	public static X GetFirstExisting<X>(this IList<X> list) where X:FlxComponent
	{
		if (list.Count == 0) return default(X);
		X r = default(X);
		for (int i = 0; i < list.Count; i++)
		{
			r = list[i];
			if (r.actor.exists) return r;
		}
		return r;
	}

	public static void RemoveAllNulls(this IList list)
	{
		int l = list.Count;
		for (int i = 0; i < l; i++)
		{
			if (list[i] == null)
			{
				list.RemoveAt(i);
				i--;
				l--;
			}
		} 
	}
	
	public static List<T> copy<T>(this IList<T> list)
	{
		var count = list.Count;
		var last = count;
		List<T> result = new List<T>();
		for (var i = 0; i < last; i++)
		{
			var tmp = list[i];
			result.Add(tmp);
		}
		return result;
	}

	public static bool ContainsOfType<T>(this IList<T> list, System.Type type)
	{
		for (int i = 0; i < list.Count; i++)
		{
			if (list[i].GetType() == type)
				return true;
		}
		return false;
	}

	public static int ContainsOfTypeCount<T>(this IList<T> list, System.Type type)
	{
		int count = 0;
		for (int i = 0; i < list.Count; i++)
		{
			if (list[i].GetType() == type)
				count++;
		}
		return count;
	}

	public static U getClosestComponent<U>(this IList<U> list, float x, float y) where U : FlxComponent
	{
		U result = null;
		float closestDistance = float.MaxValue;
		for (int i = 0; i < list.Count; i++)
		{
			U other = list[i];
			float distance = (other.actor.x - x) * (other.actor.x - x) + (other.actor.y - y) * (other.actor.y - y);
			if (distance < closestDistance)
			{
				result = other;
				closestDistance = distance;
			}
		}
		return result;
	}

	#region getClosestComponent overloads

	public static U getClosestComponent<U>(this IList<U> list, FlxBasic actor) where U : FlxComponent
	{
		return list.getClosestComponent(actor.x, actor.y);
	}

	public static U getClosestComponent<U>(this IList<U> list, FlxActor actor) where U : FlxComponent
	{
		return list.getClosestComponent(actor.x, actor.y);
	}

	public static U getClosestComponent<U>(this IList<U> list, FlxComponent component) where U : FlxComponent
	{
		return list.getClosestComponent(component.actor.x, component.actor.y);
	}

	public static U getClosestComponent<U>(this IList<U> list, Vector2 vector2) where U : FlxComponent
	{
		return list.getClosestComponent(vector2.x, vector2.y);
	}

	#endregion

	#region getClosestComponent with filter

	public static U getClosestComponent<U>(this IList<U> list, float x, float y, System.Func<U, bool> predicate) where U : FlxComponent
	{
		U result = null;
		float closestDistance = float.MaxValue;
		for (int i = 0; i < list.Count; i++)
		{
			U other = list[i];

			if (!predicate(other)) continue;

			float distance = (other.actor.x - x) * (other.actor.x - x) + (other.actor.y - y) * (other.actor.y - y);
			if (distance < closestDistance)
			{
				result = other;
				closestDistance = distance;
			}
		}
		return result;
	}

	public static U getClosestComponent<U>(this IList<U> list, FlxBasic actor, System.Func<U, bool> predicate) where U : FlxComponent
	{
		return list.getClosestComponent(actor.x, actor.y, predicate);
	}

	public static U getClosestComponent<U>(this IList<U> list, FlxComponent component, System.Func<U, bool> predicate) where U : FlxComponent
	{
		return list.getClosestComponent(component.actor.x, component.actor.y, predicate);
	}

	public static U getClosestComponent<U>(this IList<U> list, Vector2 vector2, System.Func<U, bool> predicate) where U : FlxComponent
	{
		return list.getClosestComponent(vector2.x, vector2.y, predicate);
	}

	#endregion

	public static U getClosestActor<U>(this IList<U> list, float x, float y) where U : FlxActor
	{
		U result = null;
		float closestDistance = float.MaxValue;
		for (int i = 0; i < list.Count; i++)
		{
			U other = list[i];
			float distance = (other.x - x) * (other.x - x) + (other.y - y) * (other.y - y);
			if (distance < closestDistance)
			{
				result = other;
				closestDistance = distance;
			}
		}
		return result;
	}

	#region getClosestActor overloads

	public static U getClosestActor<U>(this IList<U> list, FlxBasic actor) where U : FlxActor
	{
		return list.getClosestActor(actor.x, actor.y);
	}

	public static U getClosestActor<U>(this IList<U> list, FlxComponent component) where U : FlxActor
	{
		return list.getClosestActor(component.actor.x, component.actor.y);
	}

	public static U getClosestActor<U>(this IList<U> list, Vector2 vector2) where U : FlxActor
	{
		return list.getClosestActor(vector2.x, vector2.y);
	}

	#endregion

	/**
	 * Sets the Length of an array.
	 *
	 * @param    array        The array.
	 * @param    newLength    The Length you want the array to have.
	 generic */
	public static void setLength<T>(this List<T> list, int newLength) where T : class
	{
		if (newLength < 0) return;
		int oldLength = list.Count;
		int delta = newLength - oldLength;
		int deltaAbs = Mathf.Abs(delta);
		if (delta != 0)
		{
			for (int i = 0; i < deltaAbs; i++)
			{
				if (delta < 0)
					list.RemoveAt(list.Count - 1);
				else list.Add(null);
			}
		}
	}
}