﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameObjectExtension
{
	//public static Vector3 GetMeshCenter(this GameObject go)
	//{
	//	Bounds bounds = default(Bounds);

	//	var renderers = go.GetComponentsInChildren<Renderer>();

	//	if (renderers.Length == 0) return go.transform.position;

	//	for (int i = 0; i < renderers.Length; i++)
	//	{
	//		bounds.Encapsulate(renderers[i].bounds);
	//	}

	//	return bounds.center;
	//}

	//public static void SetMeshCenter(this GameObject go, Vector3 position)
	//{
	//	var center = go.GetMeshCenter();
	//	var diff = go.transform.position - center;

	//	go.transform.position = position + diff;
	//}

	public static T GetOrAddComponent<T>(this GameObject go) where T : Component
	{
		var c = go.GetComponent<T>();
		return c ? c : go.AddComponent<T>();
	}

	public static T GetOrAddComponent<T>(this MonoBehaviour com) where T : Component
	{
		var c = com.GetComponent<T>();
		return c ? c : com.gameObject.AddComponent<T>();
	}
}
